<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/', function () {
	return view('welcome');
});
Route::get('/sample', 'Attendance\AttendanceController@viewAttendance')->name('sample');

Auth::routes();

// Route::post("register-user", "Auth\RegisterUserController@store")->name("register-user");
//Acadmic Year Route
Route::get('/home', 'HomeController@index')->name('acadmic-year');
Route::get('/myprofile', 'Profile\ProfileController@index')->name("myprofile");
// Acadmic year management
Route::get('/manage-year', 'HomeController@manageyear')->name("manage-year");
Route::POST('/update-year', 'HomeController@store')->name("update-year");
Route::POST('/addYear', 'HomeController@addYear');
Route::POST('/editYear', 'HomeController@editYear')->name("editYear");
Route::POST('/deleteYear', 'HomeController@deleteYear');

//Route::group(['middleware' => ['web']], function () {
Route::resource('/post', 'PostController');
Route::POST('/addPost', 'PostController@addPost');
Route::POST('/editPost', 'PostController@editPost');
Route::POST('/deletePost', 'PostController@deletePost');
//});

//Faculty Management Route
Route::resource('/faculty-view', 'Faculty\FacultyController@index');
Route::get('/faculty-view', 'Faculty\FacultyController@index')->name('faculty-view');
Route::post('/faculty-add', 'Faculty\FacultyController@store')->name('faculty-add');

//routes for Ajax faculty managements
Route::POST('/editFaculty', 'Faculty\FacultyController@editFaculty');
Route::POST('/deleteFaculty', 'Faculty\FacultyController@deleteFaculty');

//Class Adviser Routes
Route::get('/faculty-class', 'Faculty\ClassAdviserController@index')->name('faculty-class');
Route::post('/store-class-ca', 'Faculty\ClassAdviserController@store')->name('store-class-ca');
Route::post('/editClassAdviser', 'Faculty\ClassAdviserController@editClassAdviser')->name('edit-class');

// subjects routes:
Route::resource('/subjects-view', 'Subject\SubjectController@index');
Route::get('/subjects-view', 'Subject\SubjectController@index')->name('subjects-view');
Route::POST('/subjects-view', 'Subject\SubjectController@index')->name('subjects-view');
Route::POST('/subjects-store', 'Subject\SubjectController@store')->name('subjects-store');
Route::POST('/subjectsEdit', 'Subject\SubjectController@subjectsEdit')->name('subjectsEdit');
Route::POST('/subjectsDelete', 'Subject\SubjectController@subjectsDelete')->name('subjectsDelete');

//subject teachers routes
Route::get('/subject-teachers', 'Subject\SubjectTeacherController@index')->name('subject-teachers');
Route::POST('/subject-teachers-store', 'Subject\SubjectTeacherController@store')->name('subject-teachers-store');
Route::POST('/deletePost', 'PostController@deletePost');

//program outcomes routes

Route::get('/program-outcomes', 'PoController@index')->name('program-outcomes');

Route::get('/po', 'PoController\PoController@index')->name('po');
Route::get('/pso', 'PoController\PoController@create')->name('pso');

/**
 * Route grouping for subjects
 *
 **/
Route::group(["prefix" => "subjects"], function () {

	Route::get("/", "Subject\SubjectController@index")->name("subjects.list");

	Route::post("/", "Subject\SubjectController@store")->name("subjects.store");

	Route::delete("/delete-school", "Subject\SubjectController@destroy")->name("subjects.destroy");
});

// Course file routes
Route::group(["prefix" => "course-file"], function () {
	Route::get('/', 'CourseFile\CourseFileController@index')->name('course-file');
	Route::get('/my-subjects', 'CourseFile\MySubjectsController@index')->name('my-subjects');
	Route::get('/view-subject', 'CourseFile\MySubjectsController@view')->name('view-my-subject');
	Route::get('/edit-subject', 'CourseFile\MySubjectsController@editSubject')->name('edit-my-subject');
	Route::get('co-statements', 'CourseFile\CoController@coindex')->name('co-statements');
	Route::post('deleteCO', 'CourseFile\CoController@deleteCO')->name('deleteCO');
	Route::post('co-add', 'CourseFile\CoController@store')->name('co-add');
	Route::get('co-po-mappings', 'CoPoMappings\CoPoMappingController@copoindex')->name('co-po-mappings');
	Route::post('/store-mappings', 'CoPoMappings\CoPoMappingController@store')->name('store-mappings');
	Route::get('internals', 'Internals\TestController@index')->name('internals');
	Route::get('internals/upload-marks', 'Internals\TestController@uploadMarks')->name('test-upload-marks');
	Route::post('internals/store-subject-marks', 'Internals\TestController@storeMarks')->name('store-subject-marks');
	Route::get('internals/test-attainment', 'Internals\TestController@testAttainment')->name('test-attainment');

});

Route::group(["prefix" => "attendance"], function () {
	Route::get('/', 'Attendance\AttendanceController@index')->name('attendance');
	Route::get('take-attendance', 'Attendance\AttendanceController@takeAttendance')->name('take-attendance');
	Route::get('extra-attendance', 'Attendance\AttendanceController@extraAttendance')->name('extra-attendance');
	Route::post('select-elective', 'Attendance\AttendanceController@selectElective')->name('select-elective');
	Route::get('store-extra-attendance', 'Attendance\AttendanceController@storeExtraAttendance')->name('store-extra-attendance');
	Route::post('save-attendance', 'Attendance\AttendanceController@saveAttendance')->name('save-attendance');
	Route::get('view-attendance', 'Attendance\AttendanceController@viewAttendance')->name('view-attendance');
	Route::get('view-subject-attendance', 'Attendance\AttendanceController@viewSubjectAttendance')->name('view-subject-attendance');
	Route::get('defaulter-list', 'Attendance\AttendanceController@defaulterList')->name('defaulter-list');
	Route::get('view-defaulter-list', 'Attendance\AttendanceController@viewDefaulter')->name('view-defaulter-list');
	Route::get('generate-defaulter', 'Attendance\AttendanceController@generateDefaulter')->name('generate-defaulter');
	Route::get('take-practical-attendance', 'Attendance\PracticalAttendanceController@takeAttendance')->name('take-practical-attendance');
	Route::get('view-practical-attendance', 'Attendance\PracticalAttendanceController@takeAttendance')->name('view-practical-attendance');
	Route::post('save-practical-attendance', 'Attendance\PracticalAttendanceController@saveAttendance')->name('save-practical-attendance');
	Route::get('view-practical-defaulter-list', 'Attendance\PracticalAttendanceController@viewDefaulter')->name('view-practical-defaulter-list');
	Route::get('view-final-defaulter-list', 'Attendance\FinalAttendanceController@finalDefaulter')->name('view-final-defaulter-list');

});
//co po mappings routes

//Unit Test Management
Route::group(["prefix" => "test-management"], function () {
	Route::get('/', 'UnitTest\UnitTestController@index')->name('test-management');
	Route::get('/question-bank', 'UnitTest\UnitTestController@questionBank')->name('question-bank');
	Route::get('/subject-question-bank', 'UnitTest\QuestionBankController@viewQuestions')->name('subject-question-bank');
	Route::get('/add-questions', 'UnitTest\QuestionBankController@index')->name('add-questions');
	Route::get('/edit-question', 'UnitTest\QuestionBankController@editQuestion')->name('edit-question');
	Route::post('store-questions', 'UnitTest\QuestionBankController@store')->name('store-questions');
	Route::post('deleteQuestion', 'UnitTest\QuestionBankController@deleteQuestion')->name('deleteQuestion');
	Route::post("store-img-question", "UnitTest\QuestionBankController@storeImageQuestion")->name("store-img-question");
	Route::post('/importQuestions', 'UnitTest\QuestionBankController@importQuestions')->name('importQuestions');
	Route::post('select-co', 'UnitTest\QuestionBankController@selectCo')->name('select-co');
	Route::get('question-paper', 'UnitTest\QuestionPaperController@index')->name('question-paper');
	Route::post('store-question-paper', 'UnitTest\QuestionPaperController@store')->name('store-question-paper');
	Route::get('view-test-paper', "UnitTest\QuestionPaperController@viewTestPaper")->name('view-test-paper');
	Route::get('create-pdf', "UnitTest\QuestionPaperController@createPDF")->name('create-pdf');

	// Route::group(["prefix" => "question-banks"], function () {
	Route::post("ajax-question-banks", "UnitTest\QuestionBankController@showQuestions")->name("question-banks-index");
	// });
	//Route::post('/question-bank', 'UnitTest\UnitTestController@questionBank')->name('question-bank');
	//Route::post('/question-bank/add-questions', 'UnitTest\UnitTestController@questionAdd')->name('add-questions');
	//Route::post('/deleteCO', 'CourseFile\CoController@deleteCO')->name('deleteCO');
	//Route::post('/co-add', 'CourseFile\CoController@store')->name('co-add');

});

// Route::post('student-management/search', 'Students\StudentController@search')->name('student-management.search');

Route::group(["prefix" => "student-management"], function () {
	Route::get("/", 'Students\StudentController@index')->name("student-management");
	Route::get("add-students", 'Students\StudentController@addStudents')->name("add-students");
	Route::post("store-students", "Students\StudentController@store")->name("store-students");
	Route::post("select-elective-subject", "Students\StudentController@electiveSubject")->name("select-elective-subject");
	Route::post("edit-student", 'Students\StudentController@test')->name("edit-student");
	Route::post('update-student', 'Students\StudentController@updateStudent')->name("update-student");
	Route::resource('student-management', 'Students\StudentController');
	Route::POST('deleteStudent', 'Students\StudentController@deleteStudent');
	Route::POST('importStudents', 'Students\StudentController@importStudents')->name('importStudents');
	//Route::get('edit-student', 'Students\StudentController@edit')->name("edit-student");

	Route::post('search', 'Students\StudentController@search')->name('student-management-search');

});
