<?php

namespace App\Jobs\UniqueCode;
use App\Jobs\Job;
use App\models\UniqueCode;
use Carbon\Carbon;

class CreateUniqueCodeJob extends Job {

	protected $request;
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($request) {
		$this->request = $request;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(UniqueCode $uniqueCode) {

		$now = Carbon::now();

		for ($i = 0; $i < count($this->request->code); $i++) {
			$data[] = [
				"code" => $this->request->code[$i],
				"short_name" => $this->request->short_name[$i],
				"slug" => str_slug($this->request->short_name[$i]),
				"created_at" => $now,
				"updated_at" => $now,
			];
		}
		return $uniqueCode->insert($data);
	}
}
