<?php

namespace App\Jobs\UniqueCode;

use App\Jobs\Job;
use App\models\UniqueCode;

class GetUniqueCodesJob extends Job {

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(UniqueCode $uniqueCode) {
		$this->uniqueCode = $uniqueCode;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {

		return $this->uniqueCode->get();
	}
}
