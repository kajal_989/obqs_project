<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\models\ClassAdvisers;
use Carbon\Carbon;

class ClassAdviserJob extends job {

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->request = $request;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(ClassAdvisers $classAdvisers) {
		$now = Carbon::now();

		for ($i = 0; $i < count($this->request->sem); $i++) {
			$data[] = [
				"acadmic_year_id" => DB::table('acadmic_years')->where('isActive', '=', '1')->select('id')->get(),
				"semister_id" => $this->request->sem[$i],
				"user_id" => $this->request->faculty[$i],
				"created_at" => $now,
				"updated_at" => $now,
			];
		}
		return $classAdvisers->insert($data);

	}
}
