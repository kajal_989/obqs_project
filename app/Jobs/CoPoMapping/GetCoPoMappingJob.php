<?php

namespace App\Jobs\CoPoMapping;
use App\Jobs\Job;
use App\models\CoPoMapping;

class GetCoPoMappingJob extends Job {

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data) {
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(CoPoMapping $coPoMapping) {

		$cosData = $this->data['cos'];
		$posData = $this->data['pos'];
		$subject_id = $this->data['subject_id'];

		foreach ($posData as $key => $po) {
			foreach ($cosData as $co) {
				$mappings[]['ratings'] = $coPoMapping
					->where('subject_id', $subject_id)
					->where('po_id', $po->id)
					->where('co_id', $co->id)
					->first();

			}
		}

		return $mappings;
	}
}
