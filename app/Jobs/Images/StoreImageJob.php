<?php

namespace App\Jobs\Images;

use App\Jobs\Job;
use App\models\Image;
use Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageFacade;

class StoreImageJob extends Job {

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($img) {

		$this->img = $img;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(Image $image) {

		$authUser = Auth::user();

		$file = $this->img;

		$fileHashName = $file->store('question_images/' . Str::random(40) . '/original', 'public');

		$thumbnailImage = 'question_images/' . Str::random(40) . '/thumbnail' . '/';

		$thumbnailImagePath = $thumbnailImage . $file->hashName();

		if (File::isDirectory($thumbnailImage)) {
			$smallSizeImage = ImageFacade::make($file)
				->resize(400, 400, function ($constraint) {
					$constraint->aspectRatio();
				})
				->save(public_path($thumbnailImagePath));
		}
		if (!File::isDirectory($thumbnailImage)) {
			File::makeDirectory($thumbnailImage, 0775, true, true);
			$smallSizeImage = ImageFacade::make($file)
				->resize(400, 400, function ($constraint) {
					$constraint->aspectRatio();
				})->save(public_path($thumbnailImagePath));
		}

		return $image->create([

			'name' => $file->getClientOriginalName(),
			'type' => $file->getClientOriginalExtension(),
			'extension' => $file->getClientOriginalExtension(),
			// 'size' => formatBytes(File::size($file)),
			'path' => $thumbnailImagePath,
			'original_image' => $fileHashName,
			'thumbnail_image' => $thumbnailImage,
			'causer_id' => $authUser->id,
		]);
		// return $this->create([
		//  'name' => $request->file('image')->getClientOriginalName(),
		//  'type' => $file->getClientOriginalExtension(),
		//  'extension' => $file->getClientOriginalExtension(),
		//  'size' => formatBytes(File::size($file)),
		//  'path' => asset($fileHashName),
		//  'original_image' => $fileHashName,
		//  'thumbnail_image' => $thumbnailImage,
		//  // 'causer_id'       => optional(user())->id,
		// ]);
	}
}
