<?php

namespace App\Jobs\Questions;

use App\Jobs\Job;
use App\models\QuestionBank;

class StoreQuestionJob extends Job {

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($question, $chapter_no, $marks, $img_co_id, $storeImage, $request) {
		$this->question = $question;
		$this->chapter_no = $chapter_no;
		$this->marks = $marks;
		$this->img_co_id = $img_co_id;
		$this->storeImage = $storeImage;
		$this->request = $request;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle(QuestionBank $question) {

		return $question->create([
			"subject_id" => $this->request->subject_id,
			"test_id" => $this->request->test_id,
			"question" => $this->question,
			"marks" => $this->marks,
			"chapter_no" => $this->chapter_no,
			"co_id" => $this->img_co_id,
			"image_id" => $this->storeImage->id,
		]);
	}
}
