<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

	protected $guarded = ["id"];

	protected $casts = [

		"more_info" => "array",
	];

	// public function imagable() {
	// 	return $this->morphTo();
	// }
}
