<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model {
	use SoftDeletes, Sluggable;
}
