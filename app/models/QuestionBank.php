<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model {

	protected $guarded = ["id"];

	public function questionCo() {
		return $this->hasOne(CourseOutcome::class, "id", "co_id");
	}

	public function image() {

		return $this->hasOne(Image::Class, "id", "image_id");
	}

	public function courseOutcome() {

		return $this->hasOne(CourseOutcome::Class, "id", "co_id");
	}
}
