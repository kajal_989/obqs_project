<?php

namespace App\models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model {

	use SoftDeletes, Sluggable;

	public function sluggable() {
		return [
			'slug' => [
				'source' => 'student_name',
			],
		];
	}

	public function attendences() {
		return $this->hasMany(Attendance::class, "student_id", "id");
	}
}
