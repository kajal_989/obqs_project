<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionPaper extends Model {

	use SoftDeletes;

	protected $guarded = ["id"];
	public $timestamps = true;

	public function questionBank() {
		return $this->hasOne(QuestionBank::class, "id", "question_id");

	}

}
