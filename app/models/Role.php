<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole {
	//

	/**
	 * Roles of the user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function users() {
		return $this->belongsToMany(User::class, "role_user", "role_id", "user_id");
	}
}
