<?php

namespace App\models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseOutcome extends Model {

	use SoftDeletes, Sluggable;

	protected $dates = ['deleted_at'];

	//create unique slug for each record

	public function sluggable() {
		return [
			'slug' => [
				'source' => 'name',
			],
		];
	}

}
