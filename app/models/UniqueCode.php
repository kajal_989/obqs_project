<?php

namespace App\models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UniqueCode extends Model {
	use SoftDeletes, Sluggable;

	protected $guarded = ["id"];

	public function sluggable() {
		return [
			'slug' => [
				'source' => 'code',
			],
		];
	}
}
