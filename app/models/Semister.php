<?php

namespace App\models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Semister extends Model {

	use SoftDeletes, Sluggable;

	//create unique slug for each record

	public function sluggable() {
		return [
			'slug' => [
				'source' => 'name',
			],
		];
	}

	public function year() {
		return $this->hasOne(Year::class, 'id', 'year_id');
	}

	public function subjects() {
		return $this->hasMany(Subject::class, "semister_id", "id");
	}

	public function students() {
		return $this->hasMany(Student::class, "semister_id", "id");
	}
}
