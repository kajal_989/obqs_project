<?php

namespace App\models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Year extends Model {

	use SoftDeletes, Sluggable;

	//create unique slug for each record

	public function sluggable() {
		return [
			'slug' => [
				'source' => 'name',
			],
		];
	}

	public function students() {
		return $this->hasMany(Student::class, "year_id", "id");
	}

}
