<?php

namespace App\models;

use App\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subject extends Model {

	use SoftDeletes, Sluggable;

	//create unique slug for each record
	public $timestamps = true;

	protected $guarded = ["id"];

	public function sluggable() {
		return [
			'slug' => [
				'source' => 'name',
				'separator' => str_random(20),
			],
		];
	}
	public function semister() {
		return $this->hasOne(Semister::class, 'id', 'semister_id');
	}

	public function theoryTeacher() {
		return $this->hasOne(User::class, 'id', 'theory_teacher_id');
	}

	public function practicalTeacher() {
		return $this->hasOne(User::class, 'id', 'practical_teacher_id');
	}

	public function attendenceSubjectCounts() {
		return $this->hasMany(AttendanceSubjectCount::class, "subject_id", "id");
	}

}
