<?php

namespace App\Http\Controllers;

use DB;

class PoController extends Controller {
	public function index() {

		$data['pos'] = DB::table('program_outcomes')->get();
		return view('acadmic.po', compact('pos'), $data);
	}
}
