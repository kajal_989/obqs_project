<?php

namespace App\Http\Controllers\Internals;

use App\Http\Controllers\Controller;
use Auth;
use Crypt;
use DB;
use Illuminate\Http\Request;

class TestController extends Controller {
	public function index() {
		$data['subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->where('theory_teacher', Auth::user()->id)
			->select('subjects.id', 'subjects.name as subject_name', 'subjects.course_code', 'semisters.short_name', 'subjects.slug as slug')
			->get();

		//dd($data);
		return view('course.internals', $data);
	}

	public function uploadMarks(Request $request) {
		//	dd($request->all());
		$slug = Crypt::decrypt($request->slug);
		$data['subject_id'] = DB::table('subjects')->where('slug', $slug)->first();
		$data['test_id'] = $request->test_id;
		$data['test'] = DB::table('unit_tests')->where('id', $request->test_id)->first();

		//dd($data['subject_id']);
		$data['marks'] = DB::table('unit_test_marks')
			->join('students', 'students.id', '=', 'unit_test_marks.student_id')
			->where('subject_id', $data['subject_id']->id)
			->where('test_id', $request->test_id)
			->select('unit_test_marks.*', 'students.student_name', 'students.roll_no')
			->get();

		//dd($data['marks']);
		return view('course.upload-marks', $data);
	}

	public function storeMarks(Request $request) {

		//dd($request->all());

		for ($i = 0; $i < count($request->student_id); $i++) {

			$marks = [
				"one_a" => $request->one_a[$i],
				"one_b" => $request->one_b[$i],
				"one_c" => $request->one_c[$i],
				"one_d" => $request->one_d[$i],
				"one_e" => $request->one_e[$i],
				"one_f" => $request->one_f[$i],
				"two_a" => $request->two_a[$i],
				"two_b" => $request->two_b[$i],
				"three_a" => $request->three_a[$i],
				"three_b" => $request->three_b[$i],
				"total" => $request->total[$i],
			];

			$update = DB::table('unit_test_marks')
				->where('student_id', $request->student_id[$i])
				->where('subject_id', $request->subject_id)
				->where('test_id', $request->test_id)
				->update($marks);

		}

		return redirect()->back();
	}

	public function testAttainment(Request $request) {
		$slug = Crypt::decrypt($request->slug);

		$subject_id = DB::table('subjects')->where('slug', $slug)->first();
		$data['subject'] = $subject_id->name;
		$count_student = DB::table('unit_test_marks')
			->select('student_id')
			->where('subject_id', $subject_id->id)
			->where('test_id', $request->test_id)
			->count();

		$data['avgs'] = [
			$avg_one_a = DB::table('unit_test_marks')
				->select('one_a')
				->where('one_a', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),

			$avg_one_b = DB::table('unit_test_marks')
				->select('one_b')
				->where('one_b', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_c = DB::table('unit_test_marks')
				->select('one_c')
				->where('one_c', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_d = DB::table('unit_test_marks')
				->select('one_d')
				->where('one_d', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_e = DB::table('unit_test_marks')
				->select('one_e')
				->where('one_e', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_f = DB::table('unit_test_marks')
				->select('one_f')
				->where('one_f', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_two_a = DB::table('unit_test_marks')
				->select('two_a')
				->where('two_a', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_two_b = DB::table('unit_test_marks')
				->select('two_b')
				->where('two_b', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_three_a = DB::table('unit_test_marks')
				->select('three_a')
				->where('three_a', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_three_b = DB::table('unit_test_marks')
				->select('three_b')
				->where('three_b', '!=', NULL)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
		];

		$val = DB::table('subjects')
			->where('slug', $slug)
			->get()
			->toArray();

		$threshold = $val[0]->threshold;
		//dd($threshold);
		$two = (((int) $threshold) * 2) / 100;
		$five = (((int) $threshold) * 5) / 100;
		//dd($five);

		$data['second_table'] = [
			$avg_one_a = DB::table('unit_test_marks')
				->select('one_a')
				->where('one_a', '>', $two)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),

			$avg_one_b = DB::table('unit_test_marks')
				->select('one_b')
				->where('one_b', '>=', $two)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_c = DB::table('unit_test_marks')
				->select('one_c')
				->where('one_c', '>=', $two)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_d = DB::table('unit_test_marks')
				->select('one_d')
				->where('one_d', '>=', $two)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_e = DB::table('unit_test_marks')
				->select('one_e')
				->where('one_e', '>=', $two)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_one_f = DB::table('unit_test_marks')
				->select('one_f')
				->where('one_f', '>=', $two)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_two_a = DB::table('unit_test_marks')
				->select('two_a')
				->where('two_a', '>=', $five)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_two_b = DB::table('unit_test_marks')
				->select('two_b')
				->where('two_b', '>=', $five)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_three_a = DB::table('unit_test_marks')
				->select('three_a')
				->where('three_a', '>=', $five)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
			$avg_three_b = DB::table('unit_test_marks')
				->select('three_b')
				->where('three_b', '>=', $five)
				->where('subject_id', $subject_id->id)
				->where('test_id', $request->test_id)
				->count(),
		];

		$quetionPaperObjects = DB::table("question_papers")->where("subject_id", $subject_id->id)->orderBy("id", "ASC")->get();

		foreach ($data['second_table'] as $value) {
			$data['percent_student'][] = round((($value / $count_student) * (100)), 2);
		}

		for ($i = 0; $i < count($quetionPaperObjects); $i++) {
			DB::table("question_papers")->where("id", $quetionPaperObjects[$i]->id)->update([
				"percent_student" => $data['percent_student'][$i],
			]);
		}

		$data['cos'] = DB::table('question_papers')
			->join('course_outcomes', 'question_papers.co_id', 'course_outcomes.id')
			->select('course_outcomes.name')
			->where('question_papers.subject_id', $subject_id->id)
			->where('question_papers.test_id', $request->test_id)
			->get()
			->toArray();
		//dd($data['cos']);
		$data['distinct_co'] = DB::table('question_papers')
			->join('course_outcomes', 'question_papers.co_id', 'course_outcomes.id')
			->select('course_outcomes.name', 'course_outcomes.id')
			->distinct()
			->get();
		//	dd($data['distinct_co']);

		for ($i = 0; $i < count($data['distinct_co']); $i++) {

			$data['co_avg'][] = round(DB::table('question_papers')
					->where('co_id', $data['distinct_co'][$i]->id)
					->avg('percent_student'));
		}

		//dd($data['co_avg']);
		for ($i = 0; $i < count($data['co_avg']); $i++) {
			if ($data['co_avg'][$i] > 65) {
				$data['levels'][$i] = 3;
			} elseif ($data['co_avg'][$i] < 65 && $data['co_avg'][$i] > 55) {
				$data['levels'][$i] = 2;
			} else {
				$data['levels'][$i] = 1;
			}
		}

		//dd($data['level']);
		return view('course.test-attainment', $data);
	}
}
