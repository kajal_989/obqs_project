<?php

namespace App\Http\Controllers;
use App\AcadmicYear;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		if (Auth::user()->hasRole("admin")) {
			$data['ac'] = DB::table('acadmic_years')->where('is_active', '=', '1')->first();

			$data['all'] = DB::table('acadmic_years')->get();

			return view('acadmic.ac', $data);
		} else {
			return redirect()->back();
		}

	}

	public function manageyear() {
		if (Auth::user()->hasRole("admin")) {
			$acyear = AcadmicYear::paginate(5);

			//dd((int) round(125.49));

			$data['all'] = DB::table('acadmic_years')->get();

			return view('acadmic.manage-ac', compact('acyear'), $data);
		} else {
			return redirect()->back();
		}
	}

	public function addYear(Request $request) {
		$rules = array(
			'from_year' => 'required',
			'to_year' => 'required',
		);

		$validator = Validator::make($request->all(), $rules);

		$isExists = DB::table('acadmic_years')->where('from_year', '=', $request->from_year)->exists();
		if ($isExists == true) {

			session()->flash('error', 'Academic year  already exist');
			return response()->json(['error' => 'Academic year is already exist',
				'url' => url()->route('manage-year')], 200);

		} else {

			$acyear = new AcadmicYear;
			$acyear->from_year = $request->from_year;
			$acyear->to_year = $request->to_year;
			$acyear->is_active = '0';
			$acyear->save();

			session()->flash('success', 'Academic added successfully');
			return response()->json(['success' => 'Academic added successfully',
				'url' => url()->route('manage-year')], 200);
		}
	}

	public function editYear(request $request) {

		$isExists = DB::table('acadmic_years')->where('from_year', '=', $request->from_year)->exists();

		if ($isExists == true) {

			$acyear = AcadmicYear::where("id", $request->id)
				->update([
					//"from_year" => $request->from_year ?? null,
					"to_year" => $request->to_year ?? null,
				]);

			session()->flash('success', 'Academic updated successfully');
			return response()->json(['success' => 'Academic updated successfully',
				'url' => url()->route('manage-year')], 200);

		} else {
			$acyear = AcadmicYear::where("id", $request->id)
				->update([
					"from_year" => $request->from_year ?? null,
					"to_year" => $request->to_year ?? null,
				]);
			session()->flash('success', 'Academic updated successfully');
			return response()->json(['success' => 'Academic updated successfully',
				'url' => url()->route('manage-year')], 200);
		}
	}

	public function deleteYear(request $request) {

		$acyear = AcadmicYear::find($request->id)->delete();

		if ($acyear == true) {

			session()->flash('success', 'Academic deleted successfully');
			return response()->json(['success' => 'Academic deleted successfully',
				'url' => url()->route('manage-year')], 200);
		}
	}

	public function insert(Request $request) {
		$query1 = DB::table('acadmic_years')->where('from_year', '=', $request->from)->first();
		if (empty($query1)) {
			$query2 = DB::table('acadmic_years')->insert(
				[
					'from_year' => $request->from,
					'to_year' => $request->to,
					'is_active' => '0',
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				]);
			\Session::flash('success', 'Acadmic year added successfully');
		} else {
			\Session::flash('error', 'Failed to add the year because Acadmic year already exists :(');
		}
		$acyear = AcadmicYear::paginate(5);

		$data['all'] = DB::table('acadmic_years')->get();

		return view('acadmic.manage-ac', compact('acyear'), $data);
	}

	public function store(Request $request) {

		$query1 = DB::table('acadmic_years')->where('id', $request->id)->update(['is_active' => '1']);

		$query2 = DB::table('acadmic_years')->where('id', '!=', $request->id)->update(['is_active' => '0']);
		\Session::flash('message', 'Successfully updated!');
		return redirect()->back();
	}

	public function destroy($id) {

		DB::table('acadmic_years')->where('id', $id)->delete();
		\Session::flash('success', 'Acadmic year deleted successfully');
		return redirect()->back();
/*
\Session::flash('msg', 'Acadmic Year Deleted Successfully..!');
return redirect()->back();
 */
	}
}
