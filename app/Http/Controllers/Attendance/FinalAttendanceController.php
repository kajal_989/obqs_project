<?php

namespace App\Http\Controllers\Attendance;

use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use PDF;

class FinalAttendanceController extends Controller {
	public function finalDefaulter(Request $request) {

		$data["start_date"] = $request->start_date;
		$data["end_date"] = $request->end_date;
		$data['batch_id'] = $request->batch_id;
		$data['headers'] = DB::table('class_advisers')
			->join('semisters', 'class_advisers.semister_id', '=', 'semisters.id')
			->join('acadmic_years', 'class_advisers.acadmic_year_id', '=', 'acadmic_years.id')
			->join('users', 'class_advisers.user_id', '=', 'users.id')
			->where("semister_id", $request->semister_id)
			->where('acadmic_year_id', $request->acadmic_year_id)
			->select('semisters.short_name', 'users.name', 'acadmic_years.from_year', 'acadmic_years.to_year')
			->first();

		if ($request->elective_subject_id == null) {

			$data['students'] = DB::table('students')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('batch_id', $request->batch_id)
				->get()
				->toArray();
			//dd($data);
			$data["total_attendence_count"] = DB::table("attendance_subject_counts")
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->count();

			$data["total_practical_attendence_count"] = DB::table("practical_attendance_counts")
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('batch_id', $request->batch_id)
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->count();

			$data['subjects'] = DB::table('subjects')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('subject_type', 'theory')
				->get()
				->toArray();

			$data['practical_subjects'] = DB::table('subjects')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('subject_type', 'practical')
				->get()
				->toArray();

			//dd($data['subjects']);
			for ($i = 0; $i < count($data['subjects']); $i++) {
				$data['total_subjects'][$i] = DB::table('attendance_subject_counts')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('subject_id', $data["subjects"][$i]->id)
					->count();
			}

			for ($i = 0; $i < count($data['practical_subjects']); $i++) {
				$data['total_subjects'][$i] = DB::table('practical_attendance_counts')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('subject_id', $data["subjects"][$i]->id)
					->where('batch_id', $request->batch_id)
					->count();
			}

			for ($i = 0; $i < count($data['students']); $i++) {

				$data['final'][$i] = [
					"roll_no" => $data["students"][$i]->roll_no,
					"student_name" => $data["students"][$i]->student_name,
				];

				$data["final"][$i]["lecture_count"] = DB::table('attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('student_id', $data["students"][$i]->id)
					->select('status')
					->count();

				$data["final"][$i]["practical_lecture_count"] = DB::table('practical_attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('batch_id', $request->batch_id)
					->where('student_id', $data["students"][$i]->id)
					->select('status')
					->count();

				$data["final"][$i]["extras"][$i] = DB::table('extra_attendances')
					->where('student_id', $data["students"][$i]->id)
					->select('extra')
					->get()
					->toArray();

				for ($k = 0; $k < count($data["subjects"]); $k++) {

					$data['final'][$i]["subject"][$data["subjects"][$k]->id] = DB::table('attendances')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->where("attendances.student_id", $data["students"][$i]->id)
						->where('subject_id', $data["subjects"][$k]->id)
						->select('status')
						->distinct()
						->count();

					// $data['final'][$i]["subject"][$data["practical_subjects"][$k]->id] = DB::table('practical_attendances')
					// 	->where('attendance_date', '>=', $request->start_date)
					// 	->where('attendance_date', '<=', $request->end_date)
					// 	->where("practical_attendances.student_id", $data["students"][$i]->id)
					// 	->where('subject_id', $data["subjects"][$k]->id)
					// 	->where('batch_id', $request->batch_id)
					// 	->select('status')
					// 	->distinct()
					// 	->count();

					$data["final"][$i]["total_extras"][$i] = $data["final"][$i]["lecture_count"] + $data["final"][$i]["extras"][$i][0]->extra;
					//dd($data["final"][$i]["total_extras"][$i]);

					$total_subject_count = DB::table('attendance_subject_counts')
					//->join('subjects')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->count();

					$total_practical_subject_count = DB::table('practical_attendance_counts')
					//->join('subjects')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->where('batch_id', $request->batch_id)
						->count();

					$data["final"][$i]["precent_attendance"] = ($data["final"][$i]["total_extras"][$i] / $total_subject_count) * 100;
					$data["final"][$i]["precent_practical_attendance"] = ($data["final"][$i]["practical_lecture_count"] / $total_practical_subject_count) * 100;

				}

			}
			// dd($data)

		} else {

			$dont_consider = DB::table('students')->where('elective_subject_id', '!=', $request->elective_subject_id)
				->distinct('elective_subject_id')
				->pluck('elective_subject_id')
				->toArray();

			//dd($dont_consider);

			$data['students'] = DB::table('students')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('batch_id', $request->batch_id)
			//->where('elective_subject_id', $request->elective_subject_id)
				->get()
				->toArray();
			//dd($data);
			$data["total_attendence_count"] = DB::table("attendance_subject_counts")
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->whereNotIn('subject_id', $dont_consider)
				->count();

			$data["total_practical_attendence_count"] = DB::table("practical_attendance_counts")
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('batch_id', $request->batch_id)
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->count();

			$data['subjects'] = DB::table('subjects')->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->whereNotIn('id', $dont_consider)
				->get()
				->toArray();

			$data['practical_subjects'] = DB::table('practical_attendance_counts')
				->join('subjects', 'subjects.id', '=', 'practical_attendance_counts.subject_id')
				->where('practical_attendance_counts.acadmic_year_id', $request->acadmic_year_id)
				->where('practical_attendance_counts.semister_id', $request->semister_id)
				->where('practical_attendance_counts.batch_id', $request->batch_id)
				->select('subjects.short_name', 'subjects.id')
				->distinct()
				->get()
				->toArray();

			//dd($data['subjects']);

			for ($i = 0; $i < count($data['subjects']); $i++) {
				$data['total_subjects'][$i] = DB::table('attendance_subject_counts')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('subject_id', $data["subjects"][$i]->id)
					->whereNotIn('subject_id', $dont_consider)
					->count();
			}

			for ($i = 0; $i < count($data['practical_subjects']); $i++) {
				$data['total_practical_subjects'][$i] = DB::table('practical_attendance_counts')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('subject_id', $data["practical_subjects"][$i]->id)
					->where('batch_id', $request->batch_id)
					->count();
			}

			for ($i = 0; $i < count($data['students']); $i++) {

				$data['final'][$i] = [
					"roll_no" => $data["students"][$i]->roll_no,
					"student_name" => $data["students"][$i]->student_name,
				];

				$data["final"][$i]["lecture_count"] = DB::table('attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('student_id', $data["students"][$i]->id)
					->select('status')
					->count();

				$data["final"][$i]["practical_lecture_count"] = DB::table('practical_attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('batch_id', $request->batch_id)
					->where('student_id', $data["students"][$i]->id)
					->select('status')
					->count();

				$data["final"][$i]["extras"][$i] = DB::table('extra_attendances')
					->where('student_id', $data["students"][$i]->id)
					->select('extra')
					->get()
					->toArray();

				for ($k = 0; $k < count($data["subjects"]); $k++) {

					$data['final'][$i]["subject"][$data["subjects"][$k]->id] = DB::table('attendances')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->where("attendances.student_id", $data["students"][$i]->id)
						->where('subject_id', $data["subjects"][$k]->id)
						->select('status')
						->distinct()
						->count();

					$data["final"][$i]["total_extras"][$i] = $data["final"][$i]["lecture_count"] + $data["final"][$i]["extras"][$i][0]->extra;
					//dd($data["final"][$i]["total_extras"][$i]);

					$total_subject_count = DB::table('attendance_subject_counts')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->whereNotIn('subject_id', $dont_consider)
						->count();

				}
				for ($k = 0; $k < count($data["practical_subjects"]); $k++) {

					// $data['final'][$i]["practical_subject"][$data["practical_subjects"][$k]->id] = DB::table('practical_attendances')
					// 	->where('attendance_date', '>=', $request->start_date)
					// 	->where('attendance_date', '<=', $request->end_date)
					// 	->where("practical_attendances.student_id", $data["students"][$i]->id)
					// 	->where('subject_id', $data["practical_subjects"][$k]->id)
					// 	->where('batch_id', $request->batch_id)
					// 	->select('status')
					// 	->distinct()
					// 	->count();

					//	dd($data['final'][$i]["practical_subject"][$data["practical_subjects"][$k]->id]);
					//dd($data["final"][$i]["practical_lecture_count"]);

					$total_practical_subject_count = DB::table('practical_attendance_counts')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->where('batch_id', $request->batch_id)
						->count();
					//dd($data['final'][$i]["subject"][$data["practical_subjects"][$k]->id]);
					$data["final"][$i]["precent_attendance"] = ($data["final"][$i]["total_extras"][$i] / $total_subject_count) * 100;
					$data["final"][$i]["precent_practical_attendance"] = ($data["final"][$i]["practical_lecture_count"] / $total_practical_subject_count) * 100;

				}

				//dd($data["final"][$i]["precent_practical_attendance"]);

			}

		}
		if ($request->submit == "Get Defaulter List") {
			return view('attendance.view-final-defaulter-list', $data);
		} else {

			$pdf = PDF::loadView('pdfs.final-defaulter', $data);
			return $pdf->download('defaulterList' . '.pdf');
		}

	}
}
