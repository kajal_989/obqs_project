<?php

namespace App\Http\Controllers\Attendance;

use App\Http\Controllers\Controller;
use App\models\Attendance;
use App\models\Semister;
use App\models\Student;
use App\models\Subject;
use App\models\Year;
use Auth;
use Crypt;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use PDF;
use Session;

class AttendanceController extends Controller {

	public function __construct(Student $student, Semister $semister, Subject $subject, Year $year) {
		$this->student = $student;
		$this->semister = $semister;
		$this->subject = $subject;
		$this->year = $year;
	}

	public function index() {
		$data['acadmic_year'] = DB::table('acadmic_years')->where('is_active', '1')->get();

		$data['theory_subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->where('subject_teachers.theory_teacher', Auth::user()->id)
			->where('subjects.subject_type', 'theory')
			->select('subjects.slug', 'subjects.name', 'subjects.id')
			->get();
		//dd($data);

		$data['practical_subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->where('subject_teachers.theory_teacher', Auth::user()->id)
			->where('subjects.subject_type', 'practical')
			->select('subjects.slug', 'subjects.name', 'subjects.id')
			->get();
		return view('attendance.attendance', $data);

	}

	public function selectElective(Request $request) {
		// dd($request->all());
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();
		if ($request->all()) {
			$data = DB::table('students')
				->join('subjects', 'students.elective_subject_id', '=', 'subjects.id')
				->where('students.semister_id', $request->semister_id)
				->where('students.acadmic_year_id', $acyear->id)
				->select('students.elective_subject_id', 'subjects.name')
				->distinct()
				->get();

			//	$data = view('ajax-select', compact('cos'))->render();
			return response()->json($data);
		}
	}

	public function takeAttendance(Request $request) {
		//dd($request->all());
		$slug = Crypt::decrypt($request->slug);
		$data['subject'] = DB::table('subjects')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->select('subjects.*', 'semisters.id as semister_id')
			->where('subjects.slug', $slug)
			->first();
		if ($data['subject']->is_elective == '1') {
			$data['students'] = DB::table('students')
				->where('semister_id', $data['subject']->semister_id)
				->where('elective_subject_id', $data['subject']->id)
				->where('acadmic_year_id', $data['subject']->acadmic_year_id)
				->get();
		} else {

			$data['students'] = DB::table('students')
				->where('semister_id', $data['subject']->semister_id)
				->where('acadmic_year_id', $data['subject']->acadmic_year_id)
				->get();
		}

		return view('attendance.take-attendance', $data);

	}

	public function saveAttendance(Request $request) {
		//dd($request->all());
		if ($request->present_students == null) {
			//dd("status is null executed");
			return redirect()->route('attendance')->with('success', 'Attendance Submited successfylly..');
		} else {

			$sem = DB::table('subjects')
				->where('id', $request->subject_id)->first();
			$attendance_date = $request->attendance_date;

			$attendance_time = $request->from_time . ' to ' . $request->to_time;
			//	$store = [];
			for ($i = 0; $i < count($request->present_students); $i++) {
				$store = [
					"acadmic_year_id" => $request->acadmic_year_id,
					"semister_id" => $request->semister_id,
					"subject_id" => $request->subject_id,
					"attendance_date" => $attendance_date,
					"attendance_time" => $attendance_time,
					"student_id" => $request->present_students[$i],
					"status" => "1",
					"created_by" => Auth::user()->id,
					"updated_by" => Auth::user()->id,
				];
				$insert = DB::table('attendances')->insert($store);
			}

			$store_subject = [
				"acadmic_year_id" => $request->acadmic_year_id,
				"semister_id" => $sem->semister_id,
				"subject_id" => $request->subject_id,
				"attendance_date" => $attendance_date,
				"attendance_time" => $attendance_time,
				"status" => "1",
				"created_by" => Auth::user()->id,
				"updated_by" => Auth::user()->id,
			];

			$store_subject_count = DB::table('attendance_subject_counts')->insert($store_subject);

			if ($store_subject_count == true) {
				return redirect()->route('attendance')->with('success', 'Attendance Submited successfylly..');
			} else {
				return redirect()->route('attendance')->with('error', 'Sorry Something went wrong ..');

			}
		}

	}

	public function defaulterList() {

		//$data['semisters'] = DB::table('semisters')->get();
		$data['acyear'] = DB::table('acadmic_years')->where('is_active', '1')->first();
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();

		$is_classAdviser = DB::table('class_advisers')
			->where('acadmic_year_id', $data['acyear']->id)
			->where('user_id', Auth::user()->id)
			->exists();

		if ($is_classAdviser == false) {
			Session::flash("error", 'Sorry you are not a Class Adviser');
			return redirect()->back();
		} else {

			$data["semisters"] = DB::table('class_advisers')
				->join('semisters', 'semisters.id', '=', 'class_advisers.semister_id')
				->where('acadmic_year_id', $data['acyear']->id)
				->where('class_advisers.user_id', Auth::user()->id)
				->get();
			//dd($data["semisters"]);
			return view('attendance.defaulter', $data);

		}

	}
	public function viewDefaulter(Request $request) {
		//
		//dd($request->all());

		$data["start_date"] = $request->start_date;
		$data["end_date"] = $request->end_date;

		$data['headers'] = DB::table('class_advisers')
			->join('semisters', 'class_advisers.semister_id', '=', 'semisters.id')
			->join('acadmic_years', 'class_advisers.acadmic_year_id', '=', 'acadmic_years.id')
			->join('users', 'class_advisers.user_id', '=', 'users.id')
			->where("semister_id", $request->semister_id)
			->where('acadmic_year_id', $request->acadmic_year_id)
			->select('semisters.short_name', 'users.name', 'acadmic_years.from_year', 'acadmic_years.to_year')
			->first();

		if ($request->elective_subject_id == null) {

			$data['students'] = DB::table('students')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->get()
				->toArray();
			//dd($data);
			$data["total_attendence_count"] = DB::table("attendance_subject_counts")
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->count();

			$data['subjects'] = DB::table('subjects')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('subject_type', 'theory')
				->get()
				->toArray();

			//dd($data['subjects']);
			for ($i = 0; $i < count($data['subjects']); $i++) {
				$data['total_subjects'][$i] = DB::table('attendance_subject_counts')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('subject_id', $data["subjects"][$i]->id)
					->count();
			}

			for ($i = 0; $i < count($data['students']); $i++) {

				$data['final'][$i] = [
					"roll_no" => $data["students"][$i]->roll_no,
					"student_name" => $data["students"][$i]->student_name,
				];

				$data["final"][$i]["lecture_count"] = DB::table('attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('student_id', $data["students"][$i]->id)
					->select('status')
					->count();

				$data["final"][$i]["extras"][$i] = DB::table('extra_attendances')
					->where('student_id', $data["students"][$i]->id)
					->select('extra')
					->get()
					->toArray();

				for ($k = 0; $k < count($data["subjects"]); $k++) {

					$data['final'][$i]["subject"][$data["subjects"][$k]->id] = DB::table('attendances')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->where("attendances.student_id", $data["students"][$i]->id)
						->where('subject_id', $data["subjects"][$k]->id)
						->select('status')
						->distinct()
						->count();

					$data["final"][$i]["total_extras"][$i] = $data["final"][$i]["lecture_count"] + $data["final"][$i]["extras"][$i][0]->extra;
					//dd($data["final"][$i]["total_extras"][$i]);

					$total_subject_count = DB::table('attendance_subject_counts')
						->where('acadmic_year_id', $request->acadmic_year_id)
						->where('semister_id', $request->semister_id)
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->count();

					$data["final"][$i]["precent_attendance"] = ($data["final"][$i]["total_extras"][$i] / $total_subject_count) * 100;

				}

			}

		} else {

			$dont_consider = DB::table('students')->where('elective_subject_id', '!=', $request->elective_subject_id)
				->distinct('elective_subject_id')
				->pluck('elective_subject_id')
				->toArray();

			//dd($dont_consider);

			$data['students'] = DB::table('students')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('elective_subject_id', $request->elective_subject_id)
				->get()
				->toArray();
			//dd($data);
			$data["total_attendence_count"] = DB::table("attendance_subject_counts")
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->whereNotIn('subject_id', $dont_consider)
				->count();
			//dd($data);

			$data['subjects'] = DB::table('subjects')->where('acadmic_year_id', $request->acadmic_year_id)
				->where('semister_id', $request->semister_id)
				->where('subject_type', 'theory')
				->whereNotIn('id', $dont_consider)
				->get()
				->toArray();

			//dd($data['subjects']);

			for ($i = 0; $i < count($data['subjects']); $i++) {
				$data['total_subjects'][$i] = DB::table('attendance_subject_counts')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('subject_id', $data["subjects"][$i]->id)
					->whereNotIn('subject_id', $dont_consider)
					->count();
			}

			//dd($data['total_subjects']);

			for ($i = 0; $i < count($data['students']); $i++) {

				$data['final'][$i] = [
					"roll_no" => $data["students"][$i]->roll_no,
					"student_name" => $data["students"][$i]->student_name,
				];

				$data["final"][$i]["lecture_count"] = DB::table('attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('student_id', $data["students"][$i]->id)
					->select('status')
					->count();

				$data["final"][$i]["extras"][$i] = DB::table('extra_attendances')
					->where('student_id', $data["students"][$i]->id)
					->select('extra')
					->get()
					->toArray();

				for ($k = 0; $k < count($data["subjects"]); $k++) {

					$data['final'][$i]["subject"][$data["subjects"][$k]->id] = DB::table('attendances')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->where("attendances.student_id", $data["students"][$i]->id)
						->where('subject_id', $data["subjects"][$k]->id)
						->select('status')
						->distinct()
						->count();

					$data["final"][$i]["total_extras"][$i] = $data["final"][$i]["lecture_count"] + $data["final"][$i]["extras"][$i][0]->extra;
					//dd($data["final"][$i]["total_extras"][$i]);

					$total_subject_count = DB::table('attendance_subject_counts')
						->where('attendance_date', '>=', $request->start_date)
						->where('attendance_date', '<=', $request->end_date)
						->whereNotIn('subject_id', $dont_consider)
						->count();

				}

				$data["final"][$i]["precent_attendance"] = ($data["final"][$i]["total_extras"][$i] / $total_subject_count) * 100;

				//dd($data["final"][$i]["precent_attendance"]);
			}

		}
		//dd($data["final"][4]["precent_attendance"]);

		// }

		$result = DB::table('attendances')
			->whereBetween('attendance_date', [$request->start_date, $request->end_date])
			->get();
		// dd($result);

		if ($request->submit == "Get Defaulter List") {
			return view('attendance.view-defaulter-list', $data);
		} else {

			$pdf = PDF::loadView('pdfs.defaulter', $data);
			$pdf->setPaper('A4', 'landscape');

			return $pdf->download('defaulterList' . '.pdf');
		}

	}

	public function viewSubjectAttendance(Request $request) {

		$slug = Crypt::decrypt($request->slug);

		$subject_id = DB::table('subjects')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->select('subjects.*', 'semisters.year_id as yid')
			->where('subjects.slug', $slug)->first();

		$data['subjects'] = $subject_id->name;
		//dd($data);

		$data["semister"] = $this->semister->where("id", $subject_id->semister_id)->with("subjects")->first();
		//$data["students"] = $this->year->find($data["semister"]->year_id)->students;
		$data['students'] = DB::table('students')->where('semister_id', $subject_id->semister_id)->get()->toArray();
		//dd($data["students"]);
		$data['result'] = DB::table('attendances')
			->where('subject_id', $subject_id->id)
			->select('attendance_date', 'attendance_time')
			->distinct()
			->get();

		//	dd($data['result']);
		if ($subject_id->is_elective == '1') {
			$data['students'] = DB::table('students')->where('acadmic_year_id', $subject_id->acadmic_year_id)
				->where('semister_id', $subject_id->semister_id)
				->where('elective_subject_id', $subject_id->id)
				->get();
		} else {
			$data['students'] = DB::table('students')->where('acadmic_year_id', $subject_id->acadmic_year_id)
				->where('semister_id', $subject_id->semister_id)
				->get();
		}
		for ($i = 0; $i < count($data['students']); $i++) {
			$data['records'][$i] = [
				"roll_no" => $data['students'][$i]->roll_no,
				"student_name" => $data['students'][$i]->student_name,

				"total" => DB::table('attendances')
					->where('student_id', $data['students'][$i]->id)
					->where('subject_id', $subject_id->id)
					->count(),
			];
			for ($j = 0; $j < count($data['result']); $j++) {
				$data['records'][$i]["subject"][$j] = DB::table('attendances')
					->where('attendance_date', $data['result'][$j]->attendance_date)
					->where('attendance_time', $data['result'][$j]->attendance_time)
					->where('student_id', $data['students'][$i]->id)
					->where('subject_id', $subject_id->id)
					->count();
			}

		}

		// dd($data['records']);

		return view('attendance.view-subject-attendance', $data);
	}

	public function extraAttendance() {

		$acyear = DB::table('acadmic_years')
			->where('is_active', '1')
			->first();

		$is_classAdviser = DB::table('class_advisers')
			->where('acadmic_year_id', $acyear->id)
			->where('user_id', Auth::user()->id)
			->pluck("semister_id")
			->first();
		// ->first();

		//dd($is_classAdviser);
		// $data['students'] = DB::table('extra_attendances')
		// 	->join('students', 'extra_attendances.student_id', '=', 'students.id')
		// 	->where('extra_attendances.acadmic_year_id', $acyear->id)
		// 	->where('students.semister_id', $is_classAdviser)
		// 	->get();

		//dd($data['students']);

		if ($is_classAdviser) {

			$data['students'] = DB::table('extra_attendances')
				->join('students', 'extra_attendances.student_id', '=', 'students.id')
				->where('extra_attendances.acadmic_year_id', $acyear->id)
				->where('extra_attendances.semister_id', $is_classAdviser)
				->select('extra_attendances.id', 'extra_attendances.extra', 'students.student_name', 'students.roll_no')
				->get();

			return view('attendance.extra', $data);

		} else {
			Session::flash("error", 'Sorry you are not a class Adviser');

			return redirect()->back();
		}

	}

	public function storeExtraAttendance(Request $request) {
		//dd($request->all());
		$active_year = DB::table('acadmic_years')->where('is_active', '1')->first();

		$acyear = DB::table('class_advisers')->where('acadmic_year_id', $active_year->id)
			->where('user_id', Auth::user()->id)->get()->toArray();

		//dd($acyear[0]->semister_id);
		// $extras = DB::table('extra_attendances')
		// 	->join('students', 'extra_attendances.student_id', '=', 'students.id')
		// 	->where('extra_attendances.acadmic_year_id', $active_year->id)
		// 	->where('students.semister_id', $acyear[0]->semister_id)
		// 	->select('extra_attendances.id','extra_attendances.extra')
		// 	->get();
		//dd($extras);

		$extras = DB::table('extra_attendances')
			->where('acadmic_year_id', $active_year->id)
			->where('semister_id', $acyear[0]->semister_id)
			->get()
			->toArray();
		//dd($extras);

		for ($i = 0; $i < count($request->id); $i++) {
			//dd($extras);
			$data = [

				"extra" => $request->extra[$i] + $extras[$i]->extra,
			];
			//dd($data);
			$update = DB::table('extra_attendances')
				->where('id', $request->id[$i])
				->update([
					"extra" => $data['extra'],
				]);

		}

		return redirect()->back()->with("success", "Extra Attendance updated successfully");

	}
}
