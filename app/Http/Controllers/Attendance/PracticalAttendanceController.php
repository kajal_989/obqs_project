<?php

namespace App\Http\Controllers\Attendance;

use App\Http\Controllers\Controller;
use Auth;
use Crypt;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use PDF;

class PracticalAttendanceController extends Controller {
	public function takeAttendance(Request $request) {
		//dd($request->all());

		if ($request->submit == "mark") {
			$slug = Crypt::decrypt($request->slug);
			$data['subject'] = DB::table('subjects')
				->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
				->select('subjects.*', 'semisters.id as semister_id')
				->where('subjects.slug', $slug)
				->first();
			//if ($data['subject']->is_elective == '1') {
			$data['students'] = DB::table('students')
				->where('semister_id', $data['subject']->semister_id)
				->where('elective_subject_id', $data['subject']->id)
				->where('batch_id', $request->batch_id)
				->where('acadmic_year_id', $data['subject']->acadmic_year_id)
				->get();
			//} else {

			$data['students'] = DB::table('students')
				->where('semister_id', $data['subject']->semister_id)
				->where('batch_id', $request->batch_id)
				->where('acadmic_year_id', $data['subject']->acadmic_year_id)
				->get();
			//}

			$data["batch_id"] = $request->batch_id;

			return view('attendance.take-practical-attendance', $data);
		} else {
			$slug = Crypt::decrypt($request->slug);

			$subject_id = DB::table('subjects')
				->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
				->select('subjects.*', 'semisters.year_id as yid')
				->where('subjects.slug', $slug)->first();
			$data['subjects'] = $subject_id->name;
			//dd($data);

			//$data["semister"] = $this->semister->where("id", $subject_id->semister_id)->with("subjects")->first();
			//$data["students"] = $this->year->find($data["semister"]->year_id)->students;
			$data['students'] = DB::table('students')
				->where('batch_id', $request->batch_id)
				->where('semister_id', $subject_id->semister_id)->get()->toArray();
			//dd($data["students"]);
			$data['result'] = DB::table('practical_attendances')
				->where('subject_id', $subject_id->id)
				->where('batch_id', $request->batch_id)
				->select('attendance_date', 'attendance_time')
				->distinct()
				->get();

			//  dd($data['result']);
			$data['students'] = DB::table('students')->where('acadmic_year_id', $subject_id->acadmic_year_id)
				->where('semister_id', $subject_id->semister_id)
				->where('batch_id', $request->batch_id)
				->get();

			for ($i = 0; $i < count($data['students']); $i++) {
				$data['records'][$i] = [
					"roll_no" => $data['students'][$i]->roll_no,
					"student_name" => $data['students'][$i]->student_name,

					"total" => DB::table('practical_attendances')
						->where('student_id', $data['students'][$i]->id)
						->where('subject_id', $subject_id->id)
						->count(),
				];
				for ($j = 0; $j < count($data['result']); $j++) {
					$data['records'][$i]["subject"][$j] = DB::table('practical_attendances')
						->where('attendance_date', $data['result'][$j]->attendance_date)
						->where('attendance_time', $data['result'][$j]->attendance_time)
						->where('student_id', $data['students'][$i]->id)
						->where('subject_id', $subject_id->id)
						->where('batch_id', $request->batch_id)
						->count();
				}

			}

			// dd($data['records']);

			return view('attendance.view-subject-attendance', $data);
		}

	}

	public function saveAttendance(Request $request) {

		if ($request->present_students == null) {
			//dd("status is null executed");
			return redirect()->route('attendance')->with('success', 'Attendance Submited successfylly..');
		} else {

			$sem = DB::table('subjects')
				->where('id', $request->subject_id)->first();
			$attendance_date = $request->attendance_date;

			$attendance_time = $request->from_time . ' to ' . $request->to_time;
			//  $store = [];
			for ($i = 0; $i < count($request->present_students); $i++) {
				$store = [
					"acadmic_year_id" => $request->acadmic_year_id,
					"semister_id" => $request->semister_id,
					"subject_id" => $request->subject_id,
					"batch_id" => $request->batch_id,
					"attendance_date" => $attendance_date,
					"attendance_time" => $attendance_time,
					"student_id" => $request->present_students[$i],
					"status" => "1",
					"created_by" => Auth::user()->id,
					"updated_by" => Auth::user()->id,
				];
				$insert = DB::table('practical_attendances')->insert($store);
			}

			$store_subject = [
				"acadmic_year_id" => $request->acadmic_year_id,
				"semister_id" => $sem->semister_id,
				"subject_id" => $request->subject_id,
				"batch_id" => $request->batch_id,
				"attendance_date" => $attendance_date,
				"attendance_time" => $attendance_time,
				"status" => "1",
				"created_by" => Auth::user()->id,
				"updated_by" => Auth::user()->id,
			];

			$store_subject_count = DB::table('practical_attendance_counts')->insert($store_subject);

			if ($store_subject_count == true) {
				return redirect()->route('attendance')->with('success', 'Attendance Submited successfylly..');
			} else {
				return redirect()->route('attendance')->with('error', 'Sorry Something went wrong ..');

			}
		}
	}

	public function viewAttendance(Request $request) {
		$slug = Crypt::decrypt($request->slug);

		$subject_id = DB::table('subjects')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->select('subjects.*', 'semisters.year_id as yid')
			->where('subjects.slug', $slug)->first();
		$data['subjects'] = $subject_id->name;
		//dd($data);
		//$data["semister"] = $this->semister->where("id", $subject_id->semister_id)->with("subjects")->first();
		//$data["students"] = $this->year->find($data["semister"]->year_id)->students;
		$data['students'] = DB::table('students')->where('semister_id', $subject_id->semister_id)->get()->toArray();
		//dd($data["students"]);
		$data['result'] = DB::table('practical_attendances')
			->where('subject_id', $subject_id->id)
			->where('batch_id', $request->batch_id)
			->select('attendance_date', 'attendance_time')
			->distinct()
			->get();

		//  dd($data['result']);
		$data['students'] = DB::table('students')->where('acadmic_year_id', $subject_id->acadmic_year_id)
			->where('semister_id', $subject_id->semister_id)
			->get();

		for ($i = 0; $i < count($data['students']); $i++) {
			$data['records'][$i] = [
				"roll_no" => $data['students'][$i]->roll_no,
				"student_name" => $data['students'][$i]->student_name,

				"total" => DB::table('practical_attendances')
					->where('batch_id', $request->batch_id)
					->where('student_id', $data['students'][$i]->id)
					->where('subject_id', $subject_id->id)
					->count(),
			];
			for ($j = 0; $j < count($data['result']); $j++) {
				$data['records'][$i]["subject"][$j] = DB::table('practical_attendances')
					->where('attendance_date', $data['result'][$j]->attendance_date)
					->where('attendance_time', $data['result'][$j]->attendance_time)
					->where('student_id', $data['students'][$i]->id)
					->where('subject_id', $subject_id->id)
					->count();
			}

		}

		// dd($data['records']);

		return view('attendance.view-subject-attendance', $data);
	}

	public function viewDefaulter(Request $request) {

		$data["start_date"] = $request->start_date;
		$data["end_date"] = $request->end_date;

		$data['headers'] = DB::table('class_advisers')
			->join('semisters', 'class_advisers.semister_id', '=', 'semisters.id')
			->join('acadmic_years', 'class_advisers.acadmic_year_id', '=', 'acadmic_years.id')
			->join('users', 'class_advisers.user_id', '=', 'users.id')
			->where("semister_id", $request->semister_id)
			->where('acadmic_year_id', $request->acadmic_year_id)
			->select('semisters.short_name', 'users.name', 'acadmic_years.from_year', 'acadmic_years.to_year')
			->first();

		$data['students'] = DB::table('students')
			->where('acadmic_year_id', $request->acadmic_year_id)
			->where('batch_id', $request->batch_id)
			->where('semister_id', $request->semister_id)
			->get()
			->toArray();

		$data["total_attendence_count"] = DB::table("practical_attendance_counts")
			->where('acadmic_year_id', $request->acadmic_year_id)
			->where('semister_id', $request->semister_id)
			->where('batch_id', $request->batch_id)
			->where('attendance_date', '>=', $request->start_date)
			->where('attendance_date', '<=', $request->end_date)
			->count();

		$data['subjects'] = DB::table('subjects')
			->where('acadmic_year_id', $request->acadmic_year_id)
			->where('semister_id', $request->semister_id)
			->where('subject_type', 'practical')
			->get()
			->toArray();
		// $data['subjects'] = DB::table('practical_attendance_counts')
		// 	->join('subjects', 'subjects.id', '=', 'practical_attendance_counts.subject_id')
		// 	->where('practical_attendance_counts.acadmic_year_id', $request->acadmic_year_id)
		// 	->where('practical_attendance_counts.semister_id', $request->semister_id)
		// 	->where('practical_attendance_counts.batch_id', $request->batch_id)
		// 	->select('subjects.short_name', 'subjects.id')
		// 	->distinct()
		// 	->get()
		// 	->toArray();

		if (empty($data["subjects"])) {

			return redirect()->back()->with('error', 'sorry data not available');
		}

		for ($i = 0; $i < count($data['subjects']); $i++) {
			$data['total_subjects'][$i] = DB::table('practical_attendance_counts')
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->where('subject_id', $data["subjects"][$i]->id)
				->where('batch_id', $request->batch_id)
				->count();
		}

		for ($i = 0; $i < count($data['students']); $i++) {

			$data['final'][$i] = [
				"roll_no" => $data["students"][$i]->roll_no,
				"student_name" => $data["students"][$i]->student_name,
			];

			$data["final"][$i]["lecture_count"] = DB::table('practical_attendances')
				->where('attendance_date', '>=', $request->start_date)
				->where('attendance_date', '<=', $request->end_date)
				->where('batch_id', $request->batch_id)
				->where('student_id', $data["students"][$i]->id)
				->select('status')
				->count();

			//dd($data["final"][$i]["lecture_count"]);

			for ($k = 0; $k < count($data["subjects"]); $k++) {

				$data['final'][$i]["subject"][$data["subjects"][$k]->id] = DB::table('practical_attendances')
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('student_id', $data["students"][$i]->id)
					->where('subject_id', $data["subjects"][$k]->id)
					->where('batch_id', $request->batch_id)
					->select('status')
					->count();

				//dd($data["subjects"][$k]->id);

				$total_subject_count = DB::table('practical_attendance_counts')
				//->join('subjects')
					->where('acadmic_year_id', $request->acadmic_year_id)
					->where('semister_id', $request->semister_id)
					->where('attendance_date', '>=', $request->start_date)
					->where('attendance_date', '<=', $request->end_date)
					->where('batch_id', $request->batch_id)
					->count();
				//dd($data['final'][$i]["subject"][$data["subjects"][$k]->id]);

				$data["final"][$i]["precent_attendance"] = ($data["final"][$i]["lecture_count"] / $total_subject_count) * 100;
				//dd($data["final"][$i]["precent_attendance"]);
			}

		}
		//dd($data["final"][$i]["precent_attendance"]);
		//dd($data);

		if ($request->submit == "Get Defaulter List") {
			return view('attendance.view-practical-defaulter-list', $data);
		} else {
			$pdf = PDF::loadView('pdfs.practical-defaulter', $data);
			$pdf->setPaper('A4', 'landscape');
			return $pdf->download('practicalDefaulter' . '.pdf');
		}

	}
	//dd()

}
