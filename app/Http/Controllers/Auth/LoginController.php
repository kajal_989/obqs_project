<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\RedirectsUsers;

class LoginController extends Controller {
	use RedirectsUsers;

	/*
		    |--------------------------------------------------------------------------
		    | Login Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles authenticating users for the application and
		    | redirecting them to your home screen. The controller uses a trait
		    | to conveniently provide its functionality to your applications.
		    |
	*/

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	protected $redirectTo = '/home';

	public function __construct() {
		// $this->redirectTo = "/home";
		$this->middleware('guest')->except('logout');
	}

	public function redirectPath() {

		// dd(method_exists($this, 'redirectTo'));
		// if (method_exists($this, 'redirectTo')) {
		// 	return $this->redirectTo();
		// }
		$this->redirectTo = Auth::user()->hasRole("admin") ? "/home" : "/student-management";

		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
	}
}
