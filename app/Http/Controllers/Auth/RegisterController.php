<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\UniqueCode;
use App\Role;
use App\User;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Register Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller handles the registration of new users as well as their
		    | validation and creation. By default this controller uses a trait to
		    | provide this functionality without requiring any additional code.
		    |
	*/

	use RegistersUsers;

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	// protected $redirectTo = '/home';

	protected $validationMessages;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator($data) {

		$validator = Validator::make($data, [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|required_with:confirm|same:confirm',
			'unique_code' => 'required|exists:unique_codes,code',
			"confirm" => "min:6",
		]);

		if ($validator->passes()) {
			return TRUE;
		}
		$this->validationMessages = $validator->messages();

		// $this->validationMessages;
		return false;
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return \App\User
	 */
	protected function create(array $data) {

		try {

			if ($this->validator($data) == true) {

				$uniqueCode = UniqueCode::where("code", $data["unique_code"])->first();

				$is_exists = DB::table('users')->where('uid', $uniqueCode->id)->exists();

				if ($is_exists == true) {
					dd("Sorry user Already Exists...");
				}

				if (!empty($uniqueCode)) {

					$subject_teacher = Role::where("name", "subject_teacher")->first();

					$user = User::create([
						'name' => $data['name'],
						'email' => $data['email'],
						'password' => bcrypt($data['password']),
						'uid' => $uniqueCode ? $uniqueCode->id : null,
					]);

					$user->attachRole($subject_teacher); // parameter can be an Role object, array, or id

					$to = "$user->email";
					$subject = "Successful Registration";
					$message = "Thank you for Registration...Your password is " . $data["password"];
					$from = "98kajalg@gmail.com";
					$headers = "From: $from";
					$sendMail = mail($to, $subject, $message, $headers);

					return redirect()->route('register')->with('success', "Registration done successfully");
				}

				$subject_teacher = Role::where("name", "subject_teacher")->first();

				$user = User::create([
					'name' => $data['name'],
					'email' => $data['email'],
					'password' => bcrypt($data['password']),
					'uid' => $uniqueCode ? $uniqueCode->id : null,
				]);

				$user->attachRole($subject_teacher); // parameter can be an Role object, array, or id

				$to = "$user->email";
				$subject = "Successful Registration";
				$message = "Thank you for Registration...Your password is " . $data["password"];
				$from = "98kajalg@gmail.com";
				$headers = "From: $from";
				$sendMail = mail($to, $subject, $message, $headers);

				return redirect()->route('register')->with('success', "Registration done successfully");

			}

			// return redirect()->back()->withErrors($this->validationMessages);

		} catch (\Exception $e) {
			dd($e->getMessage());
			// return redirect()->back()->with('error', "something went wrong");
		}

	}
}
