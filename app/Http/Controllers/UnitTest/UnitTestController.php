<?php

namespace App\Http\Controllers\UnitTest;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;

class UnitTestController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response


	 */

	public function index() {
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();

		$data['subject_list'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->where('subject_teachers.acadmic_year_id', '=', $acyear->id)
			->select('subjects.name as subject_name', 'subject_teachers.subject_id')
			->where('theory_teacher', '=', Auth::user()->id)
			->get();
		//  dd($data['subject_list']);
		return view('tests.test-management', $data);
	}

	public function questionBank(Request $request) {

		$data['subject_list'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->select('subjects.id', 'subjects.name as subject_name', 'subject_teachers.subject_id', 'subjects.slug')
			->where('theory_teacher', '=', Auth::user()->id)
			->where('subjects.subject_type', 'theory')
			->get();
		$data['tests'] = DB::table('unit_tests')
			->get();

		//	dd($data);

		return view('tests.question-list', $data);
	}

	public function questionAdd(Request $request) {
		$data['sub'] = DB::table('subjects')
			->where('id', '=', $request->subject_id)
			->first();
		$data['co'] = DB::table('course_outcomes')
			->where('subject_id', $request->subject_id)
			->get();
		if ($request->test_id == '1') {
			$data['test'] = 'Unit Test One';
		} else {
			$data['test'] = 'Unit Test Two';
		}

		header('Cache-Control: bo-cache,must-revalidate,max-age=0');
		header('Cache-Control:post-check=0,pre-check=0', false);
		header('Pragma: no-cache');
		return view('tests.add-questions', $data);
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
