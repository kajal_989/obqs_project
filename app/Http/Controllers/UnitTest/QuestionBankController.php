<?php

namespace App\Http\Controllers\UnitTest;

use App\Http\Controllers\Controller;
use App\Jobs\Images\StoreImageJob;
use App\Jobs\Questions\StoreQuestionJob;
use App\models\QuestionBank;
use Auth;
use DB;
use Illuminate\Http\Request;

class QuestionBankController extends Controller {

	// public function __construct(Question $question) {

	// 	$this->question = $question;
	// }
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		//dd($request->all());
		$data['acyear'] = DB::table('acadmic_years')->where('is_active', '1')->first();
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();

		$data['subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->where('subject_teachers.acadmic_year_id', '=', $acyear->id)
			->select('subjects.id', 'subjects.name as subject_name', 'subject_teachers.subject_id')
			->where('theory_teacher', '=', Auth::user()->id)
			->where('subjects.subject_type', 'theory')
			->get();
		$data['tests'] = DB::table('unit_tests')
			->get();

		return view('tests.add-questions', $data);

	}
	public function selectCo(Request $request) {
		//dd($request->all());
		if ($request->all()) {
			$data = DB::table('course_outcomes')
				->where('subject_id', $request->subject_id)
				->get();
			//	$data = view('ajax-select', compact('cos'))->render();
			return response()->json($data);
		}
	}

	public function store(Request $request) {

		$data = [];
		$questions = [];
		for ($i = 0; $i < count($request->marks); $i++) {

			$questions[] = [
				"subject_id" => $request->subject_id,
				"test_id" => $request->test_id,
				"question" => $request->question[$i],
				"marks" => $request->marks[$i],
				"chapter_no" => $request->chapter_no[$i],
				"co_id" => $request->co_id[$i],

			];
		}

		$qstore = DB::table('question_banks')
			->insert($questions);
		if ($qstore == true) {
			session()->flash('success', 'questions added successfully');

		} else {
			session()->flash('error', 'Questions not uploaded successfully');
		}
		return redirect()->back();
	}

	public function editQuestion(Request $request) {
		//dd($request->all());

		$data['subject'] = DB::table('subjects')
			->where('slug', $request->slug)
			->first();

		$does_have_image = DB::table('question_banks')
			->where('id', $request->question_id)
			->first();
		if ($does_have_image->image_id == NULL) {
			$data['question'] = DB::table('question_banks')
				->where('question_banks.id', $request->question_id)
				->first();
		} else {
			$data['question'] = DB::table('question_banks')
				->join('images', 'question_banks.image_id', '=', 'images.id')
				->where('question_banks.id', $request->question_id)
				->get();
		}

		$data['cos'] = DB::table('course_outcomes')
			->where('subject_id', $data['subject']->id)
			->get();

		//dd($data['question']);

		return view('tests.question-to-edit', $data);
	}

	/**
	 * Store Image Question
	 */
	public function storeImageQuestion(Request $request) {

		if ($request->img) {

			for ($i = 0; $i < count($request->img); $i++) {

				$storeImage = dispatch_now(new StoreImageJob($request->img[$i]));

				$createQuestion = dispatch_now(new StoreQuestionJob($request->question[$i], $request->chapter_no[$i], $request->marks[$i], $request->img_co_id[$i], $storeImage, $request));

			}

			session()->flash('success', 'questions added successfully');
		} else {
			session()->flash('error', 'Questions not uploaded successfully');
		}

		return redirect()->back();

	}

	public function showQuestions(Request $request) {
		$data = DB::table('question_banks')
			->where('test_id', $request->test_id)
			->join('course_outcomes', 'course_outcomes.id', '=', 'question_banks.co_id')
			->join('images', 'images.id', '=', 'question_banks.image_id')
			->where('question_banks.subject_id', $request->subject_id)
			->select('question_banks.*', 'course_outcomes.name', 'images.name as image_name', 'images.path')
			->orderBy('question_banks.marks')
			->get();

		return ($data);
	}

	public function viewQuestions(Request $request) {

		$data['subject'] = DB::table('subjects')
			->where('slug', $request->slug)
			->first();

		$does_have_image_question = DB::table('question_banks')
			->where('question_banks.subject_id', $data['subject']->id)
			->where('test_id', $request->test_id)
			->where('image_id', '!=', NULL)
			->exists();

		// if ($does_have_image_question == false) {
		//dont have image questions then

		$data["questions"] = QuestionBank::with("questionCo", "image")
			->where("test_id", $request->test_id)
			->where("subject_id", $data["subject"]->id)
			->get();

		//dd($data['questions']);

		// $data['questions'] = DB::table('question_banks')
		// 	->join('course_outcomes', 'course_outcomes.id', '=', 'question_banks.co_id')
		// 	->where('question_banks.subject_id', $data['subject'][0]->id)
		// 	->where('test_id', $request->test_id)
		// 	->select('question_banks.*', 'course_outcomes.name as co_name')
		// 	->orderBy('question_banks.marks')
		// 	->get();

		// } else {

		// $data['questions'] = DB::table('question_banks')
		// 	->where('test_id', $request->test_id)
		// 	->join('course_outcomes', 'course_outcomes.id', '=', 'question_banks.co_id')
		// 	->join('images', 'images.id', '=', 'question_banks.image_id')
		// 	->where('question_banks.subject_id', $data['subject'][0]->id)
		// 	->select('question_banks.*', 'course_outcomes.name as co_name', 'images.*')
		// 	->orderBy('question_banks.marks')
		// 	->get();

		// }

		//dd($data['questions']);

		return view('tests.view-questions', $data);

	}

	public function deleteQuestion(Request $request) {
		$question = DB::table('question_banks')
			->where('id', $request->id)
			->delete();
		//$uniquecodes = DB::delete('delete from unique_codes where id = ?', $request->id);
		if ($question == true) {

			session()->flash('success', 'Question Deleted Successfully');
			// return redirect()->refresh();
		}
	}

}
