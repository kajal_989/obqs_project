<?php

namespace App\Http\Controllers\UnitTest;

use App\Http\Controllers\Controller;
use App\models\QuestionBank;
use App\models\QuestionPaper;
use App\models\Subject;
use App\models\UnitTest;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use PDF;

class QuestionPaperController extends Controller {

	public function index() {

		$data['acyear'] = DB::table('acadmic_years')->where('is_active', '1')->first();

		$data['subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->where('subject_teachers.acadmic_year_id', '=', $data['acyear']->id)
			->select('subjects.id', 'subjects.name as subject_name', 'subject_teachers.subject_id')
			->where('theory_teacher', '=', Auth::user()->id)
			->get();

		$data['tests'] = DB::table('unit_tests')
			->get();

		return view('tests.question-paper', $data);
	}

	public function store(Request $request) {

		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();

		$now = Carbon::now();

		$isExist = DB::table('question_papers')
			->where('subject_id', $request->subject_id)
			->where('test_id', $request->test_id)
			->exists();

		if ($isExist) {

			return redirect()->back()->with('error', 'Sorry Test already Exists.');

		} else {

			$randomQuestionIds = [];
			$data = [];
			$result = [];
			$notRequiredQuestions = [];

			for ($i = 0; $i < count($request->chapter_no); $i++) {

				$questionId = QuestionBank::where('test_id', $request->test_id)
					->where('subject_id', $request->subject_id)
					->where('chapter_no', $request->chapter_no[$i])
					->where('marks', (int) $request->marks[$i])
					->whereNotIn("id", $notRequiredQuestions)
					->pluck('id')
					->random(1)
					->toArray();

				// if ($questionId) {

				// }

				array_push($notRequiredQuestions, $questionId);
				$randomQuestionIds[] = $questionId;
			}

			for ($i = 0; $i < count($randomQuestionIds); $i++) {

				$cos[$i] = DB::table('question_banks')
					->where('id', $randomQuestionIds[$i][0])
					->select('co_id')
					->first();

				$testQuestions = [
					"question_id" => $randomQuestionIds[$i][0],
					"question_name" => $request->question_name[$i],
					"co_id" => $cos[$i]->co_id,
					"subject_id" => $request->subject_id,
					"test_id" => $request->test_id,
					"acadmic_year_id" => $acyear->id,
					"created_by" => Auth::user()->id,
					"updated_by" => Auth::user()->id,
				];

				$data[] = QuestionPaper::create($testQuestions);
			}

			$semister = DB::table('subjects')
				->where('id', $request->subject_id)
				->select('semister_id')
				->first();

			$students = DB::table('students')
				->where('semister_id', $semister->semister_id)
				->select('students.id')
				->get()
				->toArray();

			$is_exist = DB::table('unit_test_marks')->where('subject_id', $request->subject_id)
				->where('test_id', $request->test_id)
				->exists();

			if ($is_exist == false) {

				for ($i = 0; $i < count($students); $i++) {

					$data = [
						"acadmic_year_id" => $acyear->id,
						"subject_id" => $request->subject_id,
						"student_id" => $students[$i]->id,
						"test_id" => $request->test_id,
						"created_at" => $now,
						"created_by" => Auth::user()->id,
						"updated_by" => Auth::user()->id,
					];

					$store = DB::table('unit_test_marks')->insert($data);
				}
			}

			if (!empty($data)) {
				return redirect()->back()->with('success', 'Test generated Successfully !');
			} else {
				return redirect()->back()->with('error', 'Sorry :( Test Not Generated!');
			}

		}

	}

	public function viewTestPaper(Request $request) {

		try {
			if ($request->submit == "View Question Paper") {

				$isExit = DB::table('question_papers')
					->where('subject_id', $request->subject_id)
					->where('test_id', $request->subject_id)
					->get();

				if ($isExit->isNotEmpty() == true) {

					return redirect()->back()->with('error', 'Sorry Test is not generated yet...');

				} else {

					$data['test'] = DB::table("unit_tests")->where("id", $request->test_id)->first();
					$data['subject'] = DB::table("subjects")->where("id", $request->subject_id)->first();

					$data["questions"] = QuestionPaper::whereHas("questionBank", function ($query) use ($request) {
						$query->where("subject_id", $request->subject_id);
						$query->where("test_id", $request->test_id);
					})
						->with("questionBank", "questionBank.questionCo")
						->get();

					return view("tests.test-paper", $data);
				}

			} else {

				$isExit = DB::table('question_papers')
					->where('subject_id', $request->subject_id)
					->where('test_id', $request->subject_id)
					->get();

				if ($isExit->isNotEmpty() == true) {
					return redirect()->back()->with('error', 'Sorry Test is not generated yet...');
				} else {
					$data['test'] = UnitTest::where("id", $request->test_id)->first();
					$data['subject'] = Subject::where("id", $request->subject_id)->first();

					$data["questions"] = QuestionPaper::whereHas("questionBank", function ($query) use ($request) {
						$query->where("subject_id", $request->subject_id);
						$query->where("test_id", $request->test_id);
					})
						->with("questionBank", "questionBank.questionCo")
						->get();

					$pdf = PDF::loadView('tests.demo', $data);

					return $pdf->download($data['subject']->name . 'UT-' . $data['test']->number . '.pdf');

				}
			}

		} catch (\Exception $e) {
			return redirect()->back();
		}
	}
}
