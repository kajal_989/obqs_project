<?php

namespace App\Http\Controllers\Faculty;

use Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\Faculty\StoreFacultyRequest;
use App\models\UniqueCode;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class FacultyController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		if (Auth::user()->hasRole("admin")) {
			$data['uniquecodes'] = DB::table('unique_codes')->where('code', '!=', 'Admin')->get();
			$uniquecodes = UniqueCode::paginate(15);
			return view('acadmic.faculty_view', compact('uniquecodes'), $data);
		} else {
			return redirect()->back();
		}
	}

	protected function validateFaculty(Request $request) {
		$this->validate($request, [
			'code' => 'required|string',
			'short_name' => 'required|string',
		]);
	}

	public function editFaculty(request $request) {

		$isCodeExists = DB::table('unique_codes')->where('code', '=', $request->code)->exists();
		if ($isCodeExists == true) {
			$acyear = UniqueCode::where("id", $request->id)
				->update([
					"short_name" => $request->short_name ?? null,
				]);

			session()->flash('success', 'Faculty updated successfully');
			return response()->json(['error' => 'Faculty Code Already Exists',
				'url' => url()->route('faculty-view')], 200);

		}
		$isExists = DB::table('unique_codes')->where('id', '=', $request->id)->exists();

		if ($isExists == true) {

			$acyear = UniqueCode::where("id", $request->id)
				->update([
					"code" => $request->code ?? null,
					"short_name" => $request->short_name ?? null,
				]);

			session()->flash('success', 'faculty Code updated successfully');
			return response()->json(['success' => 'Academic updated successfully',
				'url' => url()->route('faculty-view')], 200);

		} else {
			return response()->json(['error' => 'Academic year not found',
				'url' => url()->route('faculty-view')], 500);
		}
	}

	public function deleteFaculty(request $request) {
		$uniquecodes = UniqueCode::find($request->id)->delete();
		//$uniquecodes = DB::delete('delete from unique_codes where id = ?', $request->id);
		if ($uniquecodes == true) {

			session()->flash('success', 'Faculty Code Deleted successfully');
			return response()->json(['success' => 'Academic deleted successfully',
				'url' => url()->route('faculty-view')], 200);
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function second() {
		$data['codes'] = DB::table('unique_codes')->where('code', '!=', 'Admin')
			->get();
		//  dd($data['faculty']);
		//dd(count($data['faculty']));
		$no = array();
		for ($i = 0; $i < count($data['codes']); $i++) {
			$data['no'][$i] = $i + 1;
		}
		//dd($data['no']);
		//$data["codes"] = $this->dispatch(new GetUniqueCodesJob(new UniqueCode));
		return view('acadmic.faculty-add', $data);
	}

	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreFacultyRequest $request) {
		//$codes = $this->dispatch(new CreateUniqueCodeJob($request));
		//dd($request->all());
		$items = $request->validate([
			'code' => 'required',
			'short_name' => 'required|numeric',

		]);
		//	$items->save();
		//	return back();

		$data = [];
		$now = Carbon::now();

		for ($i = 0; $i < count($request->code); $i++) {
			$isExists = DB::table('unique_codes')->where('code', '=', $request->code[$i])->exists();
			if ($isExists == true) {
				continue;
			}

			$data[] = [
				"code" => $request->code[$i],
				"short_name" => $request->short_name[$i],
				"slug" => str_slug($request->short_name[$i]),
				"created_at" => $now,
				"updated_at" => $now,
			];
		}

		DB::table('unique_codes')->insert($data);

		session()->flash('success', 'Faculty Added successfully');

		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		DB::delete('delete from unique_codes where id = ?', [$id]);
		\Session::flash('successmsg', 'Faculty deleted successfully...');
		Alert::message('Faculty Deleted successfully');
		return redirect()->back();
	}
}
