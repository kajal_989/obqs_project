<?php

namespace App\Http\Controllers\Faculty;
use App\ClassAdvisers;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ClassAdviserController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();

		$data['ca'] = DB::table('class_advisers')
			->join('semisters', 'class_advisers.semister_id', '=', 'semisters.id')
			->join('users', 'class_advisers.user_id', '=', 'users.id')
			->select('class_advisers.id', 'semisters.short_name', 'users.name', 'class_advisers.user_id')
			->where('acadmic_year_id', $acyear->id)
			->get();
		//dd($data['ca']);
		$data['semisters'] = DB::table('semisters')->get();
		$data['faculties'] = DB::table('users')->where('uid', '!=', '1')->get();

		return view('acadmic.class-adviser', $data);

	}

	public function editClassAdviser(request $request) {
		$classadv = ClassAdvisers::where("id", $request->id)
			->update([
				"user_id" => $request->user_id ?? null,
			]);
		session()->flash('success', 'Class Adviser updated successfully');
		return response()->json(['success' => 'class Adviser updated successfully',
			'url' => url()->route('faculty-class')], 200);

	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		$now = Carbon::now();
		$data = [];
		$ac = DB::table('acadmic_years')->where('is_active', '=', '1')->first();

		for ($i = 0; $i < count($request->sem); $i++) {

			$data[] = [
				"acadmic_year_id" => $ac->id,
				"semister_id" => $request->sem[$i],
				"user_id" => $request->faculty[$i],
				"created_at" => $now,
				"updated_at" => $now,
			];

		}
		$isExists = DB::table('class_advisers')->where('semister_id', '=', $request->sem[0])->exists();
		if ($isExists == true) {
			\Session::flash('error', 'Semister already have class Adviser');

		} else {
			DB::table('class_advisers')->insert($data);
			\Session::flash('success', 'Class Adviser added successfully');
		}

		return redirect()->back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
