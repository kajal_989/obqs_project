<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use DB;

class ProfileController extends Controller {
	public function index() {
		$data['users'] = DB::table('users')
			->join('unique_codes', 'users.uid', '=', 'unique_codes.id')
			->join('role_user', 'users.id', '=', 'role_user.user_id')
			->get();

		return view('profile');
	}
}
