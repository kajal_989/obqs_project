<?php

namespace App\Http\Controllers\Students;

use App\Http\Controllers\Controller;
use App\models\Student;
use App\models\Subject;
use Auth;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Session;

class StudentController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$acyear = DB::table('acadmic_years')
			->where('is_active', '1')
			->first();
		$students = DB::table('students')
			->join('semisters', 'students.semister_id', '=', 'semisters.id')
			->select('students.id', 'students.roll_no', 'students.student_name', 'semisters.short_name')
			->orderBy('students.roll_no', 'ASC')
			->where('students.acadmic_year_id', $acyear->id)
			->paginate(500);

		return view('students.students', ['students' => $students]);
	}

	public function electiveSubject(Request $request) {
		$data = dB::table('subjects')
			->where('semister_id', $request->semister_id)
			->where('is_elective', '1')
			->get();
		return response()->json($data);
	}

	public function addStudents() {

		$data['years'] = DB::table('years')->get();
		$data['semisters'] = DB::table('semisters')->get();
		$data['acyear'] = DB::table('acadmic_years')->where('is_active', '1')->first();
		$is_classAdviser = DB::table('class_advisers')
			->where('acadmic_year_id', $data['acyear']->id)
			->where('user_id', Auth::user()->id)
			->pluck("semister_id")
			->first();

		if ($is_classAdviser) {

			return view('students.add', $data);

		} else {
			Session::flash("error", 'SORRY YOU ARE NOT A CLASS ADVISER');

			return redirect()->back();
		}

	}

	public function storeStudents(Request $request) {

		dd($request->all());
	}

	public function edit($id) {
		dd('welcome');
		return view('student-management.edit-student', ['students' => $student]);
		$students = Student::find($id);
		// Redirect to department list if updating department wasn't existed
		if ($student == null || count($student) == 0) {
			return redirect()->back();
		}

		return view('students.students', ['students' => $student]);
	}

	public function search(Request $request) {
		$constraints = [
			'student_name' => $request['enterstudentname'],
			'semisters.short_name' => $request['enteryearegse'],
		];

		$students = $this->doSearchingQuery($constraints);
		$constraints['year'] = $request['enteryearegse'];
		return view('students/students', ['students' => $students, 'searchingVals' => $constraints]);
	}

	private function doSearchingQuery($constraints) {
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();
		$query = DB::table('students')
			->leftJoin('semisters', 'students.semister_id', '=', 'semisters.id')
			->select('students.id', 'students.roll_no', 'students.student_name', 'semisters.short_name')
			->where('acadmic_year_id', $acyear->id)
			->orderBy('students.roll_no', 'ASC');

		$fields = array_keys($constraints);
		//dd($fields);
		$index = 0;
		foreach ($constraints as $constraint) {
			if ($constraint != null) {
				$query = $query->where($fields[$index], 'like', '%' . $constraint . '%');
			}
			$index++;
		}
		return $query->paginate(500);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$now = Carbon::now();
		//$data = [];
		for ($i = 0; $i < count($request->roll_no); $i++) {
			$data = [
				"acadmic_year_id" => $request->acadmic_year_id,
				"semister_id" => $request->semister_id,
				"roll_no" => $request->roll_no[$i],
				"student_name" => $request->student_name[$i],
				"department_id" => '1',
				"batch_id" => $request->batch_id[$i],
				"elective_subject_id" => $request->elective_subject_id[$i],
			];
			$store = DB::table('students')->insert($data);
		}
		//$store = DB::table('students')->insert($data);

		$students = DB::table('students')->where('acadmic_year_id', $request->acadmic_year_id)->get();
		for ($i = 0; $i < count($students); $i++) {

			$is_exists = DB::table('extra_attendances')
				->where('acadmic_year_id', $request->acadmic_year_id)
				->where('student_id', $students[$i]->id)
				->exists();
			dd($is_exists);
			if ($is_exists == true) {
				continue;
			} else {
				$data = [
					"acadmic_year_id" => $request->acadmic_year_id,
					"semister_id" => $request->semister_id,
					"student_id" => $students[$i]->id,
					"extra" => "0",
					"created_at" => $now,
					"updated_at" => $now,
					"created_by" => Auth::user()->id,
					"updated_by" => Auth::user()->id,
				];
				dd($data);
				DB::table('extra_attendances')->insert($data);
			}

		}

		// if ($store == true) {
		// 	session()->flash('success', 'Students Added Successfully to the database');
		// } else {
		// 	session()->flash('error', 'Something went wrong try again later');
		// }
		return redirect()->back()->with('success', 'students added successfully');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

		$students = DB::table('students')
			->join('semisters', 'semisters.id', '=', 'students.semister_id')
			->where('students.id', $id)
			->select('students.id', 'students.student_name', 'students.roll_no', 'semisters.short_name as class', 'students.batch_id')
			->first();

		return view('students.edit-students', ['students' => $students]);
	}

	public function updateStudent(Request $request) {

		$result = DB::table('students')
			->where('id', $request->id)
			->update([
				"roll_no" => $request->roll_no,
				"student_name" => $request->student_name,
				"batch_id" => $request->batch_id,
			]);
		return redirect('student-management')->with('success', 'student updated successfully!');

	}
	public function test(Request $request) {
		dd($request->all());
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {

		$input = [
			'id' => $request['id'],
			'roll_no' => $request['roll_no'],
			'student_name' => $request['student_name'],
		];
		$this->validate($request, [
			'roll_no' => 'required|max:2',
			'student_name' => 'required|max:100',
		]);
		Student::where('id', $id)
			->update($input);
	}

	public function deleteStudent(request $request) {
		$uniquecodes = DB::table("students")->where('id', $request->id)->delete();
		//$student = DB::delete('delete from unique_codes where id = ?', $request->id);
		if ($uniquecodes == true) {

			session()->flash('success', 'Student Deleted successfully');
			return response()->json(['success' => 'Student deleted successfully',
				'url' => url()->route('student-management')], 200);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	//function to get view for import file
	public function getImportFile() {
		return view('students.students');
	}

	private function validateInput($request) {
		$this->validate($request, [
			'year' => 'required',
			'roll_no' => 'required|max:2|number',
			'student_name' => 'required|max:100',
		]);
	}

	//function to import data
	public function importStudents(Request $request) {
		//dd($request->all());
		$now = Carbon::now();
		$semister = DB::table('semisters')->where('id', $request->semister_id)->first();
		$acyear = DB::table('acadmic_years')
			->where('is_active', '1')
			->first();

		if ($request->hasFile('studentlist')) {

			$extension = File::extension($request->studentlist->getClientOriginalName());

			if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

				$reader = new Xlsx();
				$spreadsheet = $reader->load(request()->file('studentlist'));

				$records = $spreadsheet->getActiveSheet()->toArray();
				for ($i = 0; $i < count($records); $i++) {
					//dd($records[$i]);

					$subjectObject = Subject::where("course_code", $records[$i][2])->first() ?? null;

					$is_exists = DB::table('students')->where('acadmic_year_id', $request->acadmic_year_id)
						->where('semister_id', $request->semister_id)
						->where('roll_no', $records[$i][0])
						->exists();

					if ($i == 0 || $is_exists == true) {
						continue;
					} else {

						$data[$i] = [

							"roll_no" => $records[$i][0],
							"student_name" => $records[$i][1],
							"elective_subject_id" => ($subjectObject != null) ? $subjectObject->id : null,
							"batch_id" => $records[$i][3],
							"acadmic_year_id" => $acyear->id,
							"semister_id" => $semister->id,
							"created_at" => $now,
							"updated_at" => $now,
							"created_by" => Auth::user()->id,
							"updated_by" => Auth::user()->id,

						];

						$store = Student::insert($data[$i]);
					}
				}

				$students = DB::table('students')->where('acadmic_year_id', $request->acadmic_year_id)->get();
				//dd($students);
				for ($i = 0; $i < count($students); $i++) {

					$is_exists = DB::table('extra_attendances')
						->where('acadmic_year_id', $request->acadmic_year_id)
						->where('student_id', $students[$i]->id)
						->exists();
					if ($is_exists == true) {
						continue;
					} else {
						$data = [
							"acadmic_year_id" => $request->acadmic_year_id,
							"semister_id" => $request->semister_id,
							"student_id" => $students[$i]->id,
							"extra" => "0",
							"created_at" => $now,
							"updated_at" => $now,
							"created_by" => Auth::user()->id,
							"updated_by" => Auth::user()->id,
						];

						$store = DB::table('extra_attendances')->insert($data);

					}

				}

				return redirect()->back()->with('success', "students Imported Successfully");
				// collect($records)->map($record) use (Subject $subject){
				// 	return
				// }
			}
		}
	}
}
