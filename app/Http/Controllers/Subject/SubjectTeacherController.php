<?php

namespace App\Http\Controllers\Subject;

use App\Http\Controllers\Controller;
use App\models\Subject;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class SubjectTeacherController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();
		$data['subject'] = DB::table('subjects')
			->where('acadmic_year_id', $acyear->id)
			->get();
		$data['faculties'] = DB::table('users')->where('uid', '!=', '1')->get();

		// $data['subject_teachers'] = DB::table('subjects')
		// 	->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
		// 	->join('users', 'subjects.theory_teacher', '=', 'users.id')

		// 	->select('semisters.name as sem_name', 'subjects.name as subject_name', 'users.id as theory_teacher')
		// 	->get();

		$data['subjectteacher'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->join('users', 'subject_teachers.theory_teacher', '=', 'users.id')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->select('subject_teachers.id', 'semisters.short_name as sem', 'subjects.name as subject_name', 'users.name as theory_teacher_name', 'subject_teachers.theory_teacher')
			->where('subject_teachers.acadmic_year_id', $acyear->id)
			->get();

		return view('acadmic.subject-teachers', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$now = Carbon::now();
		$data = [];
		$ac = DB::table('acadmic_years')->where('is_active', '=', '1')->first();

		for ($i = 0; $i < count($request->subject); $i++) {

			$data[] = [
				"subject_id" => $request->subject[$i],
				"acadmic_year_id" => $ac->id,
				"theory_teacher" => $request->theory_teacher[$i],
				"practical_teacher" => $request->practical_teacher[$i],
				"created_at" => $now,
				"updated_at" => $now,
			];

		}

		//dd($data);
		$isExists = DB::table('subject_teachers')->where('subject_id', '=', $request->subject[0])->exists();
		// dd($isExists);
		if ($isExists == true) {
			\Session::flash('error', 'Subject already has assigned teacher');

		} else {
			DB::table('subject_teachers')->insert($data);

			\Session::flash('success', 'faculty allocated successfully');
		}

		return redirect()->back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
