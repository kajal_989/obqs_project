<?php

namespace App\Http\Controllers\Subject;

use App\Http\Controllers\Controller;
use App\models\Subject;
use Auth;
use DB;
use Illuminate\Http\Request;

class SubjectController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	//funcions for second year subjects

	public function index() {
		if (Auth::user()->hasRole("admin")) {
			$acyear = DB::table('acadmic_years')->where('is_active', '=', '1')->first();

			$data['subjects'] = DB::table('subjects')

				->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			//->join('years', 'class_advisers.year_id', '=', 'years.id')
				->select('semisters.short_name as sem', 'subjects.course_code', 'subjects.short_name', 'subjects.id', 'subjects.name')
				->where('acadmic_year_id', $acyear->id)
				->orderBy('sem')
				->get();

			$data['faculties'] = DB::table('users')->where('uid', '!=', '1')->get();
			$data['semister'] = DB::table('semisters')->get();
			return view('acadmic.subjects', $data);
		} else {
			return redirect()->back();
		}
	}

	public function store(Request $request) {
		//dd($request->all());

		$subjects = [];
		$subjectpo = [];

		$pos = DB::table('program_outcomes')->get();

		$acyear = DB::table('acadmic_years')->where('is_active', '=', '1')->first();

		for ($i = 0; $i < count($request->course_code); $i++) {
			//dd($request->course_code);
			//$isExist = DB::table('subjects')->where('course_code', '=', $request->course_code[$i])->exists();
			$subjects[] = [
				"semister_id" => $request->semister_id[$i],
				"course_code" => $request->course_code[$i],
				"name" => $request->name[$i],
				"short_name" => $request->short_name[$i],
				"subject_type" => $request->subject_type[$i],
				"is_elective" => $request->is_elective[$i],
				"acadmic_year_id" => $acyear->id,
				"department_id" => '1',

			];
			$data = Subject::create($subjects[$i]);

		}

		//dd($subjects);
		//$data = Subject::create($subjects);
		//	dd($data);
		//	DB::table('subjects')->insert($subjects);
		$subjectids = DB::table('subjects')
			->select('id')
			->where('acadmic_year_id', '=', $acyear->id)

			->get();

		for ($i = 0; $i < count($subjectids); $i++) {
			$isExists = DB::table('subject_po_avgs')
				->where('subject_id', '=', $subjectids[$i]->id)
				->exists();

			for ($j = 0; $j < count($pos); $j++) {

				if ($isExists == true) {
					continue;
				}
				$subjectpo[] = [

					"subject_id" => $subjectids[$i]->id,
					"po_id" => $pos[$j]->id,
				];
			}

		}
		DB::table('subject_po_avgs')->insert($subjectpo);

		return redirect()->back()->with('success', 'Subject Added successfully');

	}

	public function subjectsEdit(Request $request) {
		$acyear = DB::table('acadmic_years')->where('is_active', '=', '1')->first();
		$isCodeExists = DB::table('subjects')
			->where('acadmic_year_id', '=', $acyear->id)
			->where('course_code', '=', $request->course_code)
			->exists();

		if ($isCodeExists == true) {

			$subject = DB::table('subjects')->where("id", $request->id)
				->update([
					"name" => $request->name ?? null,
					"short_name" => $request->short_name ?? null,
				]);
			session()->flash('error', 'Subject Already Exists');
			return response()->json(['error' => 'Subject Already Exists',
				'url' => url()->route('subjects-view')], 200);

		} else {

			$subject = DB::table('subjects')->where("id", $request->id)
				->update([
					"course_code" => $request->course_code ?? null,
					"name" => $request->name ?? null,
					"short_name" => $request->short_name ?? null,
				]);

			session()->flash('success', 'Subject updated successfully');
			return response()->json(['success' => 'Subject updated successfully',
				'url' => url()->route('subjects-view')], 200);

		}

	}

	public function subjectsDelete(Request $request) {
		$subjects = DB::table('subjects')->where('id', $request->id)->delete();
		//$uniquecodes = DB::delete('delete from unique_codes where id = ?', $request->id);
		if ($subjects == true) {

			session()->flash('success', 'Subject Deleted successfully');
			return response()->json(['success' => 'Academic deleted successfully',
				'url' => url()->route('subjects-view')], 200);
		}
	}

	public function create() {
		//
	}

	public function pstore(Request $request) {

	}

	public function show($id) {
		//
	}
	public function edit($id) {
		//
	}

	public function update(Request $request, $id) {
		//
	}

	public function destroy($id) {
		//
	}
}
