<?php

namespace App\Http\Controllers\CoPoMappings;

use App\Http\Controllers\Controller;
use App\models\CoPoMapping;
use App\models\Level;
use Auth;
use Crypt;
use DB;
use Illuminate\Http\Request;

class CoPoMappingController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$data['subject_id'] = 1;
		$data['cos'] = DB::table('course_outcomes')->where('subject_id', 1)->select('id', 'name')->get();
		$data['pos'] = DB::table('program_outcomes')->select('id', 'name')->get();
		$data['avg'] = DB::table('subject_po_avgs')->where('subject_id', 1)->get();
		$data['subjects'] = DB::table('subject_teachers')
			->select('subject_id')
			->where('theory_teacher', Auth::user()->uid)->get();
		$mappings = array();

		foreach ($data['pos'] as $key => $po) {
			foreach ($data['cos'] as $co) {
				$mappings[$co->id][$po->id]['ratings'] = DB::table('co_po_mappings')
					->where('subject_id', $data['subject_id'])
					->where('po_id', $po->id)
					->where('co_id', $co->id)
					->select('id', 'ratings')
					->first();
			}
		}

		//to get co-po mappings for selected subject
		//	$request->subject_id = 1; // selectted sub as 1 from input

		$data['coPoMappings'] = $mappings; //call job to fetch co-po mappings of seletced subject

		return view('course_file', $data);
	}

	public function copoindex(Request $request) {
		$id = Crypt::decrypt($request->id);
		//dd($request->all());
		$acyear = DB::table('acadmic_years')->where('is_active', '1')->first();
		$data['levels'] = DB::table('levels')->where('acadmic_year_id', $acyear->id)->get();
		$data['subject_id'] = $id;

		$data['sub'] = DB::table('subjects')
			->where('id', '=', $id)
			->first();
		$data['costatements'] = DB::table('course_outcomes')
			->where('subject_id', '=', $id)
			->get();

		$data['cos'] = DB::table('course_outcomes')->where('subject_id', $id)->select('id', 'name')->get();
		$data['pos'] = DB::table('program_outcomes')->select('id', 'name')->get();
		$data['avg'] = DB::table('subject_po_avgs')->where('subject_id', $id)->get();
		$data['weighted_avg'] = DB::table('subject_po_avgs')->where('subject_id', $id)->get();
		$data['subjects'] = DB::table('subject_teachers')
			->select('subject_id')
			->where('theory_teacher', Auth::user()->uid)->get();
		$mappings = array();

		foreach ($data['pos'] as $key => $po) {
			foreach ($data['cos'] as $co) {
				$mappings[$co->id][$po->id]['ratings'] = DB::table('co_po_mappings')
					->where('subject_id', $id)
					->where('po_id', $po->id)
					->where('co_id', $co->id)
					->select('id', 'ratings')
					->first();

			}
		}

		$data['coPoMappings'] = $mappings; //call job to fetch co-po mappings of seletced subject

		return view('course.co-po-map', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$data['cos'] = DB::table('course_outcomes')
			->where('subject_id', $request->subject_id)
			->select('id', 'name')
			->get();

		$data['pos'] = DB::table('program_outcomes')
			->select('id', 'name')
			->get();

		$data['id'] = DB::table('co_po_mappings')
			->select('id')
			->where('subject_id', $request->subject_id)
			->get();

		//dd($data['id'][0]);

		$j = 0;
		foreach ($data['id'] as $key => $ids) {
			$arrayofid[$j] = $ids->id;
			$j++;

		}

		$isExists = DB::table('co_po_mappings')->where('subject_id', $request->subject_id)->exists();

		$i = 0;

		if ($isExists == true) {
			foreach ($data['cos'] as $key => $co) {
				foreach ($data['pos'] as $key => $po) {
					$store = DB::table('co_po_mappings')->where('id', $data['id'][$i]->id
					)->update(['ratings' => (int) $request->ratings[$i],
					]);
					$i++;
				}
			}

			for ($i = 0; $i < count($data['pos']); $i++) {

				$ct[] = DB::table('co_po_mappings')
					->where('ratings', '!=', 'null')
					->where('po_id', $data['pos'][$i]->id)
					->get()
					->toArray();

				$rates[] = DB::table('co_po_mappings')
					->where('ratings', '!=', 'null')
					->where('po_id', $data['pos'][$i]->id)
					->sum('ratings');

				count($ct[$i]) == 0 ? $avgpo[$i] = 0 : $avgpo[$i] = (float) $rates[$i] / count($ct[$i]);

				$sumForPo = 0;
				$multiplicationForPo = 0;
				for ($j = 0; $j < count($data['cos']); $j++) {
					$sum[$j] = CoPoMapping::
						where('po_id', $data['pos'][$i]->id)
						->where('co_id', $data['cos'][$j]->id)
						->first(["ratings"]);

					$sumForPo = $sumForPo + $sum[$j]->ratings;

					$mul[$j] = Level::where('level', $sum[$j]->ratings)->first(['multiplication']);
					$multiplicationForPo = $multiplicationForPo + $mul[$j]->multiplication;

				}
				$weighted_avg[$i] = (double) ($sumForPo != 0) ? $multiplicationForPo / $sumForPo : 0;

				$store = DB::table('subject_po_avgs')
					->where('subject_id', $request->subject_id)
					->where("po_id", $data["pos"][$i]->id)
					->update(
						[
							'avg' => $avgpo[$i],
							'weighted_avg' => round($weighted_avg[$i], 2),
						]);

			}

		}

		return redirect()->back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request) {

		//$data = array();
		//$data['ratings'] =

		DB::table('co-po-mappings')->where('id', $request->id)->update($data);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
