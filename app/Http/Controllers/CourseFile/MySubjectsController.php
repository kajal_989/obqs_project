<?php

namespace App\Http\Controllers\CourseFile;

use App\Http\Controllers\Controller;
use Auth;
use Crypt;
use DB;
use Illuminate\Http\Request;

class MySubjectsController extends Controller {
	public function index() {
		$data['subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->select('subjects.id', 'subjects.name as subject_name', 'subjects.course_code', 'semisters.short_name', 'subjects.slug as slug')
			->where('theory_teacher', Auth::user()->id)
			->get();

		//dd($data);
		return view('course.my-subjects', $data);
	}

	public function view(Request $request) {
		$slug = Crypt::decrypt($request->slug);
		$data['subject'] = DB::table('subjects')->where('slug', $slug)->first();
		return view('course.view-my-subject', $data);
	}
}
