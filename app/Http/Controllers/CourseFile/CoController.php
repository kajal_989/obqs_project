<?php

namespace App\Http\Controllers\CourseFile;

use App\Http\Controllers\Controller;
use App\models\CourseOutcome;
use Carbon\Carbon;
use Crypt;
use DB;
use Illuminate\Http\Request;

class CoController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

	}

	public function coindex(Request $request) {
		try {

			$slug = Crypt::decrypt($request->slug);
			//dd($id);
			$data['sub'] = DB::table('subjects')
				->where('slug', '=', $slug)
				->first();
			$data['costatements'] = CourseOutcome::where('subject_id', '=', $data['sub']->id)
				->get();

			return view('course.costatements', $data);

		} catch (\Exception $e) {
			return redirect()->back();
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//dd($request->all());
		$data = [];
		$now = Carbon::now();

		for ($i = 0; $i < count($request->name); $i++) {
			$data[] = [
				"subject_id" => $request->subject_id,
				"name" => $request->name[$i],
				"statement" => $request->statement[$i],
				"created_at" => $now,
				"updated_at" => $now,
			];

			//DB::table('course_outcomes')->insert($data);

		}
		DB::table('course_outcomes')->insert($data);

		//dd($data);

		//session()->flash('success', 'Statement Added successfully');
		$forcopo = [];
		$acyear = DB::table('acadmic_years')->where('is_active', '=', '1')->first();
		$subjects = DB::table('subjects')
		//	->join('course_outcomes', 'subjects.id', '=', 'course_outcomes.subject_id')
			->where('acadmic_year_id', '=', $acyear->id)
			->select('id')
			->get();
		//	dd($subjects);
		$poids = DB::table('program_outcomes')
		//	->join('course_outcomes', 'subjects.id', '=', 'course_outcomes.subject_id')

			->select('id')
			->get();

		for ($i = 0; $i < count($subjects); $i++) {
			$coids = DB::table('course_outcomes')
				->where('subject_id', '=', $subjects[$i]->id)
				->get();
			for ($j = 0; $j < count($coids); $j++) {

				for ($k = 0; $k < count($poids); $k++) {

					$isExists = DB::table('co_po_mappings')
						->where('subject_id', '=', $subjects[$i]->id)
						->where('co_id', '=', $coids[$j]->id)
						->where('po_id', '=', $poids[$k]->id)
						->exists();
					if ($isExists == true) {
						continue;
					}
					$forcopo[] = [
						"subject_id" => $subjects[$i]->id,
						"co_id" => $coids[$j]->id,
						"po_id" => $poids[$k]->id,
					];

				}
			}
		}

		//dd($forcopo);
		$copomapp = DB::table('co_po_mappings')->insert($forcopo);

		$data['sub'] = DB::table('subjects')
			->where('id', '=', $request->subject_id)
			->first();
		$data['costatements'] = DB::table('course_outcomes')
			->where('subject_id', '=', $request->subject_id)
			->get();

		return redirect()->back()->with('success', 'CO Statements added successfu');

	}

	public function deleteCO(Request $request) {
		//$slug = Crypt::decrypt($request->slug);

		//dd($request->all());

		$co = CourseOutcome::where('id', $request->id)->delete();

		//	$delete = CourseOutcome::delete('delete from course_outcomes where id = ?', (int) $request->id);
		if ($co == true) {
			return redirect()->back()->with('success', 'Statement deleted successfully');
		} else {
			return redirect()->back()->with('error', 'Statement deleted not deleted');
		}

		//	return redirect()->back();

		// $data['sub'] = DB::table('subjects')
		// 	->where('id', '=', $request->subject_id)
		// 	->first();

		// $data['costatements'] = DB::table('course_outcomes')
		// 	->where('subject_id', '=', $request->subject_id)
		// 	->get();

		// return response()->json(['success' => 'Academic deleted successfully',
		// 	'url' => url()->route('co-statements')], 200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
