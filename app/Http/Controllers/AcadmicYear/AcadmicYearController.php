<?php

namespace App\Http\Controllers\AcadmicYear;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class AcadmicYearController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

	public function __construct() {
		$this->middleware('auth');
	}
	public function index() {

		$data['ac'] = DB::table('acadmic_years')->where('is_active', '=', '1')->first();

		$data['all'] = DB::table('acadmic_years')->get();

		return view('admin.ac', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {

		dd("yes");
	}

	public function insert(Request $request) {
		$query1 = DB::table('acadmic_years')->where('from_year', '=', $request->from)->first();
		if (empty($query1)) {
			$query2 = DB::table('acadmic_years')->insert(
				[
					'from_year' => $request->from,
					'to_year' => $request->to,
					'is_active' => '0',
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				]);
		} else {
			\Session::flash('error', 'Failed to add the year because Acadmic year already exists :(');
			return redirect()->back();
		}

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$query1 = DB::table('acadmic_years')->where('id', $request->id)->update(['is_active' => '1']);

		$query2 = DB::table('acadmic_years')->where('id', '!=', $request->id)->update(['is_active' => '0']);
		\Session::flash('message', 'Successfully updated!');
		return redirect()->back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
