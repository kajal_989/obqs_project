<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest {

	public function wantsJson() {
		if ($this->ajax()) {
			return true;
		}
		if ($this->is('api/*')) {
			return true;
		}
	}
}
