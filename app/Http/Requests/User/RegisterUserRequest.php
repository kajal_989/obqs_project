<?php

namespace App\Http\Requests\User;

// use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Request;

class RegisterUserRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		// dd(request()->all());
		return [
			'name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|min:6|required_with:confirm|same:confirm',
			'unique_code' => 'required|exists:unique_codes,code',
			"confirm" => "min:6",
		];
	}
}
