<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class YearSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::disableForeignKeyConstraints();

		DB::table('years')->truncate();

		DB::table('years')->insert([
			[
				'short_name' => 'SE',
				"name" => 'Second Year',
				'slug' => str_slug('Second Year'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'short_name' => 'TE',
				"name" => 'Third Year',
				'slug' => str_slug('Third Year'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'short_name' => 'BE',
				"name" => 'Fourth Year',
				'slug' => str_slug('Fourth Year'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

		]);

		Schema::enableForeignKeyConstraints();
	}
}
