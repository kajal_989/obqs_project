<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class AcadmicYearSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::disableForeignKeyConstraints();

		DB::table('acadmic_years')->truncate();
		DB::table('acadmic_years')->insert(
			[
				'from_year' => '2017',
				"to_year" => '2018',
				"is_active" => '1',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'from_year' => '2018',
				"to_year" => '2019',
				"is_active" => '0',
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			]
		);
	}
}
