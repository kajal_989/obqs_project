<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class SemisterSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::disableForeignKeyConstraints();

		DB::table('semisters')->truncate();

		DB::table('semisters')->insert([

			[
				'short_name' => 'III',
				"name" => 'Third',
				'slug' => str_slug('Third'),
				'year_id' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				'short_name' => 'IV',
				"name" => 'Fourth',
				'slug' => str_slug('Fourth'),
				'year_id' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'short_name' => 'V',
				"name" => 'Fifth',
				'slug' => str_slug('Fifth'),
				'year_id' => 2,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'short_name' => 'VI',
				"name" => 'Sixth',
				'slug' => str_slug('Sixth'),
				'year_id' => 2,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'short_name' => 'VII',
				"name" => 'Seventh',
				'slug' => str_slug('Seventh'),
				'year_id' => 3,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'short_name' => 'VIII',
				"name" => 'Eighth',
				'slug' => str_slug('Eighth'),
				'year_id' => 3,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

		]);

		Schema::enableForeignKeyConstraints();

	}
}
