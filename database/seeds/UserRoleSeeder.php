<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$now = Carbon::now();
		Schema::disableForeignKeyConstraints();

		DB::table('role_user')->truncate();

		DB::table('role_user')->insert([
			[
				"user_id" => 1,
				"role_id" => 1,
			],
			[
				"user_id" => 2,
				"role_id" => 2,
			],
			[
				"user_id" => 3,
				"role_id" => 2,
			],
		]);

		Schema::enableForeignKeyConstraints();
	}
}
