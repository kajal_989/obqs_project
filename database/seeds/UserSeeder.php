<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		Schema::disableForeignKeyConstraints();

		User::truncate();

		$admin = $this->userCreate([

			"name" => 'System Admin',
			'email' => 'admin@obqas.com',
			'password' => bcrypt('secret'),
			'uid' => 1,
			'department_id' => 1,
			'slug' => str_slug('admin@obqas.commin'),

		]);

		$admin->attachRole($this->role("admin"));

		$subjectTeacherOne = $this->userCreate([

			"name" => 'Shankar Patil',
			'email' => 'kajalgaikwad989@gmail.com',
			'password' => bcrypt('secret'),
			'uid' => 2,
			'department_id' => 1,
			'slug' => str_slug('Shankar Patil'),

		]);

		$subjectTeacherOne->attachRole($this->role("subject_teacher"));

		$subjectTeacherTwo = $this->userCreate([

			"name" => 'Vijay Patil',
			'email' => '98kajalg@gmail.com',
			'password' => bcrypt('secret'),
			'uid' => 3,
			'department_id' => 1,
			'slug' => str_slug('Vijay Patil'),
		]);

		$subjectTeacherTwo->attachRole($this->role("subject_teacher"));

		Schema::enableForeignKeyConstraints();

	}

	# To Create user
	public function userCreate($data) {

		return User::create($data);
	}

	# To fetch Role

	public function role($name) {

		return Role::where("name", $name)->first();
	}
}
