<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		$now = Carbon::now();
		Schema::disableForeignKeyConstraints();

		DB::table('roles')->truncate();

		DB::table('roles')->insert([
			[
				"name" => snake_case("Admin"),
				"display_name" => "Admin",
				"slug" => str_slug("admin"),
				"created_at" => $now,
				"updated_at" => $now,
			],
			[
				"name" => snake_case("Subject Teacher"),
				"display_name" => "Subject Teacher",
				"slug" => str_slug("Subject Teacher"),
				"created_at" => $now,
				"updated_at" => $now,
			],
		]);

		Schema::enableForeignKeyConstraints();
	}
}
