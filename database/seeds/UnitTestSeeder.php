<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class UnitTestSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		Schema::disableForeignKeyConstraints();

		DB::table('unit_tests')->truncate();

		DB::table('unit_tests')->insert([
			[
				'number' => '1',
				"name" => 'One',

			],
			[
				'number' => '2',
				"name" => 'Two',

			],

		]);

		Schema::enableForeignKeyConstraints();
	}
}
