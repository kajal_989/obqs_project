<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		//$this->call(UniqueCodeSeeder::class);
		$this->call(DepartmentSeeder::class);
		//	$this->call(UserSeeder::class);
		$this->call(YearSeeder::class);
		$this->call(SemisterSeeder::class);
		//$this->call(SubjectSeeder::class);
		//	$this->call(ProgramOutcomeSeeder::class);
		$this->call(AcadmicYearSeeder::class);
		$this->call(RoleTableSeeder::class);
		$this->call(UserRoleSeeder::class);
		$this->call(LevelSeeder::class);
	}
}
