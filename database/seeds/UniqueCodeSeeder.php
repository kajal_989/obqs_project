<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UniqueCodeSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		Schema::disableForeignKeyConstraints();

		DB::table('unique_codes')->truncate();

		DB::table('unique_codes')->insert([
			[
				"code" => "ADMIN",
				"short_name" => "Admin",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			],
			[
				"code" => "IT0001",
				"short_name" => "ITA",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			],
			[
				"code" => "IT0002",
				"short_name" => "ITB",
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now(),
			],
		]);

		Schema::enableForeignKeyConstraints();

	}
}
