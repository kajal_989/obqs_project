<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DepartmentSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::disableForeignKeyConstraints();

		DB::table('departments')->truncate();

		DB::table('departments')->insert([
			[
				"name" => 'Information technology',
				'short_name' => 'IT',
				'slug' => str_slug('Information technology'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'Computer Engineering',
				'short_name' => 'IT',
				'slug' => str_slug('Computer Engineering'),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

		]);

		Schema::enableForeignKeyConstraints();
	}
}
