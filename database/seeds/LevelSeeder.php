<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class LevelSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::disableForeignKeyConstraints();

		DB::table('levels')->truncate();

		DB::table('levels')->insert([
			[
				"level" => 1,
				'percent' => 33.33,
				'acadmic_year_id' => '1',
				'multiplication' => 1 * 33.33,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"level" => 2,
				'percent' => 66.66,
				'acadmic_year_id' => '1',
				'multiplication' => 2 * 66.66,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"level" => 3,
				'percent' => 100,
				'acadmic_year_id' => '1',
				'multiplication' => 3 * 100,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

		]);

		Schema::enableForeignKeyConstraints();

	}
}
