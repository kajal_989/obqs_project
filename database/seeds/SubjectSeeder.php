<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class SubjectSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {

		Schema::disableForeignKeyConstraints();

		DB::table('subjects')->truncate();

		DB::table('subjects')->insert([
			[
				'course_code' => 'TEIT601',
				"name" => 'Software Engineering',
				'short_name' => 'SE',
				'practical_oral_marks' => 25,
				'internal_marks' => 20,
				'term_works' => 25,
				'theory_marks' => 80,
				'total_marks' => 150,
				'is_elective' => 0,
				'department_id' => 1,
				'semister_id' => 6,
				'slug' => str_slug('Software Engineering'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'course_code' => 'TEIT602',
				"name" => 'Advance Internet Technology',
				'short_name' => 'AIT',
				'practical_oral_marks' => 25,
				'internal_marks' => 20,
				'term_works' => 25,
				'theory_marks' => 80,
				'total_marks' => 150,
				'is_elective' => 0,
				'department_id' => 1,
				'semister_id' => 6,
				'slug' => str_slug('Advance Internet Technology'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				'course_code' => 'TEIT603',
				"name" => 'Distributed Systems',
				'short_name' => 'DS',
				'practical_oral_marks' => 25,
				'internal_marks' => 20,
				'term_works' => 25,
				'theory_marks' => 80,
				'total_marks' => 150,
				'is_elective' => 0,
				'department_id' => 1,
				'semister_id' => 6,
				'slug' => str_slug('Distributed Systems'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

		]);

		Schema::enableForeignKeyConstraints();

	}
}
