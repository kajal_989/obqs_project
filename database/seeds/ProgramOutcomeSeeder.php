<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class ProgramOutcomeSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Schema::disableForeignKeyConstraints();

		DB::table('program_outcomes')->truncate();

		DB::table('program_outcomes')->insert([
			[
				"name" => 'PO1',
				'statement' => 'Engineering knowledge: Apply the knowledge of mathematics, science, engineering fundamentals, and an engineering specialization to the solution of complex engineering problems.',
				'department_id' => 1,
				'slug' => str_slug('PO1'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO2',
				'statement' => 'Problem analysis: Identify, formulate, review research literature, and analyze complex engineering problems reaching substantiated conclusions using first principles of mathematics, natural sciences, and engineering sciences.',
				'department_id' => 1,
				'slug' => str_slug('PO2'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO3',
				'statement' => 'Design/development of solutions: Design solutions for complex engineering problems and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety, and the cultural, societal, and environmental considerations.',
				'department_id' => 1,
				'slug' => str_slug('PO3'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"name" => 'PO4',
				'statement' => 'Conduct investigations of complex problems: Use research-based knowledge and research methods including design of experiments, analysis and interpretation of data, and synthesis of the information to provide valid conclusions.',
				'department_id' => 1,
				'slug' => str_slug('PO4'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO5',
				'statement' => 'Modern tool usage: Create, select, and apply appropriate techniques, resources, and modern engineering and IT tools including prediction and modeling to complex engineering activities with an understanding of the limitations.',
				'department_id' => 1,
				'slug' => str_slug('PO5'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO6',
				'statement' => 'The engineer and society: Apply reasoning informed by the contextual knowledge to assess societal, health, safety, legal and cultural issues and the consequent responsibilities relevant to the professional engineering practice.',
				'department_id' => 1,
				'slug' => str_slug('PO6'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO7',
				'statement' => 'Design/development of solutions: Design solutions for complex engineering problems and design system components or processes that meet the specified needs with appropriate consideration for the public health and safety, and the cultural, societal, and environmental considerations.',
				'department_id' => 1,
				'slug' => str_slug('PO7'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO8',
				'statement' => 'Environment and sustainability: Understand the impact of the professional engineering solutions in societal and environmental contexts, and demonstrate the knowledge of, and need for sustainable development.',
				'department_id' => 1,
				'slug' => str_slug('PO8'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"name" => 'PO9',
				'statement' => 'Ethics: Apply ethical principles and commit to professional ethics and responsibilities and norms of the engineering practice.',
				'department_id' => 1,
				'slug' => str_slug('PO9'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO10',
				'statement' => 'Individual and team work: Function effectively as an individual, and as a member or leader in diverse teams, and in multidisciplinary settings.',
				'department_id' => 1,
				'slug' => str_slug('PO10'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"name" => 'PO11',
				'statement' => 'Communication: Communicate effectively on complex engineering activities with the engineering community and with society at large, such as, being able to comprehend and write effective reports and design documentation, make effective presentations, and give and receive clear instructions.',
				'department_id' => 1,
				'slug' => str_slug('PO11'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],
			[
				"name" => 'PO12',
				'statement' => 'Project management and finance: Demonstrate knowledge and understanding of the engineering and management principles and apply these to one’s own work, as a member and leader in a team, to manage projects and in multidisciplinary environments',
				'department_id' => 1,
				'slug' => str_slug('PO12'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"name" => 'PO13',
				'statement' => 'Life-long learning: Recognize the need for, and have the preparation and ability to engage in independent and life-long learning in the broadest context of technological change.',
				'department_id' => 1,
				'slug' => str_slug('PO13'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"name" => 'PSO1',
				'statement' => ' Apply current trends, technologies and practices to provide Information Technology Solutions',
				'department_id' => 1,
				'slug' => str_slug('PSO1'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

			[
				"name" => 'PSO2',
				'statement' => 'Inculcate the knowledge as developer, analyst, tester and administrator in the field of Information Technology.',
				'department_id' => 1,
				'slug' => str_slug('PSO2'),
				'created_by' => 1,
				'updated_by' => 1,
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			],

		]);

		Schema::enableForeignKeyConstraints();
	}
}
