<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionBanksTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('question_banks', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('question')->nullable();
			$table->integer('marks')->nullable();
			$table->integer('chapter_no')->nullable();
			$table->unsignedBigInteger('test_id')->index()->nullable();
			$table->unsignedBigInteger('subject_id')->index()->nullable();
			$table->unsignedBigInteger('co_id')->index()->nullable();

			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('test_id')
				->references('id')
				->on('unit_tests')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('subject_id')
				->references('id')
				->on('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('co_id')
				->references('id')
				->on('course_outcomes')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('question_banks');
	}
}
