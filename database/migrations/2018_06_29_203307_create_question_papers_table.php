<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionPapersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('question_papers', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('question_name')->nullable();
			$table->unsignedBigInteger('co_id')->index()->nullable();
			$table->unsignedBigInteger('question_id')->index()->nullable();
			$table->unsignedBigInteger('subject_id')->index()->nullable();
			$table->unsignedBigInteger('test_id')->index()->nullable();
			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();
			$table->double("percent_student", 8, 2)->nullable();

			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('co_id')
				->references('id')
				->on('course_outcomes')
				->onUpdate('cascade')
				->onDelete('cascade');
			$table->foreign('question_id')
				->references('id')
				->on('question_banks')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('subject_id')
				->references('subject_id')
				->on('question_banks')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('test_id')
				->references('test_id')
				->on('question_banks')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('question_papers');
	}
}
