<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectPoAvgsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('subject_po_avgs', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('subject_id')->index()->nullable();
			$table->unsignedBigInteger('po_id')->index()->nullable();
			$table->float('avg', 4, 2)->nullable();
			$table->float('weighted_avg', 4, 2)->nullable();
			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('subject_id')
				->references('id')
				->on('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('po_id')
				->references('id')
				->on('program_outcomes')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('subject_po_avgs');
	}
}
