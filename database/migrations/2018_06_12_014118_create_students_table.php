<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('students', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('roll_no')->nullable();
			$table->string('student_name')->nullable();
			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();

			$table->unsignedBigInteger('semister_id')->index()->nullable();
			$table->unsignedBigInteger('department_id')->index()->nullable();
			$table->tinyInteger('batch_id')->nullable();
			$table->unsignedBigInteger('elective_subject_id')->index()->nullable();

			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('semister_id')
				->references('id')
				->on('semisters')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('department_id')
				->references('id')
				->on('departments')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('elective_subject_id')
				->references('id')
				->on('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('students');
	}
}
