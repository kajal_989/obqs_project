<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('images', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name')->nullable();
			$table->string('type')->nullable();
			$table->string('extension')->nullable();
			$table->string('size')->nullable();
			$table->string('path')->nullable();
			$table->string('original_image')->nullable();
			$table->string('thumbnail_image')->nullable();
			$table->string('description')->nullable();
			$table->string('summary')->nullable();
			$table->json('more_info')->nullable();
			$table->string('slug')->unique()->nullable();
			$table->unsignedInteger('imagable_id')->nullable();
			$table->string('imagable_type')->nullable();
			$table->unsignedBigInteger('user_id')->nullable();
			$table->unsignedBigInteger('causer_id')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');;
			$table->foreign('causer_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');;

			$table->index('imagable_id');
			$table->index('imagable_type');
			$table->index('user_id');
			$table->index('causer_id');
			$table->index('slug');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('images');
	}
}
