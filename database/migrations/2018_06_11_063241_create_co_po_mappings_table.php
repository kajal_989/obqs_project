<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoPoMappingsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('co_po_mappings', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('subject_id')->index()->nullable();
			$table->unsignedBigInteger('co_id')->index()->nullable();
			$table->unsignedBigInteger('po_id')->index()->nullable();
			$table->tinyInteger('ratings')->index()->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->foreign('subject_id')
				->references('id')
				->on('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('co_id')
				->references('id')
				->on('course_outcomes')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('po_id')
				->references('id')
				->on('program_outcomes')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('co_po_mappings');
	}
}
