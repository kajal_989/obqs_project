<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTestMarksTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('unit_test_marks', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();
			$table->unsignedBigInteger('subject_id')->index()->nullable();
			$table->unsignedBigInteger('student_id')->index()->nullable();
			$table->unsignedBigInteger('test_id')->index()->nullable();
			$table->string('one_a')->nullable();
			$table->string('one_b')->nullable();
			$table->string('one_c')->nullable();
			$table->string('one_d')->nullable();
			$table->string('one_e')->nullable();
			$table->string('one_f')->nullable();
			$table->string('two_a')->nullable();
			$table->string('two_b')->nullable();
			$table->string('three_a')->nullable();
			$table->string('three_b')->nullable();
			$table->string('total')->nullable();

			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('subject_id')
				->references('id')
				->on('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('student_id')
				->references('id')
				->on('students')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('test_id')
				->references('id')
				->on('unit_tests')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('unit_test_marks');
	}
}
