<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassAdvisersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('class_advisers', function (Blueprint $table) {
			$table->bigIncrements('id');

			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();
			$table->unsignedBigInteger('semister_id')->index()->nullable();
			$table->unsignedBigInteger('user_id')->index()->nullable();

			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();
			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('semister_id')
				->references('id')
				->on('semisters')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('user_id')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('class_advisers');
	}
}
