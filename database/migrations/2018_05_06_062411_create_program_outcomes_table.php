<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramOutcomesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('program_outcomes', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name')->nullable();
			$table->text('statement')->nullable();
			$table->unsignedBigInteger('department_id')->index()->nullable();
			$table->string('slug')->unique()->nullable();

			$table->timestamps();
			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('department_id')
				->references('id')
				->on('departments')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('program_outcomes');
	}
}
