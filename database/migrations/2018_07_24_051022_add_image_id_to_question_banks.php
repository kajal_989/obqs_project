<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageIdToQuestionBanks extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('question_banks', function (Blueprint $table) {

			$table->unsignedBigInteger("image_id")->after("co_id")->index()->nullable();

			$table->foreign('image_id')->references('id')->on('images')->onUpdate('cascade')->onDelete('cascade');;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('question_banks', function (Blueprint $table) {
			$table->dropColumn("image_id");
		});
	}
}
