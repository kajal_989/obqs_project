<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('subjects', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('course_code')->nullable();
			$table->string('name')->nullable();
			$table->string('short_name')->nullable();
			$table->string('subject_type')->nullable();
			$table->integer('practical_oral_marks')->nullable();
			$table->integer('term_works')->nullable();
			$table->integer('threshold')->nullable();
			$table->integer('internal_marks')->nullable();
			$table->integer('theory_marks')->nullable();
			$table->integer('total_marks')->nullable();
			$table->tinyInteger('is_elective')->nullable();
			$table->string('slug')->unique()->nullable();

			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();
			$table->unsignedBigInteger('semister_id')->index()->nullable();
			$table->unsignedBigInteger('department_id')->index()->nullable();

			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('semister_id')
				->references('id')
				->on('semisters')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('department_id')
				->references('id')
				->on('departments')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('subjects');
	}
}
