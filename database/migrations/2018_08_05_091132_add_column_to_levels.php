<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToLevels extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('levels', function (Blueprint $table) {
			$table->double("multiplication")->nullable();
			$table->text("more_info")->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('levels', function (Blueprint $table) {
			$table->dropColumn("multiplication");
			$table->dropColumn("more_info");
		});
	}
}
