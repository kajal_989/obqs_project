<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name')->nullable();
			$table->string('email')->unique()->nullable();
			$table->string('password')->nullable();
			$table->string("contact_no")->nullable();
			$table->text("address")->nullable();
			$table->text('qualification')->nullable();
			$table->rememberToken();

			$table->unsignedBigInteger("uid")->unique()->index()->nullable();
			$table->unsignedBigInteger("department_id")->index()->nullable();

			$table->string("slug")->index()->nullable();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('uid')
				->references('id')
				->on('unique_codes')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('department_id')
				->references('id')
				->on('departments')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('users');
	}
}
