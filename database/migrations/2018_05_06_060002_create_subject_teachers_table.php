<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubjectTeachersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('subject_teachers', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();
			$table->unsignedBigInteger('subject_id')->index()->nullable();
			$table->unsignedBigInteger('theory_teacher')->index()->nullable();
			$table->unsignedBigInteger('practical_teacher')->index()->nullable();

			$table->tinyInteger('isactive')->nullable();
			$table->timestamps();

			$table->unsignedBigInteger('created_by')->index()->nullable();
			$table->unsignedBigInteger('updated_by')->index()->nullable();

			$table->softDeletes();

			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('subject_id')
				->references('id')
				->on('subjects')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('theory_teacher')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('practical_teacher')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('created_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

			$table->foreign('updated_by')
				->references('id')
				->on('users')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('subject_teachers');
	}
}
