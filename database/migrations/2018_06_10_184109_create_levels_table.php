<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('levels', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('level')->nullable();
			$table->float('percent')->nullable();
			$table->unsignedBigInteger('acadmic_year_id')->index()->nullable();
			$table->timestamps();

			$table->foreign('acadmic_year_id')
				->references('id')
				->on('acadmic_years')
				->onUpdate('cascade')
				->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('levels');
	}
}
