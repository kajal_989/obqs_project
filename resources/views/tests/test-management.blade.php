@extends('layouts.admin_layout')
@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>My Subjects </h2>
                        </div>
                        <div class="body">
                                    <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                           <thead>
                              <tr>
                                  <th>SERIAL NO</th>
                                  <th>Subject Name</th>
                                  <th><center>Test ONE</center></th>
                                   <th><center>Test TWO</center></th>
                              </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                 <th>SERIAL NO</th>
                                  <th>Subject Name</th>
                                <th><center>Test ONE</center></th>
                                   <th><center>Test TWO</center></th>
                                </tr>
                           </tfoot>
                          <tbody>
                         {{ csrf_field() }}
                           @foreach($subject_list as $subject )
                              <tr>
                               <td>{{ $loop->iteration }}</td>
                               <td> {{$subject->subject_name}}</td>
                             <td>
<center>
                          <form method="post" action="{{ route('question-bank') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
                              <input type="hidden" name="test_id" value="1">
            <input type="submit" class="btn bg-teal waves-effect"  value="Question Bank" />
            </form><br>
             <form method="post" action="{{ route('co-po-mappings') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
                              <input type="hidden" name="test_id" value="1">
             <input type="submit" class="btn bg-teal waves-effect" value="Question Paper" />
           </form><br>
             <form method="post" action="{{ route('co-po-mappings') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
                              <input type="hidden" name="test_id" value="1">
             <input type="submit" class="btn bg-teal waves-effect" value="Test  Marks" />
           </form>
         </center>
          </td>

           <td>
<center>
                  <form method="post" action="{{ route('question-bank') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
                              <input type="hidden" name="test_id" value="2">
            <input type="submit" class="btn bg-teal waves-effect"  value="Question Bank" />
            </form><br>
             <form method="post" action="{{ route('co-po-mappings') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
                              <input type="hidden" name="test_id" value="2">
             <input type="submit" class="btn bg-teal waves-effect" value="Question Paper" />
           </form><br>
             <form method="post" action="{{ route('co-po-mappings') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
                              <input type="hidden" name="test_id" value="2">
             <input type="submit" class="btn bg-teal waves-effect" value="Test  Marks" />
           </form>
         </center>
          </td>
                                 </tr>
                           @endforeach
                     </tbody>
                    </table>
                </div>

               </div>
             </div>
          </div>
        </div>
    </div>
  </div>
 </div>

 </section>
@endsection