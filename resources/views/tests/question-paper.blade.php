@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Add New Questions</h2>
            </div>
              <div class="body">
                 @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1"> View Question Paper
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">
                         <form method="get" action="{{ route('view-test-paper') }}">
                             {{ csrf_field() }}
                          <div class="row clearfix">
                            <div class="col-sm-3">
                              <select class="form-control" name = "subject_id" required>
                                <option value="">--select your subject--</option>
                                  @foreach($subjects as $subject)
                                  <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="col-sm-3">
                              <select class="form-control" name = "test_id" required>
                                <option value="">--select unit test--</option>
                                  @foreach($tests as $test)
                                  <option value="{{$test->id}}">{{$test->name}}</option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="col-sm-3">
                              <input type="submit" class="btn btn-primary" name="submit" value="View Question Paper"/>
                            </div>
                             <div class="col-sm-3">
                              <input type="submit" class="btn btn-info" name="submit" value="Download Paper"/>
                            </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>

                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_2">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseOne_2" aria-expanded="false" aria-controls="collapseOne_2"> Generate Question Paper
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_2" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_2">
                      <div class="panel-body">
                          <form method="post" class="form-horizontal" action="{{ route('store-question-paper') }}">
                             {{ csrf_field() }}

                          <div class="row clearfix">

                            <div class="col-sm-4">
                              <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                              <select class="form-control" id="subject_id" name = "subject_id" onchange="myfun()" required>
                                <option value="">--select your subject--</option>
                                  @foreach($subjects as $subject)
                                  <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                                  @endforeach
                              </select>
                               </div>
                             <div class="col-sm-4">
                            <select class="form-control" id="test_id" name = "test_id" required>
                                <option value="">--select unit test--</option>
                                  @foreach($tests as $test)
                                  <option value="{{$test->id}}">{{$test->name}}</option>
                                  @endforeach
                              </select>
                            </div>

                     <div class="col-sm-4">
                <h4>Accadmic Year {{$acyear->from_year}} - {{$acyear->to_year}} </h4>
            </div>
        </div>
        <div class="table-responsive">

         <table class="table table-hover">
             <thead>
            <tr>
                 <td>Question No.</td>
                 <td>Chapter No</td>
             </tr>
             </thead>
             <tbody>
             <tr>
                <td> <input type="hidden" name="question_name[]" value="one_a"> Q. 1 <b>(a)</b></td>
                <td>
                    <input type="hidden" name="marks[]" value="2">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td> <input type="hidden" name="question_name[]" value="one_b"> Q. 1 <b>(b)</b></td>
                <td>
                     <input type="hidden" name="marks[]" value="2">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td><input type="hidden" name="question_name[]" value="one_c"> Q. 1 <b>(c)</b></td>
                <td>
                     <input type="hidden" name="marks[]" value="2">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>
                  <tr>
                <td><input type="hidden" name="question_name[]" value="one_d"> Q. 1 <b>(d)</b></td>
                <td>
                     <input type="hidden" name="marks[]" value="2">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>
                  <tr>
                <td><input type="hidden" name="question_name[]" value="one_e"> Q. 1 <b>(e)</b></td>
                <td>
                     <input type="hidden" name="marks[]" value="2">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td><input type="hidden" name="question_name[]" value="one_f"> Q. 1 <b>(f)</b></td>
                <td>
                      <input type="hidden" name="marks[]" value="2">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td><input type="hidden" name="question_name[]" value="two_a"> Q. 2 <b>(a)</b></td>
                <td>
                      <input type="hidden" name="marks[]" value="5">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td><input type="hidden" name="question_name[]" value="two_b"> Q. 2 <b>(b)</b></td>
                <td>
                     <input type="hidden" name="marks[]" value="5">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td><input type="hidden" name="question_name[]" value="three_a"> Q. 3 <b>(a)</b></td>
                <td>
                     <input type="hidden" name="marks[]" value="5">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>

                  <tr>
                <td> Q. 3 <b>(b)</b></td>
                <td><input type="hidden" name="question_name[]" value="three_b">
                     <input type="hidden" name="marks[]" value="5">
                    <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                 </td>
                 </tr>
            </tbody>
         </table>
        <center> <input class="btn btn-success " type="submit" name="submit" value="Confirm" ></center>
     </form>
    </div>
</div>
</div>
</div>
 </div>
 </div>
</div>
</div>
</div>

<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

 <script type="text/javascript">

function myfun() {
    var subject_id =document.getElementById('subject_id').value;
    console.log(subject_id);

    $.ajax({
             type: 'post',
             url: '{{ route('select-co') }}',
            data:{
                    "subject_id":subject_id,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log(data);
                $('#co_id').children('option:not(:first)').remove();
                var html = '';
            for(var i = 0; i < data.length; i++){
console.log(data[i].id);
     var option=$('<option value='+data[i].id+'>'+data[i].name+'</option>');
  $('#co_id').append(option);

}

             }
              });
}
/*
   $("select[name='subject_id']").change(function(){
      var subject_id = $(this).val();
      var token = $("input[name='_token']").val();
      $.ajax({
          url: "test-management/select-co",
          method: 'POST',
          data: {subject_id:subject_id, _token:token},
          success: function(data) {
            $("select[name='co_id'").html('');
            $("select[name='co_id'").html(data.options);
          }
      });
  });

  */
  function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 20){             // limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var colCount = table.rows[0].cells.length;
            for(var i=0; i<colCount; i++) {
                var newcell = row.insertCell(i);
                newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                }
          }else{
           alert("maximum 20 rows are allowded.");

      }
  }

function deleteRow(tableID) {
     var table = document.getElementById(tableID);
     var rowCount = table.rows.length;
     for(var i=0; i<rowCount; i++) {
     var row = table.rows[i];
     var chkbox = row.cells[0].childNodes[0];
      if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all Rows.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
</script>

    </section>

@endsection