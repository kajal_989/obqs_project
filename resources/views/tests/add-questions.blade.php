@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Add New Questions</h2>
            </div>
              <div class="body">
                   @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
    @if (Session::has('error'))
          <div class="alert alert-danger">
        {{ session('success') }}
    </div>
    @endif

                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_2">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseOne_2" aria-expanded="false" aria-controls="collapseOne_2"> Create Question Bank
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_2" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_2">
                      <div class="panel-body">
                          <form method="post" class="form-horizontal" action="{{ route('store-questions') }}" enctype="multipart/form-data">
                             {{ csrf_field() }}

                          <div class="row clearfix">

                            <div class="col-sm-4">
                              <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                              <select class="form-control" id="subject_id" name = "subject_id" onchange="myfun()" required>
                                <option value="">--select your subject--</option>
                                  @foreach($subjects as $subject)
                                  <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                                  @endforeach
                              </select>
                               </div>
                             <div class="col-sm-4">
                            <select class="form-control" id="test_id" name = "test_id" required>
                                <option value="">--select unit test--</option>
                                  @foreach($tests as $test)
                                  <option value="{{$test->id}}">{{$test->name}}</option>
                                  @endforeach
                              </select>
                            </div>

                             <div class="col-sm-4">
                             <h4>Accadmic Year {{$acyear->from_year}} - {{$acyear->to_year}} </h4>
                            </div>

                             <div class="col-sm-12">

                  <div class="table-responsive">
                  <table id="dataTable" class="table dataTable">
                 <thead>
                      <th>select</th>
                    <th>Question</th>
                    <th>Chapter no</th>
                    <th>Marks</th>
                    <th>CO</th>
                 </thead>
                  <tbody>
                    <tr>
                    <p>

                    <td><input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked required="required"/>
                    </td>
                     <td>
                      <textarea class="form-control" name="question[]" required>

                      </textarea>
                    </td>

                      <td>
                     <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                    </td>
                      <td>
                       <select class="form-control" required="required" name="marks[]">
                        <option value="">Select Marks</option>
                       <option value="1">1</option>
                       <option value="2">2</option>
                       <option value="3">3</option>
                       <option value="4">4</option>
                       <option value="5">5</option>
                       <option value="6">6</option>
                       <option value="7">7</option>
                       <option value="8">8</option>
                       <option value="9">9</option>
                       <option value="10">10</option>
                     </select>
                   </td>
                    <td>
                     <select class="form-control" required="required" id="co_id" name="co_id[]">
                        <option value="">--please select co--</option>
                     </select>
                    </td>
                   </p>
                    </tr>
                    </tbody>
                </table>
                <br>
                  <p>
                     <input type="submit" name="submit" value="Save" class="btn btn-success"  />
                      <input type="button" value="Add New Record" class="btn btn-primary"  onClick="addRow('dataTable')" />
                      <input type="button" value="Remove Record" class="btn btn-danger" onClick="deleteRow('dataTable')"  />
                  </p>
              </div>
              <div class="clear"></div>
              </div>
              </div>
            </form>
                        </div>
                      </div>
                    </div>



                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_3">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseOne_3" aria-expanded="false" aria-controls="collapseOne_3"> Image Question
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_3" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_3">
                      <div class="panel-body">

                          <form method="post" class="form-horizontal" action="{{ route('store-img-question') }}" enctype="multipart/form-data">
                             {{ csrf_field() }}

                          <div class="row clearfix">

                            <div class="col-sm-4">
                              <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                              <select class="form-control" id="img_subject_id" name = "subject_id" onchange="imgfun()" required>
                                <option value="">--select your subject--</option>
                                  @foreach($subjects as $subject)
                                  <option value="{{$subject->id}}">{{$subject->subject_name}}</option>
                                  @endforeach
                              </select>
                               </div>
                             <div class="col-sm-4">
                            <select class="form-control" id="test_id" name = "test_id" required>
                                <option value="">--select unit test--</option>
                                  @foreach($tests as $test)
                                  <option value="{{$test->id}}">{{$test->name}}</option>
                                  @endforeach
                              </select>
                            </div>

                             <div class="col-sm-4">
                             <h4>Accadmic Year {{$acyear->from_year}} - {{$acyear->to_year}} </h4>
                            </div>

                             <div class="col-sm-12">

                  <div class="table-responsive">
                  <table id="dataTableImage" class="table dataTableImage">
                 <thead>
                  <th>select</th>
                    <th>Question</th>
                    <th>Chapter no</th>
                    <th>Marks</th>
                    <th>CO</th>
                    <th>Image</th>
                 </thead>
                  <tbody>
                    <tr>
                    <p>
                        <td><input type="checkbox" id="chkbox" checked required="required"/>
                    </td>
                     <td>
                      <textarea class="form-control"  name="question[]"></textarea>
                    </td>

                      <td>
                     <select class="form-control" required="required" name="chapter_no[]">
                      <option value="">Select Chapter no</option>
                       <option value="1">Chapter 1</option>
                       <option value="2">Chapter 2</option>
                       <option value="3">Chapter 3</option>
                       <option value="4">Chapter 4</option>
                       <option value="5">Chapter 5</option>
                       <option value="6">Chapter 6</option>
                       <option value="7">Chapter 7</option>
                       <option value="8">Chapter 8</option>
                       <option value="9">Chapter 9</option>
                       <option value="10">Chapter 10</option>
                     </select>
                    </td>
                      <td>
                       <select class="form-control" required="required" name="marks[]">
                        <option value="">Select Marks</option>
                       <option value="1">1</option>
                       <option value="2">2</option>
                       <option value="3">3</option>
                       <option value="4">4</option>
                       <option value="5">5</option>
                       <option value="6">6</option>
                       <option value="7">7</option>
                       <option value="8">8</option>
                       <option value="9">9</option>
                       <option value="10">10</option>
                     </select>
                   </td>
                    <td>
                     <select class="form-control" required="required" id="img_co_id" name="img_co_id[]">
                        <option value="">--please select co--</option>
                     </select>
                    </td>
                    <td><input type="file" name="img[]" multiple></td>
                   </p>
                    </tr>
                    </tbody>
                </table>
                <br>
                  <p>
                    <center> <input type="submit" name="submit" value="Save" class="btn btn-success"  /></center>
                    <input type="button" value="Add New Record" class="btn btn-primary"  onClick="addRow('dataTableImage')" />
                    <input type="button" value="Remove Record" class="btn btn-danger" onClick="deleteRow('dataTableImage')"  />
                  </p>
              </div>

              </div>
              </div>
            </form>
                        </div>
                      </div>
                    </div>




                  </div>
                </div>
              </div>
</div>
</div>
</div>

<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

 <script type="text/javascript">

function myfun() {
    var subject_id =document.getElementById('subject_id').value;
    console.log(subject_id);

    $.ajax({
             type: 'post',
             url: '{{ route('select-co') }}',
            data:{
                    "subject_id":subject_id,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log(data);
                $('#co_id').children('option:not(:first)').remove();
                var html = '';
            for(var i = 0; i < data.length; i++){
console.log(data[i].id);
     var option=$('<option value='+data[i].id+'>'+data[i].name+'</option>');
  $('#co_id').append(option);

}
}
});
}


function imgfun() {
    var subject_id =document.getElementById('img_subject_id').value;
    console.log(subject_id);

    $.ajax({
             type: 'post',
             url: '{{ route('select-co') }}',
            data:{
                    "subject_id":subject_id,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log(data);
                $('#img_co_id').children('option:not(:first)').remove();
                var html = '';
            for(var i = 0; i < data.length; i++){
console.log(data[i].id);
     var option=$('<option value='+data[i].id+'>'+data[i].name+'</option>');
  $('#img_co_id').append(option);

}
}
});
}


  function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 20){             // limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var colCount = table.rows[0].cells.length;
            for(var i=0; i<colCount; i++) {
                var newcell = row.insertCell(i);
                newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                }
          }else{
           alert("maximum 20 rows are allowded.");

      }
  }

function deleteRow(tableID) {
     var table = document.getElementById(tableID);
     var rowCount = table.rows.length;
     for(var i=0; i<rowCount; i++) {
     var row = table.rows[i];
     var chkbox = row.cells[0].childNodes[0];
      if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all Rows.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
</script>

    </section>

@endsection