<!DOCTYPE html>
<html>
<head>
	<title>Question paper</title>

  <style type="text/css">
  body{

    padding-right: 20px;

    padding-left: 20px;
  }
    table,th,td,tbody{

    border: 1px solid black;
    border-collapse :collapse;
    }

  </style>
</head>
<body>
	 <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <img src="{{public_path('/images/logo2.png')}}" hspace="20" height="100px" width="100px" align="right">
          <img src="{{public_path('/images/logo1.png')}}" hspace="20"" height="100px" width="100px"  align="left">

          <div class="header">
          <center>
               <h3>Bharti Vidyapeeth Collage of Engineering, Navi Mumbai</h3>
               <h4>Department Of Information Technology</h4>
          </center>
            </div>
            <hr>
              <div class="body">
                          <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <center>
                                   <h5>Unit Test:  {{$test->name}} </h5>
                                   <h5>Subject Name : {{$subject->name}}   <p align="right"> Total Marks: 20</p></h5>
                              </center>
                              <br>
                              <p>
                                   <h5>Instructions</h5>
                                   <ul>
                                        <li>Assume Suitable data wherever necessary & State it.</li>
                                        <li>Draw diagrams where are necessary.</li>
                                   </ul>
                              </p>
                              <br>
                              <br>
                     <div class="table-responsive">
                        <table class="table">
                             <tr>
                                  <th><b>Q. 1.</b></th>
                                  <th>Attempt any FIVE Questions.</th>
                                  <th>Marks</th>

                                  <th>CO</th>
                             </tr>

                               @for($i=0,$j=1; $i < 6 ; $i++,$j++)
                                  <tr>
                                      <td>{{$j}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}
                                         @if($questions[$i]->questionBank->image)
                                        <br>
                                        <img src="{{public_path($questions[$i]->questionBank->image->path)}}" height="200" width="300" />
                                        @endif

                                      </td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                              <tr>
                                   <td><b>Q.2</b></td>
                                   <td><b>Attempt Any ONE of the following</b></td>
                              </tr>
                                 @for($i=6,$j=1; $i < 8 ; $i++,$j++)
                                  <tr>
                                      <td>{{$j}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}
                                         @if($questions[$i]->questionBank->image)
                                        <br>
                                        <img src="{{public_path($questions[$i]->questionBank->image->path)}}"/>
                                        @endif
                                      </td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                              <tr>
                                   <td><b>Q.3</b></td>
                                   <td><b>Attempt Any ONE of the following</b></td>
                              </tr>
                                 @for($i=8 , $j=1; $i < 10 ; $i++ , $j++)
                                  <tr>
                                      <td>{{$j}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}
                                       @if($questions[$i]->questionBank->image)
                                        <br>
                                        <img src="{{public_path($questions[$i]->questionBank->image->path)}}"/>
                                        @endif
                                      </td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                             </tbody>
                        </table>
                         </div>



                        </div>

                      </div>
                    </div>
                  </div>

</div>

</div>

</body>
</html>
