@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Edit Question</h2>
            </div>
              <div class="body">
                <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1"> Question
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">
                          <div class="row clearfix">
                            <form method="POST" action="{{ route('update-student')}}">
         <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

         <div class="table-responsive">

           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="question" class="col-md-4 control-label">Question</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" value="{{ $question->question }}" disabled>

                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Student's Roll No.</label>

                            <div class="col-md-6">
                               <input type="hidden" name="id" value="{{ $students->id }}">

                                <input id="name" type="text" class="form-control" name="roll_no" value="{{ $students->roll_no }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                             <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Student's Full Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="student_name" value="{{ $students->student_name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                           <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Student's Batch No</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="batch_id" value="{{ $students->batch_id }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
      </form>

                        </div>
                      </div>
                    </div>
                  </div>

               </div>
             </div>
           </div>
         </div>

    </section>

@endsection