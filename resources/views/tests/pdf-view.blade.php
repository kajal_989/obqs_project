<!DOCTYPE html>
<html>
<head>
  <title></title>
</head>
<body>
  <section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <img src="{{url('images/logo2.png')}}" hspace="20" height="100px" width="100px" align="right">
          <img src="{{url('images/logo1.png')}}" hspace="20"" height="100px" width="100px"  align="left">

        <div class="header">

          <center>

               <h2>Bharti Vidyapeeth Collage of Engineering, Navi Mumbai</h2><br>

               <h2>Sector-7, CBD, Belapur, Near Kharghar Railway Station, Navi Mumbai-400614</h2>
               <h6>Department Of Information Technology</h6>


          </center>

            </div>
              <div class="body">
                          <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <center>

                                   <h5>Unit Test:  {{$test->name}} </h5>
                                   <h5>Subject Name : {{$subject->name}}   <p align="right"> Total Marks: 20</p></h5>


                              </center>
                              <br>
                              <br>
                              <p class="lead">
                                   <h5>Instructions</h5>
                                   <ul>
                                        <li>Attempt 5 quetions out of 6 in Question 1</li>
                                        <li>Attempt 1 quetion out of 2 from question no 2 & 3</li>
                                        <li>Draw diagrams where are necessary</li>
                                   </ul>
                              </p>
                              <br>
                              <br>
                     <div class="table-responsive">
                        <table class="table table-condensed">
                             <tr>
                                  <th><b>Q. 1.</b></th>
                                  <th>Attempt any 5 questions.</th>
                                  <th>Marks</th>

                                  <th>CO</th>
                             </tr>

                              @for($i=0; $i < 6 ; $i++)
                                  <tr>
                                      <td>{{$questions[$i]->question_name}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}</td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                              <tr>
                                   <td><b>Q.2</b></td>
                                   <td><b>Attempt Any one of the following</b></td>
                              </tr>
                                @for($i=6; $i < 8 ; $i++)
                                  <tr>
                                      <td>{{$questions[$i]->question_name}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}</td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                              <tr>
                                   <td><b>Q.3</b></td>
                                   <td><b>Attempt Any one of the following</b></td>
                              </tr>
                                @for($i=8; $i < 10 ; $i++)
                                  <tr>
                                      <td>{{$questions[$i]->question_name}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}</td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                             </tbody>
                        </table>
                         </div>



                        </div>

                      </div>
                    </div>
                  </div>

</div>

</div>
</div>
</section>
</body>
</html>
