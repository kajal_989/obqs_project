@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <img src="{{asset('/images/logo2.png')}}" hspace="20" height="100px" width="100px" align="right">
          <img src="{{asset('/images/logo1.png')}}" hspace="20"" height="100px" width="100px"  align="left">

        <div class="header">

          <center>

               <h2>Bharti Vidyapeeth Collage of Engineering, Navi Mumbai</h2><br>

               <h2>Sector-7, CBD, Belapur, Near Kharghar Railway Station, Navi Mumbai-400614</h2>
               <h5>Department Of Information Technology</h5>


          </center>

            </div>
              <div class="body">
                          <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <center>

                                   <h5>Unit Test:  {{$test->name}} </h5>
                                   <h5>Subject Name : {{$subject->name}}   <p align="right"> Total Marks: 20</p></h5>


                              </center>
                              <br>
                              <br>
                              <p class="lead">
                                   <h5>Instructions</h5>
                                   <ul>
                                        <li>Draw diagrams where are necessary</li>
                                   </ul>
                              </p>
                              <br>
                              <br>
                     <div class="table-responsive">
                        <table class="table table-condensed">
                             <tr>
                                  <th><b>Q.1</b></th>
                                  <th>Attempt any <b>FIVE</b> questions.</th>
                                  <th>Marks</th>

                                  <th>CO</th>
                             </tr>

                              @for($i=0,$j=1; $i < 6 ; $i++,$j++)
                                  <tr>
                                      <td>{{$j}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}
                                        @if($questions[$i]->questionBank->image)

                                        <br>
                                        <br>
                                        <img src="{{asset($questions[$i]->questionBank->image->path)}}" height="200" width="200" />
                                        @endif

                                      </td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>

                              @endfor

                              <tr>
                                   <td><b>Q.2</b></td>
                                   <td><b>Attempt Any ONE of the following</b></td>
                              </tr>

                                @for($i=6,$j=1; $i < 8 ; $i++,$j++)
                                  <tr>
                                      <td>{{$j}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}
                                        @if($questions[$i]->questionBank->image)

                                         <img src="{{asset($questions[$i]->questionBank->image->path)}}" height="200" width="200" />
                                        @endif
                                      </td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                              <tr>
                                   <td><b>Q.3</b></td>
                                   <td><b>Attempt Any ONE of the following</b></td>
                              </tr>
                                @for($i=8 , $j=1; $i < 10 ; $i++ , $j++)
                                  <tr>
                                      <td>{{$j}}</td>
                                      <td>{{$questions[$i]->questionBank->question}}
                                         @if($questions[$i]->questionBank->image)
                                        <br>
                                        <br>
                                        <img src="{{asset($questions[$i]->questionBank->image->path)}}" height="200" width="200" />
                                        @endif
                                      </td>
                                      <td>{{$questions[$i]->questionBank->marks}}</td>
                                      <td>{{$questions[$i]->questionBank->questionCo->name}}</td>
                                  </tr>
                              @endfor

                             </tbody>
                        </table>
                         </div>



                        </div>

                      </div>
                    </div>
                  </div>

</div>

</div>
</div>
</section>

@endsection