@extends('layouts.admin_layout')

@section('content')

<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>{{$subject->name}}</h2>
            </div>
                <div class="body">
                    <div class="row clearfix">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="table-responsive">
                                 <table  class="table table-bordered table-striped table-hover">
                                      <thead>
                                            <tr class="quetsions-table tr">
                                                 <th>Sr. No</th>
                                                 <th>Question</th>
                                                  <th>Image</th>
                                                 <th>Chapter No</th>
                                                 <th>Marks</th>
                                                 <th>CO</th>
                                                <th>Action</th>
                                           </tr>
                                      </thead>
                                     <tbody>

                                         @foreach($questions as $question)
                                        <tr>
                                          <form method="get" action="edit-question">
                                            <input type="hidden" name="slug" value="{{$subject->slug}}">
                                            <input type="hidden" name="test_id" value="{{$question->test_id}}">

                                            <td>{{$loop->iteration}}</td>
                                           <td>
                                             <input type="hidden" name="question_id" value="{{$question->id}}">
                                            {{$question->question}}</td>
                                           <td>@if(!is_null($question->image))
                                            <img src="{{asset($question->image->path)}}" width="200" height="200"/>
                                            @else
                                            No Image
                                            @endif
                                            </td>
                                           <td>{{$question->chapter_no}}</td>
                                           <td>{{$question->marks}}</td>
                                           <td>{{$question->questionCo->name}}</td>
                                           <td>
                                                 <button class="btn btn-warning btn-sm" id="submit" name="submit"   data-toggle="tooltip" data-placement="bottom" title=" Edit this Question"   /><i class="glyphicon glyphicon-pencil"></i>
                                                  </button>
                                           </form>
                                                   <button class="delete-modal btn btn-danger btn-sm" data-id="{{$question->id}}" id="submit" name="submit"   data-toggle="tooltip" data-placement="bottom" title=" Delete this Question"  /><i class="glyphicon glyphicon-remove"></i>
                                                  </button>
                                            </td>

                                      </tr>
                                     @endforeach
                                     </tbody>
                                  </table>

                              </div>

                         </div>

                   </div>
              </div>
        </div>
    </div>
  </div>

  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Acadmic year</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" action="{{ route('editYear') }}" id="editform">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id" disabled>
            </div>
          </div>
    </form>
        <div class="deleteContent">
          Are You sure want to delete <span class="title"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript">
 $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete Post');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.title').html($(this).data('title'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'deleteQuestion',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){

           $('.delete-modal').modal('hide');
           // window.location = data.url;
           location.reload();
        }
      });
    });

  // Show function
  $(document).on('click', '.show-modal', function() {
      $('#show').modal('show');
      $('#i').text($(this).data('id'));
      $('#ti').text($(this).data('title'));
      $('#by').text($(this).data('body'));
      $('.modal-title').text('Show Post');
  });
</script>

</section>
@endsection

