@extends('layouts.admin_layout')

@section('content')

<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>My Subjects</h2>
            </div>
                <div class="body">
                    <div class="row clearfix">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <div class="table-responsive">
                                 <table  class="table table-bordered table-striped table-hover">
                                      <thead>
                                            <tr class="quetsions-table tr">
                                                 <th>Subject</th>
                                                <th>Unit Test</th>
                                                <th>
                                                  <a href="{{route('add-questions')}}" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="Add / import new questions">
                                                       <i class="material-icons">add</i>
                                                 </a>

                                                 </th>
                                           </tr>
                                      </thead>
                                     <tbody>

                                         @foreach($subject_list as $subject)
                                        <tr>
                                          <form method="get" action="subject-question-bank">
                                           <td>
                                                <input type="hidden" name="slug" value="{{$subject->slug}}">
                                                 {{$subject->subject_name}}
                                           </td>
                                           <td>
                                           <select class="form-control" name = "test_id" id="test_id" required="true">
                                          <option value="">--select unit test--</option>
                                           @foreach($tests as $test)
                                            <option value="{{$test->id}}">{{$test->name}}</option>
                                           @endforeach
                                           </select>
                                           </td>

                                           <td>
                                                 <button class="btn btn-primary" id="submit" name="submit"   data-toggle="tooltip" data-placement="bottom" title=" View Questions"  /><i class="glyphicon glyphicon-eye-open"></i>
                                                  </button>
                                            </td>
                                              </form>
                                      </tr>
                                     @endforeach
                                     </tbody>
                                  </table>

                              </div>

                         </div>

                   </div>
              </div>
        </div>
    </div>
  </div>

</section>
@endsection

