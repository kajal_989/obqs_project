@extends('layouts.admin_layout')
@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>My Subjects </h2>
                        </div>
                        <div class="body">
                                    <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                           <thead>
                              <tr>
                                  <th>SERIAL NO</th>
                                  <th>Subject Name</th>
                                  <th><center>Actions</center></th>
                              </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                 <th>SERIAL NO</th>
                                  <th>Subject Name</th>
                                  <th><center>Actions</center></th>
                                </tr>
                           </tfoot>
                          <tbody>
                         {{ csrf_field() }}
                           @foreach($subject_list as $subject )
                              <tr>
                               <td>{{ $loop->iteration }}</td>
                               <td> {{$subject->subject_name}}</td>
                             <td><center>
                          <!-- <form method="get" action="{{ route('co-statements') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
            <input type="submit" class="btn bg-teal waves-effect" value="CO Statements" />
            </form><br>
             <form method="post" action="{{ route('co-po-mappings') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="subject_id" value="{{$subject->subject_id}}">
             <input type="submit" class="btn btn-info btn-sm" value="CO-PO-Mapping" />
           </form> -->
         </center>
         <!-- <a href="{{route('co-statements', $subject->slug)}}"> CO-PO-Mapping </a> -->
         <?php $parameter = Crypt::encrypt($subject->slug);?>
         <?php $idparam = Crypt::encrypt($subject->subject_id);?>
         <center>
         <a href="{{ route('co-statements', $params = ['slug' => $parameter ]) }}" class="btn btn-primary"  data-toggle="tooltip" data-placement="bottom" title="Add CO Statements">  <i class="material-icons">layers</i></a>
          <a href="{{ route('co-po-mappings', $params = ['id' => $idparam ]) }}" class="btn btn-info" data-toggle="tooltip" data-placement="bottom" title="CO PO Mapping"><i class="glyphicon glyphicon-random"></i></a>
        </center>
          </td>
                                 </tr>
                           @endforeach
                     </tbody>
                    </table>
                </div>

               </div>
             </div>
          </div>
        </div>
    </div>
  </div>
 </div>

 </section>
@endsection