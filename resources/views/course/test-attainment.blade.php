@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Test Attainment  for {{$subject}}</h2>
            </div>
              <div class="body">
                 @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif

                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1">Table One
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">

                     <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>Question NO:</th>
                            <th>Q.1.A</th>
                            <th>Q.1.B</th>
                             <th>Q.1.C</th>
                            <th>Q.1.D</th>
                             <th>Q.1.E</th>
                            <th>Q.1.F</th>
                             <th>Q.2.A</th>
                            <th>Q.2.B</th>
                             <th>Q.3.A</th>
                            <th>Q.3.B</th>
                          </tr>
                          <tr>
                            <td>Related CO Statement</td>
                            @foreach($cos as $co)
                            <td>{{$co->name}}</td>
                            @endforeach
                          </tr>
                        </thead>
                       </table>
                     </div>
                      </div>
                    </div>
                  </div>


                <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_2">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_2" aria-expanded="false" aria-controls="collapseOne_1">Table Two
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_2" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_2">
                      <div class="panel-body">

                     <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>Question NO:</th>
                            <th>Q.1.A</th>
                            <th>Q.1.B</th>
                             <th>Q.1.C</th>
                            <th>Q.1.D</th>
                             <th>Q.1.E</th>
                            <th>Q.1.F</th>
                             <th>Q.2.A</th>
                            <th>Q.2.B</th>
                             <th>Q.3.A</th>
                            <th>Q.3.B</th>
                          </tr>
                          <tr>
                            <td>No Of student attended question </td>
                            @foreach($avgs as $avg)
                            <td>{{$avg}}</td>
                            @endforeach
                          </tr>
                        </thead>
                       </table>
                     </div>
                      </div>
                    </div>
                  </div>

                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_3">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseOne_3" aria-expanded="false" aria-controls="collapseOne_3"> Table Three
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_3" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_3">
                      <div class="panel-body">

                           <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>Question NO:</th>
                            <th>Q.1.A</th>
                            <th>Q.1.B</th>
                             <th>Q.1.C</th>
                            <th>Q.1.D</th>
                             <th>Q.1.E</th>
                            <th>Q.1.F</th>
                             <th>Q.2.A</th>
                            <th>Q.2.B</th>
                             <th>Q.3.A</th>
                            <th>Q.3.B</th>
                          </tr>
                          <tr>
                            <td>No Of student who got more than 70% Marks</td>
                            @foreach($second_table as $st)
                            <td>{{$st}}</td>
                            @endforeach
                          </tr>
                           <tr>
                            <td>% students</td>
                            @foreach($percent_student as $ps)
                            <td>{{$ps}}</td>
                            @endforeach
                          </tr>
                        </thead>
                       </table>
                     </div>
                      </div>
                    </div>
                  </div>

                    <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_4">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseOne_4" aria-expanded="false" aria-controls="collapseOne_4"> Course Attainment
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_4" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_4">
                      <div class="panel-body">

                           <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>CO Reference</th>
                             @foreach($distinct_co as $co)
                             <th>{{$co->name}}</th>
                             @endforeach
                          </tr>
                          <tr>
                            <td>% Attainment</td>
                            @foreach($co_avg as $c)
                            <td>{{$c}}</td>
                            @endforeach
                          </tr>
                           <tr>
                            <td>Level Achieved</td>
                            @foreach($levels as $l)
                            <td>{{$l}}</td>
                            @endforeach
                          </tr>
                        </thead>
                       </table>
                     </div>
                      </div>
                    </div>
                  </div>
       </div>
    </div>
  </div>
</div>
 </div>
 </div>
</div>
</div>
</div>
</section>
@endsection