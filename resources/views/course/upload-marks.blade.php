@extends('layouts.admin_layout')
@section('content')
	 <section class="content">

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                          <h2>{{$subject_id->name}} &nbsp; Unit Test:{{$test->name}}
                          </h2>
                        </div>
                        <div class="body">
                             @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                  <div class="table-responsive">
                    <form method="post" action="{{ route('store-subject-marks') }}">
                      {{ csrf_field() }}
                      <input type="hidden" name="subject_id" value="{{$subject_id->id}}">
                      <input type="hidden" name="test_id" value="{{$test->id}}">
                          <table  class="table table-js-exportable">
                 <thead>
                      <th>Roll No</th>
                      <th>Name</th>
                      <th>Q 1. A</th>
                      <th>Q 1. B</th>
                      <th>Q 1. C</th>
                      <th>Q 1. D</th>
                      <th>Q 1. E</th>
                      <th>Q 1. F</th>
                      <th>Q 2. A</th>
                      <th>Q 2. B</th>
                      <th>Q 3. A</th>
                      <th>Q 3. B</th>
                      <th>Total</th>
                    </thead>

                  <tbody>
                    @foreach($marks as $mark)
                    <tr>
                      <td>{{$mark->roll_no}}</td>
                      <td>{{$mark->student_name}}</td>
                      <td><input type="hidden" name="student_id[]" value="{{$mark->student_id}}"><input type="text" name="one_a[]" class="form-control" value="{{$mark->one_a}}"></td>
                      <td><input type="text" name="one_b[]" class="form-control" value="{{$mark->one_b}}"></td>
                      <td><input type="text" name="one_c[]" class="form-control" value="{{$mark->one_c}}"></td>
                      <td><input type="text" name="one_d[]" class="form-control" value="{{$mark->one_d}}"></td>
                      <td><input type="text" name="one_e[]" class="form-control" value="{{$mark->one_e}}"></td>
                      <td><input type="text" name="one_f[]" class="form-control" value="{{$mark->one_f}}"></td>
                      <td><input type="text" name="two_a[]" class="form-control" value="{{$mark->two_a}}"></td>
                      <td><input type="text" name="two_b[]" class="form-control" value="{{$mark->two_b}}"></td>
                      <td><input type="text" name="three_a[]" class="form-control" value="{{$mark->three_a}}"></td>
                      <td><input type="text" name="three_b[]" class="form-control" value="{{$mark->three_b}}"></td>
                      <td><input type="text" name="total[]" class="form-control" value="{{$mark->total}}"></td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
              <center><input type="submit" class="btn btn-info"  name="Confirm"></center>
            </form>
            </div>
        </div>
    </div>
  </div>
</div>
</section>
@endsection
