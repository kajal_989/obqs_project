@extends('layouts.teacher_layout')
@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Subject Name:  <b>{{$sub->name}}</b></h2>
                        </div>
                        <div class="body">

                    @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                  <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover ">
                           <thead>
                              <tr>
                                  <th>SERIAL NO</th>
                                  <th>Reference</th>
                                  <th>CO Statement</th>
                                  <th> <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#addModal">
                                <i class="material-icons">add</i>
                               </button></th>
                              </tr>
                            </thead>
                           <tbody>
                              @foreach($costatements as $co )
                              <tr>
                               <td>{{ $loop->iteration }}</td>
                               <td> {{$co->name}}</td>
                               <td>{{$co->statement}}</td>
                               <td>
                               <button class="btn btn-warning btn-sm">
                                <i class="material-icons">edit</i>
                               </button>
                               <button class="delete-modal btn btn-danger btn-sm" data-id="{{$co->id}}" data-name="{{$co->name}}">
                               <i class="material-icons">delete</i>
                               </button>
                               </td>
                             </tr>
                             @endforeach
                            </tbody>
                    </table>
                </div>

               </div>
             </div>
          </div>
        </div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Acadmic year</h4>
      </div>
      <div class="modal-body">
        {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="title"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
        Delete
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>

<script type="text/javascript">
   $(document).on('click', '.delete-modal', function() {

        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete CO Statement');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.title').html($(this).data('name'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'deleteCO',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){

           $('.delete-modal').modal('hide');
           window.location = data.url;
        }
      });
    });
</script>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="defaultModalLabel">Add new CO Statement</h4>
              </div>
              <div class="modal-body">
                  <form method="post" class="form-horizontal" action="{{ route('co-add') }}">
                             {{ csrf_field() }}

                  <div class="table-responsive">
                  <table id="dataTable" class="table table-striped">
                 <thead>
                      <th>select</th>
                      <th>Reference</th>
                      <th>CO Statement</th>
                 </thead>
                  <tbody>
                    <tr>
                    <p>

                  <td><input type="checkbox" id="md_checkbox_21" class="form-control" checked required="required" />
                    <input type="hidden" name="subject_id" value="{{$sub->id}} ">
                    </td>

                    <td>
                     <input type="text" class="form-control" required="required" placeholder="eg CO1." name="name[]">
                    </td>

                    <td>
                    <input type="text" class="form-control" required="required"  placeholder="statement" name="statement[]">
                    </td>

                   </p>
                    </tr>
                    </tbody>
                </table>
                <br>
                  <p>
                      <input type="button" value="Add New Record" class="btn btn-primary"  onClick="addRow('dataTable')" />
                      <input type="button" value="Remove Record" class="btn btn-danger" onClick="deleteRow('dataTable')"  />
                  </p>
              </div>
              <div class="clear"></div>
              </div>
                <div class="modal-footer">
                  <input type="submit" value="ADD" class="btn btn-link waves-effect"/>
                  <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
              </form>
              </div>
            </div>
          </div>
          <script type="text/javascript">
  function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 20){             // limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var colCount = table.rows[0].cells.length;
            for(var i=0; i<colCount; i++) {
                var newcell = row.insertCell(i);
                newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                }
          }else{
           alert("maximum 20 rows are allowded.");

      }
  }

function deleteRow(tableID) {
     var table = document.getElementById(tableID);
     var rowCount = table.rows.length;
     for(var i=0; i<rowCount; i++) {
     var row = table.rows[i];
     var chkbox = row.cells[0].childNodes[0];
      if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all Rows.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
</script>
 </section>
@endsection