@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Edit Student</h2>
            </div>
              <div class="body">
                <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1"> Student Information
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">
                          <div class="row clearfix">


         <div class="table-responsive">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Subject Name</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" value="{{ $subject->name }}" disabled>
                            </div>
                        </div>
                         <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Course Code:</label>

                            <div class="col-md-6">


                                <input id="name" type="text" class="form-control" name="roll_no" value="{{ $subject->course_code }}" disabled>

                            </div>
                        </div>

                            <label for="name" class="col-md-4 control-label">Semister</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="student_name" value="{{ $subject->semister_id }}" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
      </form>

                        </div>
                      </div>
                    </div>
                  </div>

               </div>
             </div>
           </div>
         </div>

    </section>

@endsection