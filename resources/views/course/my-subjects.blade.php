@extends('layouts.admin_layout')
@section('content')
	 <section class="content">

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                          <h2>My Subject List</h2>
                        </div>
                        <div class="body">
                             @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                  <div class="table-responsive">
                          <table  class="table table-js-exportable">
                 <thead>
                      <th>Sr No</th>
                      <th>Semister</th>
                      <th>Cource Code</th>
                       <th>Subject Name</th>
                       <th>Action</th>
                    </thead>

                  <tbody>
                    @foreach($subjects as $subject)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$subject->short_name}}</td>
                      <td>{{$subject->course_code}}</td>
                      <td>{{$subject->subject_name}}</td>
                      <td>
                         <?php $parameter = Crypt::encrypt($subject->slug);?>
                         <a href="{{ route('view-my-subject', $params = ['slug' => $parameter ]) }}" class="btn btn-info"  data-toggle="tooltip" data-placement="bottom" title="View Details">
              <i class="glyphicon glyphicon-eye-open" ></i>
            </a>
            <a href="{{ route('edit-my-subject', $params = ['slug' => $parameter ]) }}" class="btn btn-warning btn-sm"  data-toggle="tooltip" data-placement="bottom" title="Edit Details">
              <i class="glyphicon glyphicon-pencil"></i>
            </a>

                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>
</section>
@endsection
