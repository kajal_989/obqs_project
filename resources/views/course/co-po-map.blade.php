@extends('layouts.admin_layout')

@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                          <div class="header">
                            <h2>Subject Name:  {{$sub->name}}</h2>
                        </div>
                        <div class="body">
 <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover ">
                           <thead>
                              <tr>
                               <th>No.</th>
                                  <th>Reference</th>
                                  <th>CO Statement</th>

                              </tr>
                            </thead>
                           <tbody>
                              @foreach($costatements as $co )
                              <tr>
                               <td>{{ $loop->iteration }}</td>
                               <td> {{$co->name}}</td>
                               <td>{{$co->statement}}</td>

                             </tr>
                             @endforeach
                            </tbody>
                    </table>
                </div>
 <div class="header">
                           <center> <h2>CO-PO- Mapping</h2> </center>
                        </div>
                        <form  method="POST" action="{{ route('store-mappings') }}">
                        {{ csrf_field() }}

                        <div class="body" >

                            <div class="table-responsive">
                                <table id="mainTable" class="table table-bordered table-striped table-hover">

                                    <thead>
                                    <tr>
                                        <th>CO/PO</th>
                                        @foreach($pos as $key=>$po)
                                        <th>
                                            {{$po->name}}
                                        </th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>

                                        @foreach($cos as $co)
                                        <tr>
                                            <td>{{$co->name}}</td>

                                        @foreach($pos as $key => $po)



                                                <td ><input type="text" class="form-control" name="ratings[]" id ="{{$coPoMappings[$co->id][$po->id]['ratings']->id}}" onchange="myHandler('{{$coPoMappings[$co->id][$po->id]['ratings']->id}}')" contenteditable="true" value="{{$coPoMappings[$co->id][$po->id]['ratings']->ratings}}"></td>

                                                <input type="hidden"  name="weights[]" id ="weights[]"  value="">

                                        @endforeach

                                        </tr>

                                        @endforeach

                                        <tr>

                                <td>PO Average</td>
                               @foreach($avg as $a )
                               <td><input type="number" name="avg" value="{{$a->avg}}" disabled></td>
                               @endforeach
                               </tr>

                                   <tr>

                                <td>PO weighted Avg</td>
                               @foreach($avg as $a )
                               <td><input type="number"   class="form-control"  name="avg" value="{{$a->weighted_avg}}" disabled></td>
                               @endforeach
                               </tr>

                                    </tbody>


                                </table>

                           <script type="text/javascript">
                             function myHandler(val) {
                              console.log(val-1);
                               var x = document.getElementsByName("ratings[]")[val-1].value;
                               document.getElementsByName("weights[]")[val-1].value = x;
                             }
                           </script>
                                <input type="hidden" name="subject_id" value="{{$subject_id}}">



                               <center>  <input type="submit" class="btn btn-info btn-lg"  name="submit" value="calculate average"></center>

                               <br>


                            </div>
                        </div>
                    </div>
                </form>
                <div>

                            </div>
                </div>
            </div>
                </div>
            </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <b>Profile Content</b>
                                   <select id="select2" style="width:100%;" multiple="multiple">
<optgroup label="Group 1">
    <option value="19">green </option>
    <option value="18">red </option>
    <option value="14">test </option>
  </optgroup>

  <optgroup label="Group 2">
    <option value="11">Strategy </option>
    <option value="18">red </option>
  </optgroup>

  <optgroup label="Group 3">
    <option value="11">Strategy </option>
  </optgroup>
</select>


                                </div>
                                              </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Example Tab -->

         </div>

    </section>
@endsection