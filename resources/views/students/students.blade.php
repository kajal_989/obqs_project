@extends('layouts.admin_layout')
@section('content')
  <section class="content">
   <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                         <div class="header">
                            <h2>Students List
                            </h2>
                        </div>
                        <div class="body">
                            @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
    @if (Session::has('error'))
          <div class="alert alert-danger">
        {{ session('error') }}
    </div>
    @endif
                                  <div class="panel panel-info">
                                            <div class="panel-heading" role="tab" id="headingOne_1">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1">
                                                       Click here to search student
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne_1">
                                                <div class="panel-body">
                                                   <form method="POST" action="{{ route('student-management-search') }}">
         {{ csrf_field() }}
         @component('layouts.search', ['title' => 'Search'])
          @component('layouts.two-cols-search-row', ['items' => ['Enter Student Name', 'Enter Year eg SE'],
          'oldVals' => [isset($searchingVals) ? $searchingVals['student_name'] : '', isset($searchingVals) ? $searchingVals['year'] : '']])
          @endcomponent
        @endcomponent
      </form>
                                                </div>
                                            </div>
                                        </div>


    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">

          <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th>Semister</th>
                <th>Roll No</th>
                <th>Student Name</th>
                <th>
                <a href="{{route('add-students')}}" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="Add / import Students">
                <i class="material-icons">add</i>
                </a>
              </th>
              </tr>
            </thead>
            <tbody>
            @foreach ($students as $student)
                <tr>
                  <td>{{ $student->short_name }}</td>
                  <td>{{ $student->roll_no }}</td>
                  <td>{{ $student->student_name }}</td>
                  <td>
                     <a href = "{{ route('student-management.show',$student->id) }}" id="edit" class=" btn btn-warning btn-sm" data-toggle="tooltip" data-placement="bottom" title="Edit this Student">
               <i class="material-icons">edit</i>
</a>
            <button class="delete-modal btn btn-danger btn-sm" data-id="{{$student->id}}" data-student_name="{{$student->student_name}}" data-roll_no="{{$student->roll_no}}" data-toggle="tooltip" data-placement="bottom" title="Delete this Student">
               <i class="material-icons">delete</i>
            </button>
                  </td>
              </tr>
            @endforeach
            </tbody>
            <tfoot>
              <tr>
               <th>Year</th>
                <th>Roll No</th>
                <th>Student Name</th>
                <th>Action</th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-5">
          <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to {{count($students)}} of {{count($students)}} entries</div>
        </div>
        <div class="col-sm-7">
          <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
            {{ $students->links() }}
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="Code">Code</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="code" name="code"
              placeholder="Faculty Unique Code" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="to_year">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="short_name" name="short_name"
              placeholder="Faculty name" required>
            </div>
          </div>
        </form>

        {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete Roll no: <span class="code"></span> Student name  <span class="name"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>

<script type="text/javascript">
  $(document).on('click', '.edit-modal', function() {

    $('#footer_action_button').text("Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Edit Faculty code');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#id').val($(this).data('id'));

    $('#code').val($(this).data('code'));

    $('#short_name').val($(this).data('short_name'));

    $('#myModal').modal('show');

});



// form Delete function
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete Student');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.code').html($(this).data('roll_no'));
        $('.name').html($(this).data('student_name'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'student-management/deleteStudent',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){
           $('.delete-modal').modal('hide');
           window.location = data.url;
        }
      });
    });


  function editStudent(obj) {
    var roll_no = obj.getAttribute('data-roll_no');
     var student_name = obj.getAttribute('data-student_name');
     console.log(roll_no,student_name);
        $.ajax({
             type: 'get',
             url: '{{ route('edit-student') }}',
            data:{
                    "roll_no":roll_no,
                     "student_name":student_name,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log($data);
             }
              });
  }
  function deleteStudent() {
    var test_id = $('#test_id').val();
     var subject_id = $('#subject_id').val();
     console.log(test_id,subject_id);
  }
</script>
    </section>

@endsection