@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Add New Students</h2>
            </div>
              <div class="body">
                   @if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif
    @if (Session::has('error'))
          <div class="alert alert-danger">
        {{ session('success') }}
    </div>
    @endif
                <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1"> Import Excel File
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">
                         <form method="post" action="{{ route('importStudents') }}" enctype="multipart/form-data">
                             {{ csrf_field() }}
                          <div class="row clearfix">
                            <div class="col-sm-4">
                              <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                             <select class="form-control" name = "semister_id" required>
                                <option value="">--select semister--</option>
                                  @foreach($semisters as $semister)
                                  <option value="{{$semister->id}}">{{$semister->short_name}}</option>
                                  @endforeach
                              </select>
                              </div>

                            <div class="col-sm-4">
                              <input type="file" class="form-control" name="studentlist" required/>
                            </div>
                            <div class="col-sm-4">
                              <button class="btn btn-primary" name="submit"  data-toggle="tooltip" data-placement="bottom" title="Upload Students"><i class="material-icons">arrow_upward</i></button>
                               <a class="btn btn-info" href="{{asset('formats/student_import.xlsx')}}"  data-toggle="tooltip" data-placement="bottom" title="Download File Format" download>
                                <i class="material-icons">arrow_downward</i>
                               </a>
                            </div>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>

                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_2">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse"  href="#collapseOne_2" aria-expanded="false" aria-controls="collapseOne_2"> Add Students using form
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_2" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_2">
                      <div class="panel-body">
                          <form method="post" class="form-horizontal" action="{{ route('store-students') }}">
                             {{ csrf_field() }}

                          <div class="row clearfix">

                            <div class="col-sm-4">
                              <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                              <select class="form-control" id="semister_id" name = "semister_id" onchange="myfun()" required>
                                <option value="">--select semister--</option>
                                  @foreach($semisters as $semister)
                                  <option value="{{$semister->id}}">{{$semister->short_name}}</option>
                                  @endforeach
                              </select>
                            </div>
                             <div class="col-sm-4">
                             <h4>Accadmic Year {{$acyear->from_year}} - {{$acyear->to_year}} </h4>
                            </div>

                             <div class="col-sm-12">

                  <div class="table-responsive">
                  <table id="dataTable" class="table dataTable">
                 <thead>
                      <th>Select</th>
                      <th>Roll No</th>
                      <th>Student Name</th>
                      <th>Batch</th>
                      <th>Elective Subject</th>
                 </thead>
                  <tbody>
                    <tr>
                    <p>

                    <td><input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked required="required"/>
                    </td>

                    <td>
                     <input type="number"  class="form-control" placeholder="Roll No" name="roll_no[]">
                    </td>

                    <td>
                    <input type="text"  class="form-control"  placeholder="Student Name" name="student_name[]">
                    </td>
                    <td>
                      <select class="form-control" name="batch_id[]" required="true">
                        <option value="">--Select Batch--</option>
                        <option value="1">Batch I</option>
                        <option value="2">Batch II</option>
                        <option value="3">Batch III</option>
                        <option value="4">Batch IV</option>

                      </select>
                    </td>

                    <td>
                       <select class="form-control" id="elective_subject_id" name = "elective_subject_id[]" >
                                <option value="">--select elective subject--</option>
                      </select>
                    </td>
                   </p>
                    </tr>
                    </tbody>
                </table>
                <br>
                  <p>
                     <input type="submit" name="submit" value="Save" class="btn btn-success"  />
                      <input type="button" value="Add New Record" class="btn btn-primary"  onClick="addRow('dataTable')" />
                      <input type="button" value="Remove Record" class="btn btn-danger" onClick="deleteRow('dataTable')"  />
                  </p>
              </div>
              <div class="clear"></div>
              </div>
              </div>
            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
</div>
</div>
</div>


 <script type="text/javascript">

  function myfun() {
    var semister_id =document.getElementById('semister_id').value;
    console.log(semister_id);

    $.ajax({
             type: 'post',
             url: '{{ route('select-elective-subject') }}',
            data:{
                    "semister_id":semister_id,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log(data);
                $('#elective_subject_id').children('option:not(:first)').remove();
                var html = '';
            for(var i = 0; i < data.length; i++){
console.log(data[i].id);
     var option=$('<option value='+data[i].id+'>'+data[i].name+'</option>');
  $('#elective_subject_id').append(option);

}
}
});
}

  function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 20){             // limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var colCount = table.rows[0].cells.length;
            for(var i=0; i<colCount; i++) {
                var newcell = row.insertCell(i);
                newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                }
          }else{
           alert("maximum 20 rows are allowded.");

      }
  }

function deleteRow(tableID) {
     var table = document.getElementById(tableID);
     var rowCount = table.rows.length;
     for(var i=0; i<rowCount; i++) {
     var row = table.rows[i];
     var chkbox = row.cells[0].childNodes[0];
      if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all Rows.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
</script>

    </section>

@endsection