@extends('layouts.admin_layout')

@section('content')


	 <section class="content">
            <!-- Example Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                               Course File

                            </h2>

                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#home" data-toggle="tab">CO-PO Mapping</a></li>
                                      </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">

                                    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="block-header">

            </div>
            <!-- Exportable Table -->

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form  method="POST" action="{{ route('store-mappings') }}">
                        {{ csrf_field() }}

    <label>Select your subject: &nbsp;</label>
                                      <select  required="true">
                                        <option value="">Select Your Subject</option>




                                         <option value="">Software Engineering</option>


                                </select>

                                <script type="text/javascript">

                                </script>


                    <div class="card">
                        <div class="body" >

                            <div class="table-responsive">



                                <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">

                                    <thead>
                                    <tr>
                                        <th>CO/PO</th>
                                        @foreach($pos as $key=>$po)
                                        <th>
                                            {{$po->name}}
                                        </th>
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>



                                        @foreach($cos as $co)
                                        <tr>
                                            <td>{{$co->name}}</td>

                                        @foreach($pos as $key => $po)

{{dd($coPoMappings[$co->id][$po->id]['ratings']->ratings)}}

                                                <td ><input type="number" name="ratings[]" id ="{{$coPoMappings[$co->id][$po->id]['ratings']->id}}"  value="{{$coPoMappings[$co->id][$po->id]['ratings']->ratings}}"></td>

                                        @endforeach

                                        </tr>

                                        @endforeach

                                        <tr>

                                <td>PO Average</td>
                               @foreach($avg as $a )
                               <td><input type="text" class="form-control" name="avg" value="{{$a->avg}}" disabled></td>
                               @endforeach
                               </tr>

                                    </tbody>


                                </table>

                                <input type="hidden" name="subject_id" value="{{$subject_id}}">



                               <center>  <input type="submit" class="btn btn-info btn-lg"  name="submit" value="calculate average"></center>

                               <br>


                            </div>
                        </div>
                    </div>
                </form>
                <div>

                            </div>
                </div>
            </div>
                </div>
            </div>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <b>Profile Content</b>
                                   <select id="select2" style="width:100%;" multiple="multiple">
<optgroup label="Group 1">
    <option value="19">green </option>
    <option value="18">red </option>
    <option value="14">test </option>
  </optgroup>

  <optgroup label="Group 2">
    <option value="11">Strategy </option>
    <option value="18">red </option>
  </optgroup>

  <optgroup label="Group 3">
    <option value="11">Strategy </option>
  </optgroup>
</select>

<h2>


Selected Tags:
<span id="selected">
</span>
</h2>

                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <b>Message Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                        pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                        sadipscing mel.
                                    </p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="settings">
                                    <b>Settings Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                        pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                        sadipscing mel.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Example Tab -->

         </div>

    </section>
@endsection