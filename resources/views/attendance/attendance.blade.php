@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Attendance</h2>
            </div>
              <div class="body">
                 @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif

                 <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1">Theory Attendance
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">

                     <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>Sr. No</th>
                            <th>Subject Name</th>
                            <th>Action</th>
                          </tr>
                           @foreach($theory_subjects as $subject)

                          <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$subject->name}}</td>
                            <td>
                              <?php $parameter = Crypt::encrypt($subject->slug);?>
                              <a href="{{ route('take-attendance', $params = ['slug' => $parameter]) }}" class="btn btn-info"  data-toggle="tooltip" data-placement="bottom" title="Mark Attendance">
                             <i class="glyphicon glyphicon-check" ></i>
                            </a>

                          </tr>
                             @endforeach
                        </thead>
                       </table>
                     </div>
                      </div>
                    </div>
                  </div>


                   <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_3">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_3" aria-expanded="false" aria-controls="collapseOne_3">Practical Attendance
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_3" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_3">
                      <div class="panel-body">

                     <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>Sr. No</th>
                            <th>Subject Name</th>
                            <th>Batch No</th>
                            <th>Action</th>
                          </tr>
                           @foreach($practical_subjects as $subject)
                         <form method="get" action="{{ route('take-practical-attendance')}}">
                          <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>
                               <?php $parameter = Crypt::encrypt($subject->slug);?>
                              <input type="hidden" name="slug" value="{{$parameter}}">
                              {{$subject->name}}</td>
                            <td>
                              <select name="batch_id" id="batch_id" class="form-control" required="true">
                                <option value="">--select batch--</option>
                                <option value="1"> Batch I</option>
                                <option value="2"> Batch II</option>
                                <option value="3"> Batch III</option>
                                <option value="4"> Batch IV</option>
                              </select>
                            </td>
                            <td>
                            <button type="submit" name="submit" value="mark" class="btn btn-info"  data-toggle="tooltip" data-placement="bottom" title="Mark Attendance">
                             <i class="glyphicon glyphicon-check" ></i>
                            </button>

                            <button type="submit" name="submit" value="view" class="btn btn-primary"  data-toggle="tooltip" data-placement="bottom" title="view Attendance">
                             <i class="glyphicon glyphicon-eye-open" ></i>
                            </button>


                           </td>
                          </tr>
                        </form>
                             @endforeach
                        </thead>
                       </table>

                     </div>
                      </div>
                    </div>
                  </div>


                      <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_2">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_2" aria-expanded="false" aria-controls="collapseOne_2">View Attendance(theory)
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_2" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_2">
                      <div class="panel-body">

                     <div class="table-responsive">

                       <table class="table table-bordered table-hover dataTable" role="grid">
                        <thead>
                          <tr>
                            <th>Sr. No</th>
                            <th>Subject Name</th>
                            <th>Action</th>
                          </tr>
                           @foreach($theory_subjects as $subject)

                          <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$subject->name}}</td>
                            <td>
                              <?php $parameter = Crypt::encrypt($subject->slug);?>
                              <a href="{{ route('view-subject-attendance', $params = ['slug' => $parameter]) }}" class="btn btn-info"  data-toggle="tooltip" data-placement="bottom" title="View Attendance">
                             <i class="material-icons">remove_red_eye</i>
                            </a>

                          </tr>
                             @endforeach
                        </thead>
                       </table>
                     </div>
                      </div>
                    </div>
                  </div>



       </div>
    </div>
  </div>
</div>
 </div>
 </div>
</div>
</div>
</div>
</section>
@endsection