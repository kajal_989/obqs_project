<?php

namespace App\Http\Controllers\Attendance;

use App\Http\Controllers\Controller;
use App\models\Attendance;
use App\models\Semister;
use App\models\Student;
use App\models\Subject;
use App\models\Year;
use Auth;
use Carbon\Carbon;
use Crypt;
use DB;
use Illuminate\Http\Request;

class AttendanceController extends Controller {

	public function __construct(Student $student, Semister $semister, Subject $subject, Year $year) {
		$this->student = $student;
		$this->semister = $semister;
		$this->subject = $subject;
		$this->year = $year;
	}

	public function index() {

		$data['subjects'] = DB::table('subject_teachers')
			->join('subjects', 'subject_teachers.subject_id', '=', 'subjects.id')
			->where('subject_teachers.theory_teacher', Auth::user()->id)
			->select('subjects.slug', 'subjects.name', 'subjects.id')
			->get();
		//dd($data);
		return view('attendance.attendance', $data);

	}

	public function takeAttendance(Request $request) {
		$slug = Crypt::decrypt($request->slug);
		$data['subject'] = DB::table('subjects')
			->join('semisters', 'subjects.semister_id', '=', 'semisters.id')
			->join('years', 'semisters.year_id', '=', 'years.id')
			->select('subjects.*', 'years.id as year_id')
			->where('subjects.slug', $slug)
			->first();
		if ($data['subject']->is_elective == '1') {
			$data['students'] = DB::table('students')
				->where('year_id', $data['subject']->year_id)
				->where('elective_subject_id', $data['subject']->id)
				->where('acadmic_year_id', $data['subject']->acadmic_year_id)
				->get();
		} else {

			$data['students'] = DB::table('students')
				->where('year_id', $data['subject']->year_id)
				->where('acadmic_year_id', $data['subject']->acadmic_year_id)
				->get();
		}

		return view('attendance.take-attendance', $data);

	}

	public function saveAttendance(Request $request) {
		//	dd($request->all());
		$sem = DB::table('subjects')
			->where('id', $request->subject_id)->first();
		$attendance_date = Carbon::now()->format('Y-m-d');
		$attendance_time = Carbon::now()->format('H:i:s');
		//	$store = [];
		for ($i = 0; $i < count($request->present_students); $i++) {
			$store = [
				"acadmic_year_id" => $request->acadmic_year_id,
				"year_id" => $request->year_id,
				"subject_id" => $request->subject_id,
				"attendance_date" => $attendance_date,
				"attendance_time" => $attendance_time,
				"student_id" => $request->present_students[$i],
				"status" => "1",
				"created_by" => Auth::user()->id,
				"updated_by" => Auth::user()->id,
			];
			$insert = DB::table('attendances')->insert($store);
		}

		$store_subject = [
			"acadmic_year_id" => $request->acadmic_year_id,
			"year_id" => $request->year_id,
			"subject_id" => $request->subject_id,
			"attendance_date" => $attendance_date,
			"attendance_time" => $attendance_time,
			"semister_id" => $sem->semister_id,
			"status" => "1",
			"created_by" => Auth::user()->id,
			"updated_by" => Auth::user()->id,
		];

		$store_subject_count = DB::table('attendance_subject_counts')->insert($store_subject);

		if ($store_subject_count == true) {
			return redirect()->route('attendance')->with('success', 'Attendance Submited successfylly..');
		} else {
			return redirect()->route('attendance')->with('error', 'Sorry Something went wrong ..');

		}

	}

	public function defaulterList() {

		$data['semisters'] = DB::table('semisters')->get();

		return view('attendance.defaulter', $data);
	}
	public function viewDefaulter(Request $request) {

		$data["semister"] = $this->semister->where("id", $request->semister_id)->with("subjects")->first();
		$data["students"] = $this->year->find($data["semister"]->year_id)->students;

		// DB::table('semisters')->where('id', $request->semister_id)->first();
		$data['subjects'] = DB::table('subjects')->where('semister_id', $request->semister_id)
			->get();
		// $data['students'] = $this->studen
		// 	->where('year_id', $semister->year_id)
		// 	->get();
		//dd($data['students']);

		for ($i = 0; $i < count($data['subjects']); $i++) {
			$data['total_subjects'][$i] = $this->subject->find($data['subjects'][$i]->id)->attendenceSubjectCounts;
		}

		for ($i = 0; $i < count($data['subjects']); $i++) {

			foreach ($data["students"] as $student) {
				$data["records"][$i][] = [
					// "student_name" => $student->student_name,
					"attendance_count" => DB::table('attendances')
						->whereBetween('attendance_date', [$request->start_date, $request->end_date])
						->where('student_id', $student->id)
						->where('subject_id', $data['subjects'][$i]->id)
						->select('status')
						->count(),
				];

				// $data['student_count'][$j][$i] = DB::table('attendances')
				// 	->whereBetween('attendance_date', [$request->start_date, $request->end_date])
				// 	->where('student_id', $data['students'][$j]->id)
				// 	->where('subject_id', $data['subjects'][$i]->id)
				// 	->select('status')
				// 	->count();

				// $data['lecture_count'][$j] = DB::table('attendances')
				// 	->whereBetween('attendance_date', [$request->start_date, $request->end_date])
				// 	->where('student_id', $data['students'][$j]->id)
				// 	->select('status')
				// 	->count();
			}
		}

		// dd($data['student_count']);
		$result = DB::table('attendances')
			->whereBetween('attendance_date', [$request->start_date, $request->end_date])
			->get();
		// dd($result);

		return view('attendance.view-defaulter-list', $data);
	}
}
