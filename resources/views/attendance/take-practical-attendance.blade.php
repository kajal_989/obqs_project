@extends('layouts.admin_layout')
@section('content')
   <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Subject Name:  {{$subject->name}}</h2>
                        </div>
                        <div class="body">
  <form action="{{route('save-practical-attendance')}}" method="post">
                     {{ csrf_field() }}
<div class="table-responsive">
  <table class="table">
    <td> Date: <input type ="date" name="attendance_date" class="form-control" required="true">
<!-- <script> document.write(new Date().toLocaleDateString()); </script> -->
</td>
    <td>
  Time From : <input type="time" name="from_time" class="form-control" required="true">
</td>
  <td>
  Time To : <input type="time" name="to_time" class="form-control" required="true">
</td>
<td>
 <!--select class="form-control" id="att" name="att" onchange="uncheckAll()">
                        <option value="p">Present</option>
                        <option value="a">Absent</option>
                      </select-->
                    </td>
                    <td>

                       <div class="demo-radio-button" id="att" onchange="uncheckAll()">
                                <input  name="group1" type="radio" id="present" value="p" checked="true" />
                                <label for="present">Present Students</label>
                                <input  name="group1" value="a" type="radio" id="absent" />
                                <label for="absent">Absent Students</label>

                            </div>
                    </td>

                    </table>

                    </div>

                             @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                  <div class="table-responsive">

                    <input type="hidden" name="acadmic_year_id" value="{{$subject->acadmic_year_id}}">
                    <input type="hidden" name="semister_id" value="{{$subject->semister_id}}">
                    <input type="hidden" name="subject_id" value="{{$subject->id}}">
                    <input type="hidden" name="batch_id" value="{{$batch_id}}">

                <table  class="table table-js-exportable">
                 <thead>
                      <th>Roll NO</th>
                      <th>Student Name</th>
                      <th>Status</th>
                    </thead>

                  <tbody>
                    @foreach($students as $student)
                    <tr>
                      <td>{{$student->roll_no}}</td>
                      <td>{{$student->student_name}}</td>
                      <td>
                    <label class="switch">
                    <input type="checkbox" name="present_students[]" id="status" value="{{$student->id}}" checked="true" >
                     <span class="slider round"></span>
                    </label>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
              <center><input class="btn btn-primary" type="submit" name="submit" value="Submit"></center>

         </form>

           <script type="text/javascript">


                     function uncheckAll(){

                        var value= $("input:radio[name=group1]:checked").val();
                        console.log(value);

                          if(value == "p")
                        {
                          console.log("first if executed");
                         //  $('input[name=present_students]').prop('checked', true);
                           $('input[type="checkbox"]:unchecked').prop('checked',true);
                        }
                         else if(value == "a")
                        {
                          console.log("second if executed");
                         //  $('input[name=present_students]').prop('checked', false);
                         $('input[type="checkbox"]:checked').prop('checked',false);
                        }


                       // var r1 = document.getElementById('att').value;
                       // console.log(r1);
                       //  if(r1 == "p")
                       //  {
                       //    console.log("first if executed");
                       //   //  $('input[name=present_students]').prop('checked', true);
                       //     $('input[type="checkbox"]:unchecked').prop('checked',true);
                       //  }
                       //   else if(r1 == "a")
                       //  {
                       //    console.log("second if executed");
                       //   //  $('input[name=present_students]').prop('checked', false);
                       //   $('input[type="checkbox"]:checked').prop('checked',false);
                       //  }

                }
                     </script>
            </div>
        </div>
    </div>
  </div>
</div>

</section>
@endsection