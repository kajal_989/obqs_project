@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Defaulter List</h2>
            </div>
              <div class="body">
                 @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_1">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1"> Theory Defaulter List
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_1" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_1">
                      <div class="panel-body">
                         <form method="get" action="{{ route('view-defaulter-list') }}">
                             {{ csrf_field() }}
                             <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                          <div class="row clearfix">
                            <div class="col-sm-3">
                               <label>Select Class</label>
                              <select class="form-control" name = "semister_id" id="semister_id" onchange="myfun()" required>
                                <option value="">--Select Semister--</option>
                                  @foreach($semisters as $semister)
                                  <option value="{{$semister->id}}">{{$semister->short_name}}</option>
                                  @endforeach
                              </select>
                            </div>

                            <div class="col-sm-3">
                              <label>From</label>
                             <input type="date" name="start_date" class="datepicker form-control" placeholder="Please choose a date...">
                            </div>

                               <div class="col-sm-3">
                              <label>To</label>
                             <input type="date" name="end_date" class="datepicker form-control" placeholder="Please choose a date...">
                            </div>

                            <div class="col-sm-3">
                               <label>Select Elective (IF HAVE)</label>
                              <select class="form-control" name = "elective_subject_id" id="elective_subject_id">
                                 <option value="">--select Elective Subject-</option>
                              </select>
                               <br>

                            </div>

                        </div>
                        <center>
                         <input type="submit" class="btn btn-primary" name="submit" value="Get Defaulter List">&nbsp;
                              <input type="submit" class="btn btn-info" name="submit" value="Download">
                            </center>

                      </form>
                      </div>
                    </div>
                  </div>
                  <script type="text/javascript">

function myfun() {
    var semister_id =document.getElementById('semister_id').value;
    console.log(semister_id);

    $.ajax({
             type: 'post',
             url: '{{ route('select-elective') }}',
            data:{
                    "semister_id":semister_id,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log(data);
                $('#elective_subject_id').children('option:not(:first)').remove();
                var html = '';
            for(var i = 0; i < data.length; i++){
console.log(data[i].id);
     var option=$('<option value='+data[i].elective_subject_id+'>'+data[i].name+'</option>');
  $('#elective_subject_id').append(option);

}
}
});
}

function final() {
    var final_semister_id =document.getElementById('final_semister_id').value;
    console.log(semister_id);

    $.ajax({
             type: 'post',
             url: '{{ route('select-elective') }}',
            data:{
                    "semister_id":final_semister_id,
                 },
             async: true,
           dataType: 'json',
             success: function (data) {
              console.log(data);
                $('#final_elective_subject_id').children('option:not(:first)').remove();
                var html = '';
            for(var i = 0; i < data.length; i++){
console.log(data[i].id);
     var option=$('<option value='+data[i].elective_subject_id+'>'+data[i].name+'</option>');
  $('#final_elective_subject_id').append(option);

}
}
});
}
                  </script>


                    <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_2">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_2" aria-expanded="false" aria-controls="collapseOne_1"> Practical Defaulter List
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_2" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_2">
                      <div class="panel-body">
                         <form method="get" action="{{ route('view-practical-defaulter-list') }}">
                             {{ csrf_field() }}
                             <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                          <div class="row clearfix">
                            <div class="col-sm-3">
                               <label>Select Semister</label>
                              <select class="form-control" name = "semister_id" id="semister_id" onchange="myfun()" required>
                                <option value="">--Select Semister--</option>
                                  @foreach($semisters as $semister)
                                  <option value="{{$semister->id}}">{{$semister->short_name}}</option>
                                  @endforeach
                              </select>
                            </div>
                              <div class="col-sm-3">
                               <label>Batch</label>
                              <select class="form-control" name = "batch_id" id="attendance_type" required>
                                <option value="">--Select batch--</option>
                                  <option value="1">Batch I</option>
                                  <option value="2">Batch II</option>
                                  <option value="3">Batch III</option>
                                  <option value="4">Batch IV</option>
                              </select>
                            </div>
                            <div class="col-sm-3">
                              <label>From</label>
                             <input type="date" name="start_date" class="datepicker form-control" placeholder="Please choose a date...">
                            </div>

                               <div class="col-sm-3">
                              <label>To</label>
                             <input type="date" name="end_date" class="datepicker form-control" placeholder="Please choose a date...">
                            </div>

                        </div>
                        <center>
                         <input type="submit" class="btn btn-primary" name="submit" value="Get Defaulter List">&nbsp;
                              <input type="submit" class="btn btn-info" name="submit" value="Download">
                            </center>

                      </form>
                      </div>
                    </div>
                  </div>



                    <div class="panel panel-info">
                  <div class="panel-heading" role="tab" id="headingOne_3">
                    <h4 class="panel-title">
                      <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseOne_3" aria-expanded="false" aria-controls="collapseOne_3"> Final Defaulter List
                      </a>
                    </h4>
                    </div>
                    <div id="collapseOne_3" class="panel-collapse collapse in" role="tabpanel"aria-labelledby="headingOne_3">
                      <div class="panel-body">
                         <form method="get" action="{{ route('view-final-defaulter-list') }}">
                             {{ csrf_field() }}
                             <input type="hidden" name="acadmic_year_id" value="{{$acyear->id}}">
                          <div class="row clearfix">
                            <div class="col-sm-3">
                               <label>Select Semister</label>
                              <select class="form-control" name = "semister_id" id="final_semister_id" onchange="final()" required>
                                <option value="">--Select Semister--</option>
                                  @foreach($semisters as $semister)
                                  <option value="{{$semister->id}}">{{$semister->short_name}}</option>
                                  @endforeach
                              </select>
                            </div>
                              <div class="col-sm-3">
                               <label>Batch</label>
                              <select class="form-control" name = "batch_id" id="attendance_type" required>
                                <option value="">--Select batch--</option>
                                  <option value="1">Batch I</option>
                                  <option value="2">Batch II</option>
                                  <option value="3">Batch III</option>
                                  <option value="4">Batch IV</option>
                              </select>
                            </div>
                            <div class="col-sm-3">
                              <label>From</label>
                             <input type="date" name="start_date" class="datepicker form-control" placeholder="Please choose a date...">
                            </div>

                               <div class="col-sm-3">
                              <label>To</label>
                             <input type="date" name="end_date" class="datepicker form-control" placeholder="Please choose a date...">
                            </div>

                            <div class="col-sm-3">
                               <label>Select Elective (IF HAVE)</label>
                              <select class="form-control" name = "elective_subject_id" id="final_elective_subject_id" >
                                 <option value="">--select Elective Subject-</option>
                              </select>
                               <br>

                            </div>
                        </div>
                        <center>
                         <input type="submit" class="btn btn-primary" name="submit" value="Get Defaulter List">&nbsp;
                              <input type="submit" class="btn btn-info" name="submit" value="Download">
                            </center>

                      </form>
                      </div>
                    </div>
                  </div>


    </section>

@endsection