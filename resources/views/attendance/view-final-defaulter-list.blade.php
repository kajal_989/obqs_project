@extends('layouts.admin_layout')
@section('content')

	 <section class="content">

            <div class="row">


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                           <img src="{{asset('/images/logo2.png')}}" hspace="20" height="100px" width="100px" align="right">
          <img src="{{asset('/images/logo1.png')}}" hspace="20"" height="100px" width="100px"  align="left">

          <div class="header">
          <center>
               <h3>Bharti Vidyapeeth Collage of Engineering, Navi Mumbai</h3>
               <h4>Department Of Information Technology</h4>
          </center>
            </div>
                        <div class="body">
                             <center>

                                   <h5>Acadmic Year  {{$headers->from_year}} - {{$headers->to_year}} </h5>
                                   <h5>Semister: {{$headers->short_name}}</h5>
                                   <h5>Defaulter List From Date: {{$start_date}}
                                    To Date: {{ $end_date}}
                                   </h5>
                              </center>
                              <br>
                                    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                           <thead>
                              <tr>
                                  <th>Roll No</th>
                                  <th>Student Name</th>

                                  <th>Percent Attendance (theory)<br>100%</th>
                                  <th>Percent Attendance (Practical)</th>
                                  <th>Total Percent Attendance</th>
                                  <th>Signature</th>
                              </tr>
                            </thead>
                          <tbody>
                    @if($final)
                    @for($i=0; $i<count($final);$i++)

<tr>
     <td>{{$final[$i]['roll_no']}}</td>
     <td>{{$final[$i]['student_name']}}</td>
<td>{{round($final[$i]['precent_attendance'],2)}}</td>
<td>{{round($final[$i]['precent_practical_attendance'],2)}}</td>
<td>{{(round($final[$i]['precent_attendance'],2) + round($final[$i]['precent_practical_attendance'],2))/2}}</td>
<td></td>
</tr>

 @endfor
 @endif
                     </tbody>
                    </table>
                </div>
               </div>
             </div>
          </div>
        </div>
    </div>
  </div>
 </div>

 </section>
@endsection