@extends('layouts.admin_layout')
@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>{{$subjects}} &nbsp;&nbsp;
                            </h2>
                        </div>
                        <div class="body">
                     <div class="row clearfix">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="table-responsive">
                       <table class="table table-bordered table-striped ">
                           <thead>
                              <tr>
                                  <th>Roll No</th>
                                  <th>Student Name</th>
                                  @foreach($result as $r)
                                  <th>{{$r->attendance_date}} <br>({{$r->attendance_time}})</th>
                                  @endforeach
                                  <th>Present Lectures<br>
                                    Out Of({{count($result)}})
                                  </th>
                              </tr>
                            </thead>
                          <tbody>

                              @for($i=0; $i< count($records); $i++)
 <tr>
                              <td>{{$records[$i]["roll_no"]}}</td>
                               <td>{{$records[$i]["student_name"]}}</td>
                              <?php $c = 0?>
                              @for($j=0; $j< count($result);$j++)
                               <td>@if($records[$i]["subject"][$j]==1)
                                   <?php $c++;?>

                                   <center><div style="height: 20px; width: 20px; background: green"  data-toggle="tooltip" data-placement="bottom" title="{{$c}}"></div></center>
                                   @endif
                                   @if($records[$i]["subject"][$j]==0)
                                   <center><div style="height: 20px; width: 20px; background: red"></div></center>
                                   @endif

                               </td>
                             @endfor
                             <td><center>{{$records[$i]["total"]}}</center></td>
                        </tr>
                             @endfor


                     </tbody>
                    </table>
                </div>

               </div>
             </div>
          </div>
        </div>
    </div>
  </div>
 </div>

 </section>
@endsection