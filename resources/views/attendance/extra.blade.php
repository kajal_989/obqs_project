@extends('layouts.admin_layout')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>Extra Attendances</h2>
            </div>
              <div class="body">
                 @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif

                        <form method="get" action="{{ route('store-extra-attendance') }}">

                            <div class="table-responsive">
                            <table class="table table-striped table-hover">
                              <thead>
                                <th>Roll NO</th>
                                <th>Student Name</th>
                                <th>Previous Extras</th>
                                <th>Extras</th>
                              </thead>
                              <tbody>
                                @foreach($students as $student)
                                <tr>
                                  <td>{{$student->roll_no}}</td>
                                  <td>{{$student->student_name}}</td>
                                  <td>{{$student->extra}}</td>
                                  <td>
                                    <input type="hidden" name="id[]" value="{{$student->id}}">
                                    <input type="text" class="form-control"  name="extra[]"></td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                          <center>
                          <input type="submit" class="btn btn-info" name="finish" value="Finish">
                        </center>

                      </form>


    </section>

@endsection