@extends('layouts.admin_layout')

@section('content')
<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>My Profile</h2>
            </div>
                <div class="body">
                    <div class="row clearfix">

                                <center class="m-t-30"> <img src="{{url('images/common.png')}}" class="img-circle" width="150" />
                                   </center>
                                   <center>
                                    <h4 class="card-title m-t-10"><br>{{Auth::user()->name}}</h4>
                                    <h6 class="card-subtitle">{{Auth::user()->email}}</h6><br>
                                     <h6>Department: IT Department</h6>
                                  </center>
                                 <br>


                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                                <br>
                              <center><h4 class="card-title m-t-10">Your Personal Details</h4></center>

                              <br>
                              <center>
                                <form class="form-horizontal form-material">
                                    <div class="form-group">
                                        <label class="col-md-12">Full Name</label>
                                        <div class="col-md-12">
                                            <input type="text" value=""  class="form-control form-control-line" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" value=class="form-control form-control-line" name="example-email" id="example-email" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-12">Phone No</label>
                                        <div class="col-md-12">
                                            <input type="text" value='' class="form-control form-control-line" disabled>
                                        </div>
                                    </div>


                                </form>
                              </center>
                            </div>
                        </div>
                    </div>

                </div>
<br>
 <ol class="breadcrumb">
 <li class="breadcrumb-item "><b>Change Password</b></li>
 </ol>

    <div class="card">
     <div class="card-body">
        <form action="" method="post">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Your Old Password</label>
                  <input class="form-control" id="exampleInputName" type="password" name="opass" aria-describedby="nameHelp" placeholder="old password"  required>
              </div>
              <div class="col-md-6">
                <label for="exampleInputLastName">New Password</label>
                <input class="form-control" id="exampleInputLastName" type="password" name="npass" aria-describedby="nameHelp" placeholder="new password" required>
              </div>
            </div>
          </div>
          <input type="submit" name="submit" class="btn btn-primary btn-block" value="Change Password">
         </form>
          </div>
         </div>
      </div>
      </div>
    </div>
  </div>
</section>
@endsection
