@extends('layouts.admin_layout')
@section('content')
	 <section class="content">
  <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name}}</div>
                    <div class="email">{{ Auth::user()->email}}</div>

                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">

                    <li>
                        <a href="{{ url('/home') }}">
                            <i class="material-icons">person</i>
                            <span>Faculty Management</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="{{ url('/subject') }}">
                            <i class="material-icons">text_fields</i>
                            <span>Subject Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/po') }}">
                            <i class="material-icons">layers</i>
                            <span>PO Statements</span>
                        </a>
                    </li>


                    <li>
                        <a href="{{ url('/pso') }}" >
                            <i class="material-icons">swap_calls</i>
                            <span>PSO Statements</span>
                        </a>
                    </li>


                    <li>
                        <a href="{{ url('/students') }}" >
                            <i class="material-icons">people</i>
                            <span>Manage Students</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/course_file') }}" >
                            <i class="material-icons">collections_bookmark</i>
                            <span>Course File</span>
                        </a>
                    </li>

                      <li>
                        <a href="{{ url('/attendance') }}" >
                            <i class="material-icons">event_available</i>
                            <span>Attendance</span>
                        </a>
                    </li>

                      <li>
                        <a href="{{ url('/qpgen') }}" >
                            <i class="material-icons">picture_as_pdf</i>
                            <span>Question Paper Generation</span>
                        </a>
                    </li>

                      <li>
                        <a href="{{ url('/myprofile') }}" >
                            <i class="material-icons">person</i>
                            <span>My Profile</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->

        <!-- #END# Right Sidebar -->
    </section>

            <!-- Example Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Subject Management

                            </h2>

                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#home" data-toggle="tab">Add Acadmic Year</a></li>
                                <li role="presentation"><a href="#profile" data-toggle="tab">Add Subjects</a></li>
                                <li role="presentation"><a href="#messages" data-toggle="tab"></a></li>
                                <li role="presentation"><a href="#settings" data-toggle="tab">Allocate Subjects to faculty</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">

                                    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="block-header">

            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>CODE</th>
                                            <th>SHORT NAME</th>
                                            <th>ACTION</th>

                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                           <th>Sr No</th>
                                            <th>CODE</th>
                                            <th>SHORT NAME</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                     </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                    <b>Profile Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                        pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                        sadipscing mel.
                                    </p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="messages">
                                    <b>Message Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                        pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                        sadipscing mel.
                                    </p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="settings">
                                    <b>Settings Content</b>
                                    <p>
                                        Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                        Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                        pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                        sadipscing mel.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Example Tab -->

         </div>

    </section>
@endsection