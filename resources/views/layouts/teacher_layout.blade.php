<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>OBQAS</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>home</title>

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    {{ Html::style('plugins/bootstrap/css/bootstrap.css') }}
    {{ Html::style('plugins/node-waves/waves.css') }}
    {{ Html::style('plugins/animate-css/animate.css') }}
    {{ Html::style('plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}
    {{ Html::style('css/style.css') }}

    {{ Html::style('css/themes/all-themes.css') }}

</head>

<body class="theme-red">

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">

        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">OBQAS</a>

            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li>
                        <a type="button"  class="btn btn-danger" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>

                  <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>

        </aside>
            </div>
        </div>
    </nav>

 <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="{{url('images/user.png')}}" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Prof. {{ Auth::user()->name}} </b></div>
                    <div class="email">{{Auth::user()->email}}
                    </div>
                    <h4>{{Auth::user()->display_name}}</h4>

                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">




       <!-- program outcomes route and menu -->
                    <li <?php if (Route::currentRouteName() == 'program-outcomes') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/program-outcomes') }}">
                            <i class="material-icons">layers</i>
                            <span>Program Outcomes</span>
                        </a>
                    </li>



 <!-- Student managemnet -->


   <li <?php if (Route::currentRouteName() == 'student-management') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/student-management') }}" >
                            <i class="material-icons">face</i>
                            <span>Student Management</span>
                        </a>
                    </li>


 <!-- Unit Test managemnet
   <li <?php if (Route::currentRouteName() == 'test-management') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/test-management') }}" >
                            <i class="material-icons">assignment</i>
                            <span>Test Management</span>
                        </a>
                    </li>
-->
  <!-- Test management -->
                    <li <?php if (Route::currentRouteName() == "question-paper" || Route::currentRouteName() == 'question-bank' || Route::currentRouteName() == 'test-marks') {echo 'class="active"';}
?> >
                         <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">text_fields</i>
                            <span>Test Management</span>
                        </a>

                          <ul class="ml-menu">
                            <li <?php if (Route::currentRouteName() == "question-bank") {
	echo 'class="active"';
}
?>>
                                <a href="{{url('test-management/question-bank')}}">Question Bank</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'question-paper') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('test-management/question-paper')}}">Question Paper</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'test-marks') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/test-marks')}}">Test Marks</a>
                            </li>


                         </ul>
                    </li>


                    <li <?php if (Route::currentRouteName() == 'course-file') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/course-file') }}" >
                            <i class="material-icons">collections_bookmark</i>
                            <span>Course File</span>
                        </a>
                    </li>

                    <!-- Attendance management -->

                        <li <?php if (Route::currentRouteName() == "attendance") {echo 'class="active"';}
?> >
                         <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">event_available</i>
                            <span>Attendance</span>
                        </a>

                          <ul class="ml-menu">
                            <li <?php if (Route::currentRouteName() == "attendance") {
	echo 'class="active"';
}
?>>
                                <a href="{{url('test-management/question-bank')}}">Mark Attemdance</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'defaulter') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('test-management/question-paper')}}">Defaulter list</a>
                            </li>

                         </ul>
                    </li>




                      <li <?php if (Route::currentRouteName() == 'myprofile') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/myprofile') }}" >
                            <i class="material-icons">account_circle</i>
                            <span>My Profile</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 - 2018 <a href="javascript:void(0);"></a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>

        </aside>
        <!-- #END# Right Sidebar -->


    </section>

    @yield('content')
    <!-- Scripts -->
    {{ Html::script('plugins/jquery/jquery.min.js') }}
    {{ Html::script('plugins/bootstrap/js/bootstrap.js') }}

    {{ Html::script('plugins/jquery-slimscroll/jquery.slimscroll.js') }}
    {{ Html::script('plugins/node-waves/waves.js') }}

    {{ Html::script('js/pages/ui/tooltips-popovers.js') }}

{{ Html::script('plugins/bootstrap-notify/bootstrap-notify.js') }}


   {{ Html::script('plugins/jquery-countto/jquery.countTo.js') }}
    {{ Html::script('plugins/bootstrap-notify/bootstrap-notify.js') }}
    {{ Html::script('plugins/jquery-sparkline/jquery.sparkline.js') }}
    {{ Html::script('js/admin.js') }}
    {{ Html::script('js/pages/index.js') }}
    {{ Html::script('js/demo.js') }}
   {{ Html::script('plugins/jquery-validation/jquery.validate.js') }}
    {{ Html::script('js/pages/ui/dialogs.js') }}

    {{ Html::script('plugins/jquery-datatable/jquery.dataTables.js') }}
    {{ Html::script('plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}
    {{ Html::script('plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}
    {{ Html::script('plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}
    {{ Html::script('plugins/jquery-datatable/extensions/export/jszip.min.js') }}
    {{ Html::script('plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}
    {{ Html::script('plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}
    {{ Html::script('plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}

    {{ Html::script('plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}
    {{ Html::script('js/pages/tables/jquery-datatable.js') }}



</body>
</html>