<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>OBQAS</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>home</title>

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="plugins/animate-css/animate.css" rel="stylesheet" />

  <!-- JQuery DataTable Css -->
    <link href="plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="css/themes/all-themes.css" rel="stylesheet" />

</head>

<body class="theme-red">

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">OBQAS</a>

            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                    <li>
                        <a type="button"  class="btn btn-danger" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>

                </ul>

                  <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>

        </aside>
            </div>
        </div>
    </nav>

 <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Prof. {{ Auth::user()->name}} </b></div>
                    <div class="email">{{ Auth::user()->email}}</div>

                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">

            <!-- Acadmic year -->
                       <li <?php if (Route::currentRouteName() == 'acadmic-year' || Route::currentRouteName() == 'manage-year') {
	echo 'class="active"';
}
?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">event_available</i>
                            <span>Acadmic Year</span>
                        </a>

                        <ul class="ml-menu">
                            <li <?php if (Route::currentRouteName() == 'acadmic-year') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/home')}}">Current Year</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'manage-year') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/manage-year')}}">Manage Years</a>
                            </li>
                        </ul>
                    </li>


<!-- faculty -->
                    <li <?php if (Route::currentRouteName() == 'faculty-view' || Route::currentRouteName() == 'faculty-add' || Route::currentRouteName() == 'faculty-class') {
	echo 'class="active"';
}
?>>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">person</i>
                            <span>Faculty Management</span>
                        </a>

                        <ul class="ml-menu">
                            <li <?php if (Route::currentRouteName() == 'faculty-view') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/faculty-view')}}">View Faculties</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'faculty-class') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/faculty-class')}}">Class Advisers</a>
                            </li>
                         </ul>
                    </li>


    <!-- Subject menu -->
                    <li <?php if (Route::currentRouteName() == "subjects-view" || Route::currentRouteName() == 'subject-teachers') {echo 'class="active"';}
?> >
                         <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">text_fields</i>
                            <span>Subject Management</span>
                        </a>

                          <ul class="ml-menu">
                            <li <?php if (Route::currentRouteName() == "subjects-view") {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/subjects-view')}}">Subjects</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'subject-teachers') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/subject-teachers')}}">Subject Teachers</a>
                            </li>


                         </ul>
                    </li>

       <!-- program outcomes route and menu -->
                    <li <?php if (Route::currentRouteName() == 'program-outcomes') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/program-outcomes') }}">
                            <i class="material-icons">layers</i>
                            <span>PO Statements</span>
                        </a>
                    </li>

<!-- program pso menu and routes -->
                    <li <?php if (Route::currentRouteName() == 'pso') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/pso') }}" >
                            <i class="material-icons">swap_calls</i>
                            <span>PSO Statements</span>
                        </a>
                    </li>
 <!-- Students menu -->
                    <li <?php if (Route::currentRouteName() == "se-students" || Route::currentRouteName() == 'te-students' || Route::currentRouteName() == 'be-students') {echo 'class="active"';}
?> >
                         <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">people</i>
                            <span>Student Management</span>
                        </a>

                          <ul class="ml-menu">
                            <li <?php if (Route::currentRouteName() == "se-students") {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/se-students')}}">Second Year (SE)</a>
                            </li>

                            <li <?php if (Route::currentRouteName() == 'te-students') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/te-students')}}">Third Year (TE)</a>
                            </li>

                             <li <?php if (Route::currentRouteName() == 'be-students') {
	echo 'class="active"';
}
?>>
                                <a href="{{url('/be-students')}}">Fourth Year (BE)</a>
                            </li>


                         </ul>
                    </li>



                    <li <?php if (Route::currentRouteName() == 'course-file') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/course-file') }}" >
                            <i class="material-icons">collections_bookmark</i>
                            <span>Course File</span>
                        </a>
                    </li>

                      <li <?php if (Route::currentRouteName() == 'attendance') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/attendance') }}" >
                            <i class="material-icons">event_available</i>
                            <span>Attendance</span>
                        </a>
                    </li>

                      <li <?php if (Route::currentRouteName() == 'qpgen') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/qpgen') }}" >
                            <i class="material-icons">picture_as_pdf</i>
                            <span>Question Paper Generation</span>
                        </a>
                    </li>

                      <li <?php if (Route::currentRouteName() == 'myprofile') {
	echo 'class="active"';
}
?>>
                        <a href="{{ url('/myprofile') }}" >
                            <i class="material-icons">person</i>
                            <span>My Profile</span>
                        </a>
                    </li>

                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 - 2018 <a href="javascript:void(0);"></a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.5
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
            </ul>

        </aside>
        <!-- #END# Right Sidebar -->


    </section>

    @yield('content')
    <!-- Scripts -->




    <!-- Jquery Core Js -->
    <script src="plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="plugins/bootstrap/js/bootstrap.js"></script>


    <!-- Slimscroll Plugin Js -->
    <script src="plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="plugins/node-waves/waves.js"></script>

    <!-- Jquery CountTo Plugin Js -->
    <script src="plugins/jquery-countto/jquery.countTo.js"></script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="plugins/bootstrap-notify/bootstrap-notify.js"></script>

    <!-- Sparkline Chart Plugin Js -->
    <script src="plugins/jquery-sparkline/jquery.sparkline.js"></script>

    <!-- Custom Js -->
    <script src="js/admin.js"></script>
    <script src="js/pages/index.js"></script>

    <!-- Demo Js -->
    <script src="js/demo.js"></script>

 <script src="plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->

    <script src="js/pages/ui/dialogs.js"></script>

     <!-- Jquery DataTable Plugin Js -->
    <script src="plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->

    <script src="js/pages/tables/jquery-datatable.js"></script>

    <!-- jquery support -->


    @yield('custom-js')

</body>
</html>