<div class="row">
  @php
    $index = 0;
  @endphp
  @foreach ($items as $item)
<div class="col-sm-6">
  <div class="form-line">
          @php
            $stringFormat =  strtolower(str_replace(' ', '', $item));
          @endphp
          <div class="col-sm-9">
            <input value="{{isset($oldVals) ? $oldVals[$index] : ''}}" type="text" class="form-control" name="<?=$stringFormat?>" id="input<?=$stringFormat?>" placeholder="{{$item}}">
          </div>

      </div>

    </div>
  @php
    $index++;
  @endphp
  @endforeach
</div>
<div class="col-sm-9">
          <button type="submit" class="btn btn-primary">
     <i class="material-icons">search</i> Search Student
    </button>
  </div>
