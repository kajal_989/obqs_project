<?php

namespace App\Http\Controllers\Faculty;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ClassAdviserController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		$data['ca'] = DB::table('class_advisers')
			->join('semisters', 'class_advisers.semister_id', '=', 'semisters.id')
		//->join('years', 'class_advisers.year_id', '=', 'years.id')
			->join('users', 'class_advisers.user_id', '=', 'users.id')
			->select('semisters.short_name', 'users.name')
			->get();
		//	dd($data['ca']);
		$data['semisters'] = DB::table('semisters')->get();
		$data['faculties'] = DB::table('users')->get();

		return view('acadmic.class-adviser', $data);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {

		$now = Carbon::now();

		// $year = DB::table('semisters')->where('id', '=', )->select('id')->first();

		for ($i = 0; $i < count($request->sem); $i++) {

			$semister = DB::table('semisters')->where('id', $request->sem[$i])->first();

			$check = DB::table('class_advisers')
				->where("semister_id", $request->sem[$i])
				->where("user_id", $request->faculty[$i])
				->where("acadmic_year_id", $semister->year_id)
				->exists();

			if ($check == true) {

				\Session::flash('errormsg', 'Already Exists');

			} else {
				$data[] = [
					"acadmic_year_id" => $semister->year_id,
					"semister_id" => $request->sem[$i],
					"user_id" => $request->faculty[$i],
					"created_at" => $now,
					"updated_at" => $now,
				];

				DB::table('class_advisers')->insert($data);

				\Session::flash('successmsg', 'Class Adviser added successfully');
			}

		}
		return redirect()->back();

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}
}
