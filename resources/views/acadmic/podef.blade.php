@extends('layouts.admin_layout')

@section('content')


     <section class="content">


            <!-- Example Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                              Program Outcome Statements

                            </h2>

                        </div>
                        <div class="body">
                            <!-- Nav tabs -->

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">

                                    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="block-header">

            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Sr No</th>
                                            <th>Name</th>
                                            <th>Statement</th>
                                            <th>ACTION</th>

                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                           <th>Sr No</th>
                                            <th>CODE</th>
                                            <th>SHORT NAME</th>
                                            <th>ACTION</th>
                                        </tr>
                                    </tfoot>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Example Tab -->

         </div>

    </section>
@endsection