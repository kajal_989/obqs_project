@extends('layouts.admin_layout')
@section('content')
<section class="content">
 <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="card">
          <div class="header">
              <h2>Program Outcome Statements &nbsp;</h2>
          </div>
          <div class="body">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                              @if (Session::has('message'))
        <h4 style="color:green">{!! session('message') !!}</h4>
   @endif

    @if (Session::has('success'))
            <h5 style="color:green">{!! session('success') !!}</h5>
    @endif

    @if (Session::has('error'))
        <h5 style="color:red">{!! session('error') !!}</h5>
    @endif
    <div class="table table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="table">
      <tr>
        <th width="150px">Sr No.</th>
        <th>PO Reference</th>
        <th>PO Statement</th>
        <th>Actions</th>
      </tr>
      {{ csrf_field() }}
      <?php $no = 1;?>
      @foreach ($pos as $po)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $po->name }}</td>
          <td>{{ $po->statement }}</td>
          <td>
            <button class="edit-modal btn btn-warning btn-sm" data-id="{{$po->id}}" data-name="{{$po->name}}" data-statement="{{$po->statement}}">
               <i class="material-icons">edit</i>
            </button>
            <button class="delete-modal btn btn-danger btn-sm" data-id="{{$po->id}}" data-name="{{$po->name}}" data-statement="{{$po->statement}}">
               <i class="material-icons">delete</i>
            </button>
          </td>
        </tr>
      @endforeach
    </table>
  </div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
@endsection
