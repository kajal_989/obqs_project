@extends('layouts.admin_layout')
@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="card">
                        <div class="header">
                            <h2>Subject list  &nbsp; <button class="btn btn-primary" data-toggle="modal" data-target="#add3Modal">Add New Subject</button> </h2>
                        </div>
                        <div class="body">
                             @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                  <div class="table-responsive">
                          <table  class="table table-js-exportable">
                 <thead>
                      <th>Sr No</th>
                      <th>Semister</th>
                      <th>Cource Code</th>
                       <th>Subject Name</th>
                       <th>Short Name</th>
                       <th>Action</th>
                    </thead>

                  <tbody>
                    @foreach($subjects as $subject)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{$subject->sem}}</td>
                      <td>{{$subject->course_code}}</td>
                      <td>{{$subject->name}}</td>
                      <td>{{$subject->short_name}}</td>
                      <td>
                         <a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$subject->id}}" data-sem="{{$subject->sem}}" data-course_code="{{$subject->course_code}}" data-name="{{$subject->name}}" data-short_name="{{$subject->short_name}}" >
              <i class="glyphicon glyphicon-eye-open" ></i>
            </a>
            <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{$subject->id}}" data-sem="{{$subject->sem}}" data-course_code="{{$subject->course_code}}" data-name="{{$subject->name}}" data-short_name="{{$subject->short_name}}" >
              <i class="glyphicon glyphicon-pencil"></i>
            </a>
            <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{$subject->id}}" data-sem="{{$subject->sem}}" data-course_code="{{$subject->course_code}}" data-name="{{$subject->name}}" data-short_name="{{$subject->short_name}}" >
              <i class="glyphicon glyphicon-trash"></i>
            </a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
              </table>
            </div>
        </div>
    </div>
  </div>
</div>

  <script type="text/javascript">
      function addRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  if(rowCount < 20){             // limit the user from creating fields more than your limits
    var row = table.insertRow(rowCount);
    var colCount = table.rows[0].cells.length;
    for(var i=0; i<colCount; i++) {
      var newcell = row.insertCell(i);
      newcell.innerHTML = table.rows[1].cells[i].innerHTML;
    }
  }else{
     alert("Maximum rows 10");

  }
}

function deleteRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all Records.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
 </script>


<!--  modal adding sub -->

     <div class="modal fade" id="add3Modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="defaultModalLabel">Add Subjects</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" class="form-horizontal" action="{{ route('subjects-store') }}">
                             {{ csrf_field() }}
             <div class="table-responsive">
               <table id="dataTable" class="table table-bordered table-striped ">
                 <thead>
                      <th>select</th>
                      <th>Semister</th>
                      <th>Course Code</th>
                       <th>Subject Name(Full)</th>
                       <th>Subject short Name</th>
                       <th>Subject Type</th>
                       <th>is Elective</th>

                    </thead>
             <tbody>
             <tr>
             <p>
            <td><input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked required/></td>
            <td>
               <select class="form-control" name="semister_id[]" required>
                <option value="">--Please select semister--</option>
                @foreach($semister as $sem)
                <option value="{{$sem->id}}">{{$sem->short_name}}</option>
                @endforeach
                 </select>
            </td>
            <td>
              <input type="text" class="form-control placeholder="Course Code" name="course_code[]" required>
            </td>
            <td>
              <input type="text" class="form-control placeholder="Subject Name" name="name[]" required>
            </td>
            <td>
              <input type="text" class="form-control placeholder="short name" name="short_name[]" required>
            </td>
            <td>
              <select name="subject_type[]" class="form-control" id="subject_type" required>
                <option value="">--please select type--</option>
                <option value="theory">Theory</option>
                <option value="practical">Practical</option>

              </select>
            </td>
            <td>
                <select name="is_elective[]" class="form-control required>
                    <option value="">--please select--</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                </select>
            </td>
            </p>
            </tr>
            </tbody>
            </table>
            <br>
            <p>
          <input type="button" value="Add New Record" class="btn btn-primary"  onClick="addRow('dataTable')" />
          <input type="button" value="Remove Record" class="btn btn-danger" onClick="deleteRow('dataTable')"  />
            </p>
          </div>
          <div class="clear"></div>
                         <div class="modal-footer">
                            <input type="submit" value="ADD" class="btn btn-link waves-effect" id="add-record"/>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
     </div>


        <!-- Edit and delete subjects modal -->

 <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="sem">Semister</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="sem" name="sem"
              placeholder="Faculty Unique Code" disabled>
            </div>
          </div>

           <div class="form-group">
            <label class="control-label col-sm-2" for="course_code">Course Code:</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="course_code" name="course_code"
              placeholder="Course Code" required>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="name"> Subject Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="name" name="name"
              placeholder="Subject name" required>
            </div>
          </div>

           <div class="form-group">
            <label class="control-label col-sm-2" for="short_name"> Subject Short  Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="short_name" name="short_name"
              placeholder="Subject Short name" required>
            </div>
          </div>
        </form>

        {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="code"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>

  <!--function Edit POST -->
 <script type="text/javascript">

$(document).on('click', '.edit-modal', function() {

    $('#footer_action_button').text("Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Edit Subject');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#id').val($(this).data('id'));

    $('#sem').val($(this).data('sem'));
    $('#course_code').val($(this).data('course_code'));
    $('#name').val($(this).data('name'));
    $('#short_name').val($(this).data('short_name'));

    $('#myModal').modal('show');

});

$(document).on('click', '#myModal', function() {
    $("#edit").click(function() {
            $.ajax({

                'type':'POST',
                'url':'subjectsEdit',
                'data': {
                    '_token': $('.input[name=_token]').val(),
                    'id':$('#id').val(),
                    'course_code': $('#course_code').val(),
                    'name':$('#name').val(),
                    'short_name': $('#short_name').val()
                },
                success:function(data) {

                    $('#myModal').modal('hide');
                    window.location = data.url;
                },
            });
    });
});

// form Delete function
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete Subject');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.code').html($(this).data('course_code'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'subjectsDelete',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){
           $('.uniquecodes' + $('.id').text()).remove();
           $('.delete-modal').modal('hide');
           window.location = data.url;
        }
      });
    });
</script>
</section>
@endsection