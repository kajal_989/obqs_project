@extends('layouts.admin_layout')

@section('content')


	 <section class="content">


            <!-- Example Tab -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>All Registered Faculties</h2>

                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                <li role="presentation" class="active"><a href="#home" data-toggle="tab">View All Faculty</a></li>
                                <li role="presentation"><a href="#profile" data-toggle="tab">Add Faculty ID</a></li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home">

                                    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="block-header">

            </div>

            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">

                        <div class="body">

                           <table id="dataTable" class="table-responsive" border="1">
                  <tbody>
                    <tr>
                      <p>

            <td><input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked />
              <label for="md_checkbox_21"></label></td>
            <td>

              <input type="text" required="required" name="code[]">
             </td>
             <td>

              <input type="text" required="required"  name="short_name[]">
               </td>

        </p>
                    </tr>
                    </tbody>
                </table>

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>

                                            <th>CODE</th>
                                            <th>NAME</th>
                                            <th>ACTION</th>

                                        </tr>
                                    </thead>


                                    <tfoot>
                                        <tr>

                                            <th>CODE</th>
                                            <th>NAME</th>
                                            <th>ACTION</th>

                                        </tr>
                                    </tfoot>
                                    <tbody>

        @foreach($faculty as $f )
         <form  method="GET"  onsubmit = "return confirm('Are you sure?')">
                        {{ csrf_field() }}
            <tr name = "{{$f->id}}" >

            <td> {{$f->code}}</td>
            <td> {{$f->name}}</td>
            <td><a href="delete/{{ $f->id }}"  class="btn btn-danger" >Delete</a>

                  </td>
             </tr>

        @endforeach
</form>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
            </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile">
                                  <div class="table-responsive">
                                    <form action="{{route('store.unique-codes')}}" class="register" method="POST">

                                      {{csrf_field()}}


        <p>
          <input type="button" value="Add New Record" onClick="addRow('dataTable')" />
          <input type="button" value="Remove Record" onClick="deleteRow('dataTable')"  />

        </p>

            </fieldset>

      <input class="submit" type="submit" value="Confirm" name = "submit" />


      <div class="clear"></div>
      <script type="text/javascript">
      function addRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  if(rowCount < 20){             // limit the user from creating fields more than your limits
    var row = table.insertRow(rowCount);
    var colCount = table.rows[0].cells.length;
    for(var i=0; i<colCount; i++) {
      var newcell = row.insertCell(i);
      newcell.innerHTML = table.rows[0].cells[i].innerHTML;
    }
  }else{
     alert("Maximum Passenger per ticket is 5.");

  }
}

function deleteRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 1) {             // limit the user from removing all the fields
        alert("Cannot Remove all the Passenger.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
      </script>
        </form>
      </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Example Tab -->

         </div>

    </section>
@endsection