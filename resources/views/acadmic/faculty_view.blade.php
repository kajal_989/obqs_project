@extends('layouts.admin_layout')
@section('content')
	 <section class="content">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>All Registered Faculties &nbsp;&nbsp;  <a type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addModal">
                                        Add New Faculty ID</a>
                            </h2>
                        </div>
                        <div class="body">
                                    <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               @if (Session::has('success'))
               <div class="alert alert-success">
               <strong>Success! {!! session('success') !!}</strong>
               </div>
                @endif
                 @if (Session::has('error'))
               <div class="alert alert-danger">
               <strong>Failed! {!! session('error') !!}</strong>
               </div>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="table-responsive">
                       <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                           <thead>
                              <tr>
                                  <th>SERIAL NO</th>
                                  <th>CODE</th>
                                  <th>Short Name</th>
                                  <th>Actions</th>
                              </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                   <th>SERIAL No</th>
                                   <th>CODE</th>
                                   <th>Short Name</th>
                                  <th>Actions</th>
                                </tr>
                           </tfoot>
                          <tbody>
                         {{ csrf_field() }}
                           @foreach($uniquecodes as $uc )
                              <tr>
                               <td>{{ $loop->iteration }}</td>
                               <td> {{$uc->code}}</td>
                               <td> {{$uc->short_name}}</td>
                                  <td>
            <button class="edit-modal btn btn-warning btn-sm" data-id="{{$uc->id}}" data-code="{{$uc->code}}" data-short_name="{{$uc->short_name}}" @if($uc->code == 'ADMIN') disabled="true" @endif>
               <i class="material-icons">edit</i>
            </button>
            <button class="delete-modal btn btn-danger btn-sm" data-id="{{$uc->id}}" data-code="{{$uc->code}}" data-short_name="{{$uc->short_name}}" @if($uc->code == 'ADMIN') disabled="true" @endif>
               <i class="material-icons">delete</i>
            </button>
          </td>
                                 </tr>
                           @endforeach
                     </tbody>
                    </table>
                </div>

               </div>
             </div>
          </div>
        </div>
    </div>
  </div>
 </div>

 <script type="text/javascript">
  function addRow(tableID) {
        var table = document.getElementById(tableID);
        var rowCount = table.rows.length;
        if(rowCount < 20){             // limit the user from creating fields more than your limits
            var row = table.insertRow(rowCount);
            var colCount = table.rows[0].cells.length;
            for(var i=0; i<colCount; i++) {
                var newcell = row.insertCell(i);
                newcell.innerHTML = table.rows[1].cells[i].innerHTML;
                }
          }else{
           alert("maximum 20 rows are allowded.");

      }
  }

function deleteRow(tableID) {
     var table = document.getElementById(tableID);
     var rowCount = table.rows.length;
     for(var i=0; i<rowCount; i++) {
     var row = table.rows[i];
     var chkbox = row.cells[0].childNodes[0];
      if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all Rows.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
</script>
 <div class="modal fade" id="addModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title" id="defaultModalLabel">Add new Faculty ID</h4>
              </div>
              <div class="modal-body">
                  <form method="post" class="form-horizontal" action="{{ route('faculty-add') }}">
                             {{ csrf_field() }}

                  <div class="table-responsive">
                  <table id="dataTable" class="table table-condensed">
                 <thead>
                      <th>select</th>
                      <th>Unique Code</th>
                      <th>Name</th>
                 </thead>
                  <tbody>
                    <tr>
                    <p>

                    <td><input type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked required="required"/>
                    </td>

                    <td>
                     <input type="text"  class="form-control" placeholder="Unique ID" name="code[]">
                    </td>

                    <td>
                    <input type="text"  class="form-control"  placeholder="name" name="short_name[]">
                    </td>

                   </p>
                    </tr>
                    </tbody>
                </table>
                <br>
                  <p>
                      <input type="button" value="Add New Record" class="btn btn-primary"  onClick="addRow('dataTable')" />
                      <input type="button" value="Remove Record" class="btn btn-danger" onClick="deleteRow('dataTable')"  />
                  </p>
              </div>
              <div class="clear"></div>
              </div>
                <div class="modal-footer">
                  <input type="submit" value="ADD" class="btn btn-link waves-effect"/>
                  <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
              </div>
              </form>
              </div>
            </div>
          </div>

<!--  Edit and delete faculty modal codes -->

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id" disabled>
            </div>


            <label class="control-label col-sm-2" for="Code">Code</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="code" name="code"
              placeholder="Faculty Unique Code" required>

          </div>

            <label class="control-label col-sm-2" for="to_year">Name</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="short_name" name="short_name"
              placeholder="Faculty name" required>
            </div>
         </div>
        </form>

        {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="code"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>

  <!--function Edit POST -->
 <script type="text/javascript">

$(document).on('click', '.edit-modal', function() {

    $('#footer_action_button').text("Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Edit Faculty code');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#id').val($(this).data('id'));

    $('#code').val($(this).data('code'));

    $('#short_name').val($(this).data('short_name'));

    $('#myModal').modal('show');

});

$(document).on('click', '#myModal', function() {
    $("#edit").click(function() {
            $.ajax({

                'type':'POST',
                'url':'editFaculty',
                'data': {
                    '_token': $('.input[name=_token]').val(),
                    'id':$('#id').val(),
                    'code': $('#code').val(),
                    'short_name': $('#short_name').val()
                },
                success:function(data) {

                    $('#myModal').modal('hide');
                    window.location = data.url;
                },
            });
    });
});

// form Delete function
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete Faculty');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.code').html($(this).data('code'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'deleteFaculty',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){
           $('.uniquecodes' + $('.id').text()).remove();
           $('.delete-modal').modal('hide');
           window.location = data.url;
        }
      });
    });
</script>
 </section>
@endsection