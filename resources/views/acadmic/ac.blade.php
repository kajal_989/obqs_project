@extends('layouts.admin_layout')
@section('content')
	 <section class="content">

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Acadmic Years
                            </h2>
                        </div>
                        <div class="body">

                                    <b>
                                        <a type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#changeacModal">
                                        Change acadmic year</a>&nbsp;&nbsp;
                                    </b>
                       @if (Session::has('message'))
        <h4 style="color:green">{!! session('message') !!}</h4>
   @endif

    @if (Session::has('success'))
            <h5 style="color:green">{!! session('success') !!}</h5>
    @endif

    @if (Session::has('error'))
        <h5 style="color:red">{!! session('error') !!}</h5>
    @endif
                                    <h1>
                                       Current Acadmic Year  {{$ac->from_year}}  - {{$ac->to_year}}
                                    </h1>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Example Tab -->

         </div>

    </section>
      <!-- modal for changing acadmic year-->
            <div class="modal fade" id="changeacModal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h2 class="modal-title" id="defaultModalLabel">Change Acadmic Year</h2>
                        </div>
                         <form method="post" action="{{ route('update-year') }}">
                             {{ csrf_field() }}
                        <div class="modal-body">


                            <label>Select Acadmic Year</label><br>

                                        <select name="id" style=" height: 29px;
   overflow: hidden;
   width: 240px; " >
                                        <option>---Please select the Acadmic Year---</option>
                                    @foreach($all as $al)
                                        <option  value="{{$al->id}}"> {{$al->from_year}}  -  {{$al->to_year}}</option>

                                     @endforeach
                                        </select>

                        </div>
                        <div class="modal-footer">
                            <input type="submit" value="CHANGE" class="btn btn-link waves-effect"/>
                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>

 </section>
@endsection