@extends('layouts.admin_layout')
@section('content')
<section class="content">
 <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
       <div class="card">
          <div class="header">
              <h2>All Acadmic Years &nbsp;
                <a href="#" class="create-modal btn btn-success btn-sm">Add new Year
                </a>
             </h2>
          </div>
          <div class="body">

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">

    @if (Session::has('success'))
            <h5 style="color:green">{!! session('success') !!}</h5>
    @endif

    @if (Session::has('error'))
        <h5 style="color:red">{!! session('error') !!}</h5>
    @endif
                            <div class="table table-responsive">
    <table class="table table-bordered" id="table">
      <tr>
        <th width="150px">No</th>
        <th>Starting Year</th>
        <th>Ending Year</th>
        <th>Status</th>
        <th>Actions</th>
      </tr>
      {{ csrf_field() }}
      <?php $no = 1;?>
      @foreach ($acyear as $ac)
        <tr class="acyear{{$ac->id}}">
          <td>{{ $no++ }}</td>
          <td>{{ $ac->from_year }}</td>
          <td>{{ $ac->to_year }}</td>
          <td>{{ ($ac->is_active == 1) ? "Active" : "InActive"}}</td>

          <td>
            <button class="edit-modal btn btn-warning btn-sm" data-id="{{$ac->id}}" data-from_year="{{$ac->from_year}}" data-to_year="{{$ac->to_year}}">
               <i class="material-icons">edit</i>
            </button>
            <button class="delete-modal btn btn-danger btn-sm" data-id="{{$ac->id}}" data-from_year="{{$ac->to_year}}" data-to_year="{{$ac->to_year}}" @if($ac->is_active == 1) disabled="true" @endif>
               <i class="material-icons">delete</i>
            </button>
          </td>
        </tr>
      @endforeach
    </table>
  </div>
  {{$acyear->links()}}
</div>
</div>
</div>
</div>
</div>
</div>

{{-- Modal Form Create Post --}}
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
         <form class="form-horizontal" role="form">
          <div class="form-group row add">
            <label class="control-label col-sm-2" for="from_year">Starting Year</label>
            <div class="col-sm-10">
            <select id="from_year" name="from_year" required="true">
                <option value="2018">2018</option>
                <option value="2018">2019</option>
                <option value="2018">2020</option>
                <option value="2018">2021</option>
                <option value="2018">2022</option>
                <option value="2018">2023</option>
                <option value="2018">2024</option>
                <option value="2018">2025</option>
            </select>
              <p class="error text-center alert alert-danger hidden"></p>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="to_year">Ending Year</label>
            <div class="col-sm-10">
               <select id="to_year" name="to_year" required="true">
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
            </select>
              <p class="error text-center alert alert-danger hidden"></p>
            </div>
          </div>
      </form>

      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="add">
              <span class="glyphicon glyphicon-plus"></span>Save Post
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remobe"></span>Close
            </button>
          </div>

    </div>

  </div>
</div></div>

{{-- Modal Form Edit and Delete Post --}}
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="from_year">Starting Year</label>
            <div class="col-sm-10">
             <select id="modal_from_year" name="modal_from_year">
                <option value="2018">2018</option>
                <option value="2018">2019</option>
                <option value="2018">2020</option>
                <option value="2018">2021</option>
                <option value="2018">2022</option>
                <option value="2018">2023</option>
                <option value="2018">2024</option>
                <option value="2018">2025</option>
            </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="to_year">Ending Year</label>
            <div class="col-sm-10">
             <select id="modal_to_year" name="modal_to_year">
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
            </select>
            </div>
          </div>
        </form>

        {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="title"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('js/app.js') }}"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript">

  $(document).on('click','.create-modal', function() {
        $('#create').modal('show');
        $('.form-horizontal').show();
        $('.modal-title').text('New Acadmic Year');
  });

  $("#add").click(function() {
    $.ajax({
      type: 'POST',
      url: 'addYear',
      data: {
        '_token': $('.input[name=_token]').val(),
        'from_year': $('#from_year option:selected').text(),
        'to_year': $('#to_year option:selected').text()
      },
      success: function(data){
         if ((data.errors)) {
          $('.error').removeClass('hidden');
          $('.error').text(data.errors.error);
          $('.error').text(data.errors.to_year);
        } else {
        $('.create-modal').modal('hide');           //after redirect hide url
        window.location = data.url;
        }               // redirect to given url which is passed in response through controller
        // if ((errors)) {
        //   $('.error').removeClass('hidden');
        //   $('.error').text(errors);

        // } else {
        //   $('.error').remove();
        //   $('#table').append("<tr class='acyear" + data.id + "'>"+
        //   "<td>" + data.id + "</td>"+
        //   "<td>" + data.from_year + "</td>"+
        //   "<td>" + data.to_year + "</td>"+
        //   "<td>" + data.status + "</td>" +
        //   "<td><button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-title='" + data.from_year + "' data-body='" + data.to_year + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-title='" + data.from_year + "' data-body='" + data.to_year + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
        //   "</tr>");
        // }
      },
    });
    $('#title').val('');
    $('#body').val('');
  });

// function Edit POST
$(document).on('click', '.edit-modal', function() {

    $('#footer_action_button').text("Update");
    $('#footer_action_button').addClass('glyphicon-check');
    $('#footer_action_button').removeClass('glyphicon-trash');
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Edit Acadmic Year');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#id').val($(this).data('id'));

    $('#modal_from_year').val($(this).data('modal_from_year'));

    $('#modal_to_year').val($(this).data('modal_to_year'));

    $('#myModal').modal('show');

});

$(document).on('click', '#myModal', function() {
    $("#edit").click(function() {
            $.ajax({
                type:'POST',
                url:'editYear',
                data: {
                    '_token': $('.input[name=_token]').val(),
                    'id':$('#id').val(),
                    'from_year': $('#modal_from_year option:selected').text(),
                    'to_year': $('#modal_to_year option:selected').text()
                },

                success:function(data) {

                    $('#myModal').modal('hide');
                    window.location = data.url;
                }
            });
    });
});





// form Delete function
    $(document).on('click', '.delete-modal', function() {
        $('#footer_action_button').text(" Delete");
        $('#footer_action_button').removeClass('glyphicon-check');
        $('#footer_action_button').addClass('glyphicon-trash');
        $('.actionBtn').removeClass('btn-success');
        $('.actionBtn').addClass('btn-danger');
        $('.actionBtn').addClass('delete');
        $('.modal-title').text('Delete Post');
        $('.id').text($(this).data('id'));
        $('.deleteContent').show();
        $('.form-horizontal').hide();
        $('.title').html($(this).data('title'));
        $('#myModal').modal('show');
    });

    $('.modal-footer').on('click', '.delete', function(){
      $.ajax({
        type: 'POST',
        url: 'deleteYear',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $('.id').text()
        },
        success: function(data){
           $('.acyear' + $('.id').text()).remove();
           $('.delete-modal').modal('hide');
           window.location = data.url;
        }
      });
    });

  // Show function
  $(document).on('click', '.show-modal', function() {
      $('#show').modal('show');
      $('#i').text($(this).data('id'));
      $('#ti').text($(this).data('title'));
      $('#by').text($(this).data('body'));
      $('.modal-title').text('Show Post');
  });
</script>

</section>
@endsection

