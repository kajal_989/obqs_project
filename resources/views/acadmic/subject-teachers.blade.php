@extends('layouts.admin_layout')
@section('content')
<section class="content">
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
              <h2>Subject Allocation</h2>
            </div>
            <div class="body">
            @if (Session::has('success'))
            <div class="alert alert-success">
            <strong>Success! {!! session('success') !!}</strong>
            </div>
             @endif
              @if (Session::has('error'))
            <div class="alert alert-danger">
            <strong>Failed! {!! session('error') !!}</strong>
            </div>
             @endif
            <form method="POST" action="{{route('subject-teachers-store')}}">
            {{csrf_field()}}
            <div class="table-responsive">
            <table id="dataTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                 <thead>
                    <th>select</th>
                    <th>Subject</th>
                    <th>Theory Teacher</th>

                    <th>Actions</th>
                  </thead>
                  <tbody>
                   <tr>
                   <td>

                      <input  type="checkbox" id="md_checkbox_21" class="filled-in chk-col-red" checked required />

                    </td>
                    <td>
                      <input type="hidden" name="$">
                      <select class="form-control"  required="required" name="subject[]">
                      <option value="">--please Select The Subject--</option>
                      @foreach($subject as $sub)
                      <option value="{{$sub->id}}">{{$sub->name}}</option>
                       @endforeach
                      </select>
                    </td>

                    <td>
                     <select class="form-control" required="required" name="theory_teacher[]">
                        <option value="">--please Select Faculty--</option>
                         @foreach($faculties as $faculty)
                        <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                         @endforeach
                     </select>
                    </td>

                  <td>
                    <input type="submit" value="Save " class="btn btn-success" />
                  </td>
              </tr>
           </tbody>
         </table>
       </div>
      </form>
    </div>
                   <script type="text/javascript">
      function addRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  if(rowCount < 20){             // limit the user from creating fields more than your limits
    var row = table.insertRow(rowCount);
    var colCount = table.rows[0].cells.length;
    for(var i=0; i<colCount; i++) {
      var newcell = row.insertCell(i);
      newcell.innerHTML = table.rows[1].cells[i].innerHTML;
    }
  }else{
     alert("Maximum Passenger per ticket is 5.");

  }
}

function deleteRow(tableID) {
  var table = document.getElementById(tableID);
  var rowCount = table.rows.length;
  for(var i=0; i<rowCount; i++) {
    var row = table.rows[i];
    var chkbox = row.cells[0].childNodes[0];
    if(null != chkbox && true == chkbox.checked) {
      if(rowCount <= 2) {             // limit the user from removing all the fields
        alert("Cannot Remove all the Rows.");
        break;
      }
      table.deleteRow(i);
      rowCount--;
      i--;
    }
  }
}
      </script>
</div>
 <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <center><h2>Subject Teacher's list </h2></center>
                        </div>

                        <div class="body">

                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>SERIAL NO</th>
                                            <th>SEMISTER</th>
                                            <th>SUBJECT NAME</th>
                                            <th>FACULTY NAME</th>
                                            <th>ACTIONS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                               @foreach($subjectteacher as $st)
                               <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$st->sem}}</td>
                               <td>{{$st->subject_name}}</td>
                               <td>{{$st->theory_teacher_name}}</td>
                               <td>
                                <button class="edit-modal btn btn-warning btn-sm" data-id="{{$st->id}}" data-subject_name="{{$st->subject_name}}" data-theory_teacher_name="{{$st->theory_teacher_name}}">
                               <i class="material-icons">edit</i>
                                </button>
                              </td>
                                </tr>
                                @endforeach
                              </tbody>
                          </table>
                     </div>
                 </div>
               </div>
            </div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="modal">
          <div class="form-group">
            <label class="control-label col-sm-2" for="id">ID</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="id" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="short_name">Semister:</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="short_name" name="code"
              placeholder="Faculty Unique Code" disabled>
            </div>
          </div>
          <div class="form-group">
             <label class="control-label col-sm-2" for="name">Faculty:</label>
             <select required="required" id="name">
                 @foreach($faculties as $faculty)
                <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                 @endforeach
              </select>

          </div>
        {{-- Form Delete Post --}}
        <div class="deleteContent">
          Are You sure want to delete <span class="code"></span>?
          <span class="hidden id"></span>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn actionBtn" data-dismiss="modal" id="edit">
          <span id="footer_action_button" class="glyphicon"></span>
        </button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">
          <span class="glyphicon glyphicon"></span>close
        </button>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript">
  $(document).on('click', '.edit-modal', function() {
    $('#footer_action_button').text("Update");
    $('.actionBtn').addClass('btn-success');
    $('.actionBtn').removeClass('btn-danger');
    $('.actionBtn').addClass('edit');
    $('.modal-title').text('Edit Class Advisers');
    $('.deleteContent').hide();
    $('.form-horizontal').show();
    $('#id').val($(this).data('id'));
    $('#short_name').val($(this).data('short_name'));

    $('#user_id').val($(this).data('user_id'));
    $('#myModal').modal('show');

});

$(document).on('click', '#myModal', function() {
    $("#edit").click(function() {
            $.ajax({

                'type':'POST',
                'url':'editClassAdviser',
                'data': {
                    '_token': $('.input[name=_token]').val(),
                    'id':$('#id').val(),
                    'short_name': $('#short_name').val(),
                    'user_id': $('#name option:selected').val()
                },
                success:function(data) {

                    $('#myModal').modal('hide');
                    window.location = data.url;
                },
            });
    });
});

</script>

    </section>
@endsection