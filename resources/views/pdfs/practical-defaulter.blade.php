<!DOCTYPE html>
<html>
<head>
	<title>Defaulter List</title>
  <style type="text/css">
  body{

    padding-right: 20px;

    padding-left: 20px;
  }
    table,th,td,tbody{

    border: 1px solid black;
    border-collapse :collapse;
    }

  </style>
</head>
<body>
	 <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
          <img src="{{public_path('/images/logo2.png')}}" hspace="20" height="100px" width="100px" align="right">
          <img src="{{public_path('/images/logo1.png')}}" hspace="20"" height="100px" width="100px"  align="left">

          <div class="header">
          <center>
               <h3>Bharti Vidyapeeth Collage of Engineering, Navi Mumbai</h3>
               <h4>Department Of Information Technology</h4>
          </center>
            </div>
            <hr>

              <div class="body">
                          <div class="row clearfix">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <center>
                                   <h5>Acadmic Year  {{$headers->from_year}} - {{$headers->to_year}} </h5>
                                   <h5>Semister: {{$headers->short_name}}</h5>
                                    <h5>Defaulter List From Date: {{$start_date}}
                                    To Date: {{ $end_date}}
                                   </h5>
                              </center>
                              <br>

                     <div class="table-responsive">
                        <table  id="table1">
                            <thead>
                              <tr>
                                 <th>Roll No</th>
                                  <th>Student Name</th>
                                  @for($i=0 ; $i< count($subjects); $i++ )
                                  <th><center>{{$subjects[$i]->short_name}}</center><hr>
                                    <center>{{$total_subjects[$i]}}</center>
                                  </th>
                                  @endfor

                                  <th>Total Lectures<br>Out of ({{ $total_attendence_count}})</th>

                                  <th>Percent Attendance<br>100%</th></th>
                                  <th>Signature</th>
                              </tr>
                            </thead>
                            <tbody>
                               @if($final)
                    @for($i=0; $i< count($final);$i++)

<tr>
     <td><center>{{$final[$i]['roll_no']}}</center></td>
     <td>{{$final[$i]['student_name']}}</td>
     @for($k=0; $k< count($subjects);$k++)
     <td><center>{{$final[$i]['subject'][$subjects[$k]->id]}}</center></td>
@endfor

<td><center>{{$final[$i]['lecture_count']}}</center></td>
<td><center>{{round($final[$i]['precent_attendance'],2)}}</center></td>
<td></td>
</tr>

 @endfor
 @endif
</tbody>
                        </table>
                      <br>
                      <br>
                      <br>
                      <p style="text-align:left; float: left"> Signature</p> <p style="text-align: right; float: right"> Signature</p>
                        <br>
                        <br>
                        <br>
                          <p style="text-align:left; float: right" >Prof. S. M. Patil.<br>H.O.D</p> <p style="text-align:left; float: left">Prof. {{$headers->name}}<br>Class Adviser </p></h5>
                         </div>

                        </div>

                      </div>
                    </div>
                  </div>

</div>

</div>

</body>
</html>
